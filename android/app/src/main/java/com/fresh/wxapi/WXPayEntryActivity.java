/**
 * Created by wtfssd on 2017/10/18.
 */

package com.fresh.wxapi;
import android.app.Activity;
import android.os.Bundle;

import com.theweflex.alipay.WeChatModule;

public class WXPayEntryActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WeChatModule.handleIntent(getIntent());
        finish();
    }
}
