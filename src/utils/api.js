const DEBUG = 'http://192.168.1.194/fresh_admin/public/';


const TEST = true;
const RELEASE = TEST ? 'http://60.205.231.25:8082/' : 'http://60.205.231.25:8082/';
const PRODUCTION = true;


const HOST = PRODUCTION ? RELEASE : DEBUG;
export default {
    PRODUCTION,
    DEBUG,
    TEST,
    RELEASE,
    HOST,
    //首页banner
    homeBanner: '/api/banner',
    //首页
    homeHotInformation: '/api/hotInformation',
    //首页热门推荐
    homeRecommend: 'api/recproduct',
    homeGuessLike: 'api/recommend_goods',
    //home
    //商品列表
    productList: 'api/procate',
    //商品详情
    productDetail: 'api/prodetail',

    //商品评价
    productComment: 'api/product_comment',

    //商品收藏
    productFav: 'api/colpro',

    //商品收藏 列表
    productFavList: 'api/memcolpro',

    //店铺收藏
    storeFav: 'api/store_collects',
    //店铺收藏 列表
    storeFavList: 'api/user_store_list',

    //店铺列表
    storeList: 'api/storelist',

    //店铺详情
    storeDetail: 'api/storedetail',

    //个人中心
    //短信
    sms: 'api/sendsms',
    //注册
    signUp: 'api/register',
    //登录
    signIn: 'api/login',
    changeUserName: 'api/changeUsername',
    //个人信息
    userInfo: 'api/memberdata',
    //修改密码
    changePwd: 'api/changePassword',
    //添加地址
    addressAdd: 'api/addaddress',
    //地址列表
    addressList: 'api/addresslist',
    //删除地址
    addressDelete: 'api/deladdress',


    //购物车列表
    cartList: TEST ? 'api/mycar1' : 'api/mycar',


    carMarketList: 'api/car_markets',


    //添加到购物车
    // cartAdd: 'api/addcar',
    cartAdd: TEST ? 'api/addcar1' : 'api/addcar',


    //获取市场列表
    markList: 'api/shop/markets',


    //收单免运费券
    freeFreight: 'api/user_coupon',


    /**
     * param:
     *  user_id
     *
     *  issend:
     *      取消 0
     *      未接单 1
     *      已接单 2
     *      已发货 3
     *      待评价 4
     *      已完成 5
     *      退款.处理中 6
     *      退款.成功 7
     */
    orderList: 'api/order/user_order_status',


    //删除购物车 商品
    cartDelete: 'api/deletepro',


    //order 创建订单
    createOrder: TEST ? 'api/new_order' : 'api/order',

    //订单
    orderDetail: 'api/order/user_orderInfo',

    orderPayStatus: TEST ? 'api/payStatus1' : 'api/order/payStatus',
    //搜索
    search: 'api/search',


    //店铺菜单列表
    shopCategoryList: 'api/shop/cate',

    //店铺商品列表
    shopProductList: 'api/shop/product',

    //店铺商品推荐
    shopRecommend: 'api/recommend',


    //测试接口
    test_cartList: 'api/mycar1',


    //评价
    evaluate: 'api/order/evaluate',

    //意见反馈
    feedBack: 'api/feedback',

    //领取优惠券
    intergralCoupon: 'api/intergralCoupon',
    //获取用户积分和剩余优惠卷的数量
    getCouponIntergral: 'api/getCouponIntergral',
    //获取用户优惠卷列表
    userCouponList: 'api/user_coupon',
    //获取平台所有优惠卷
    allCoupon: 'api/coupon',

    //高德地图ip定位
    aMapGeo: 'http://restapi.amap.com/v3/geocode/geo',
    aMapKey: '3ea566230f01591c98ef636057dd3a82'
}