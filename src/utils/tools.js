import {Toast} from 'antd-mobile'
import {ERROR_MSG_DURATION} from './constant'

export const distanceStr = (distance) => {
    const d = distance > 1000 ? distance / 1000 : distance
    const unit = distance > 1000 ? 'Km' : 'm'
    return `${Math.ceil(d * 100) / 100}${unit}`
}

export const tookQty = (qty) => {

    if (qty == 0) {
        return 30
    }
    if (qty <= 5 && qty < 10) {
        return Math.ceil(qty * 10 + parseInt(qty))
    }
    if (qty < 100) {
        return Math.ceil(qty * 1.5)
    }
    return qty
}

export const chooseRate = (rate) => {
    if (rate > 100) {
        return 99
    }
    return rate
}

export const checkToken = (auth, fail = null) => {

    if (auth instanceof Object && Object.keys(auth).length > 0) {

        if (auth.isSignIn) {

            if (auth.profile instanceof Object && Object.keys(auth.profile).length > 0) {

                if (auth.profile.token && auth.profile.token.length > 0) {

                    return true
                }
            }
        }
    }
    fail && fail()
    return false
}

export const checkResponse = (data, code = [402, 403], msg = '账户信息已过期，或者在其他地方登陆过,请重新登录') => {
    let success = true
    if (code instanceof Array) {
        for (let i = 0; i < code.length; i++) {
            if (code[i] == data.code) {
                success = false
                break
            }
        }
    } else if (code == data.code) {
        success = false
    }
    if (!success) {
        if (msg) {
            Toast.fail(msg, 1.5)
        }
    }
    return success
}

export const checkSns = (auth, channel = '') => {
    let info = null
    if (auth.profile.sns && auth.profile.sns.length > 0) {
        auth.profile.sns.map((sns) => {
            if (sns.platform == 'web' && sns.channel == channel) {
                info = sns
            }
        })
    }
    if (auth.sns && auth.sns.length > 0) {
        auth.profile.sns.map((sns) => {
            if (sns.platform == 'web' && sns.channel == channel) {
                info = sns
            }
        })
    }
    return info
}

export function array_chunk(input, size, preserve_keys) {
    // *     example 1: array_chunk(['Kevin', 'van', 'Zonneveld'], 2);
    // *     returns 1: [['Kevin', 'van'], ['Zonneveld']]
    // *     example 2: array_chunk(['Kevin', 'van', 'Zonneveld'], 2, true);
    // *     returns 2: [{0:'Kevin', 1:'van'}, {2: 'Zonneveld'}]
    // *     example 3: array_chunk({1:'Kevin', 2:'van', 3:'Zonneveld'}, 2);
    // *     returns 3: [['Kevin', 'van'], ['Zonneveld']]
    // *     example 4: array_chunk({1:'Kevin', 2:'van', 3:'Zonneveld'}, 2, true);
    // *     returns 4: [{1: 'Kevin', 2: 'van'}, {3: 'Zonneveld'}]

    let x, p = '', i = 0, c = -1, l = input.length || 0, n = []

    if (size < 1) {
        return null
    }

    if (Object.prototype.toString.call(input) === '[object Array]') {
        if (preserve_keys) {
            while (i < l) {
                (x = i % size) ? n[c][i] = input[i] : n[++c] = {}, n[c][i] = input[i]
                i++
            }
        }
        else {
            while (i < l) {
                (x = i % size) ? n[c][x] = input[i] : n[++c] = [input[i]]
                i++
            }
        }
    }
    else {
        if (preserve_keys) {
            for (p in input) {
                if (input.hasOwnProperty(p)) {
                    (x = i % size) ? n[c][p] = input[p] : n[++c] = {}, n[c][p] = input[p]
                    i++
                }
            }
        }
        else {
            for (p in input) {
                if (input.hasOwnProperty(p)) {
                    (x = i % size) ? n[c][x] = input[p] : n[++c] = [input[p]]
                    i++
                }
            }
        }
    }
    return n
}

/**
 *
 * @param errors
 * @param message
 */
export const toastValidateError = (errors, message = '') => {
    errors && Object.keys(errors).map((errorKey) => {
        errors[errorKey].errors.map((e, i) => {
            message += e.message + (errors[errorKey].errors.length - 1 !== i ? '' : '\n');
        });
    });
    Toast.fail(message, ERROR_MSG_DURATION);
};


/**
 *
 *  查找购物车内的商品
 * @param products{Array}
 * @param id{Number||String}
 * @return number {Number}
 */
//TODO:查找购物车内的商品
export const productInCart = (products, id) => {
    let count = 0;
    if(!Array.isArray(products)) return count;
    products.map(p=>{
        p.sub.map(item=>{
            if(item.id === id){
                count = item.pivot.count
            }
        })
    });
    return count;
};

/***
 * 计算购物车商品总价
 * @param  {Array} products
 * @return  {number}
 */
export const totalInCart = (products) => {

    let price = 0.0;

    if(!Array.isArray(products)) return price;

    products.map((p => {
        p.sub.map((i => {
            price += Number(i.price) * Number(i.pivot.count);
        }))
    }));
    return price;
};

/**
 * 计算购物车商品总数
 * @param products
 * @return {number}
 */
export const countInCart = (products) => {
    let count = 0;
    if(!Array.isArray(products)) return count;
    products.map((p => {
        count += p.sub.length;
    }));
    return count;
};

/**
 *
 * @param array
 * @param field
 * @param datasField
 * @param saveFields
 * @returns {Array}
 */
export const chuck = (array, field, datasField = 'data', saveFields = []) => {
    let map = {}, dest = [];
    for (let i = 0; i < array.length; i++) {
        let ai = array[i];
        if (!map['key_' + ai[field]]) {
            let obj = {};
            obj[datasField] = [ai];
            obj[field] = ai[field];
            saveFields.map(item => {
                obj[item] = ai[item];
            });
            dest.push(obj);
            map['key_' + ai[field]] = ai;

        } else {
            for (let j = 0; j < dest.length; j++) {
                let d1 = dest[j];
                if (d1[field] === ai[field]) {
                    d1[datasField].push(ai);
                }
            }
        }
    }
    return dest;
};
