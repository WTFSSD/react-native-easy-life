/**
 * Created by wtfssd on 2017/9/13.
 */

import {Actions, ActionConst} from 'react-native-router-flux'

import {AsyncStorage} from 'react-native'
import {Toast} from 'antd-mobile';
import {ERROR_MSG_DURATION} from '../utils/constant'
import * as request from '../utils/request'

const mode = {
    namespace: 'product',
    state: {
        list: [],
        loading: true,
        detail: {
            category_id: 0,
            created_at: "",
            id: 0,
            name: "",
            preview: "",
            price: "0.00",
            spec: "0",
            store_id: 0,
            store_name: "",
            summary: "",
            updated_at: "",
            zdyfl_id: 0,
            comment: [],
            is_collect: 0
        },
        comments: [],
    },
    reducers: {
        save(state, {payload}) {
            return {...state, ...payload}
        }
    },

    effects: {
        * fetchList({payload}, {call, put}) {
            yield put({
                type: 'save',
                payload: {loading: true, list: []}
            });
            const data = yield call(request.post, request.API.productList, payload);
            if (!data) return;
            yield put({
                type: 'save',
                payload: {
                    loading: false,
                    list: data.data.map(item => ({...item, icon: item.preview}))
                }
            });
        },

        //商品详情
        * fetchDetail({payload}, {call, put, select}) {
            yield put({type: 'save', loading: true});
            const data = yield call(request.post, request.API.productDetail, payload);
            yield put({type: 'save', loading: true});
            if (!data) return;
            yield put({type: 'save', payload: {detail: data.data}});
            yield put({type: 'fetchComment', payload})
        },


        //评论列表
        * fetchComment({payload}, {call, put}) {
            yield put({type: 'save', payload: {comments: []}});
            const data = yield call(request.post, request.API.productComment, payload);
            if (data) {
                let comments = [];
                if (Array.isArray(data.data)) {
                    data.data.map(item => {
                        if (item.get_comment) {
                            comments.push(item)
                        }
                    })
                }
                yield put({type: 'save', payload: {comments}});
            }
        },
        * addFav({payload}, {call, put, select}) {
            yield put({type: 'save', loading: true});
            const auth = yield select(state => state.auth);
            const product = yield select(state => state.product);
            if (!auth.isSignIn) {
                yield call(Actions.auth);
                return;
            }
            const data = yield call(request.post, request.API.productFav, payload);
            yield put({type: 'save', loading: false});
            if (!data || typeof data === 'undefined') {
                return;
            }
            product.detail.is_collect = product.detail.is_collect === 0 ? 1 : 0;
            Toast.success(data.data, ERROR_MSG_DURATION);
            yield put({type: 'save', payload: {...product}});
        },

    },

    subscriptions: {
        setup({dispatch}) {}
    },
};

export default mode