/**
 * Created by wtfssd on 2017/9/13.
 */

import * as request from '../utils/request'
import {AsyncStorage} from 'react-native'

const STORAGE_KEY = "STORAGE_KEY_HOME";
const homeModel = {
    namespace: 'home',

    state: {
        isSignIn: false,
        hot_information: [],
        banner: [],
        recommend: [],
        searchResult: [],
        loading: false,
    },

    reducers: {
        save(state, {payload}) {
            return {...state, ...payload}
        }
    },

    effects: {
        //获取banner
        * fetchBanner({payload}, {call, put}) {

            yield put({type:'save',payload:{loading:true}});
            const data = yield call(request.post, request.API.homeBanner);
            yield put({type:'save',payload:{loading:false}});
            if (!data) return;
            yield put({
                type: 'save',
                payload: {banner: data.data.map(item => ({icon: item}))}
            })
        },
        //获取热门信息
        * fetchHotInformation({payload}, {call, put}) {
            const data = yield call(request.post, request.API.homeHotInformation);
            if (!data) return;
            if(!data.data) return;
            let hot_information = [];
            if(Array.isArray(data.data)) hot_information = data.data;
            if(typeof data.data === 'object') hot_information = [data.data];
            yield put({
                type: 'save',
                payload: {hot_information}
            });
        },
        //获取热门推荐
        * fetchRecommend({payload}, {call, put}) {
            const data = yield call(request.post, request.API.homeGuessLike);
            if (!data) return;
            yield put({
                type: 'save',
                payload: {
                    recommend: data.data.data.map(item => ({
                        ...item,
                        text: item.name,
                        icon: item.preview.indexOf(request.API.HOST) === -1 ? request.API.HOST + `/${item.preview}` : item.preview,
                        product_id: item.id
                    }))
                }
            })
        },

        * search({payload}, {call, put}) {
            yield put({
                type: 'save',
                payload: {
                    loading: true,
                    searchResult: [],
                }
            });
            const data = yield call(request.get, request.API.search, payload);
            yield put({
                type: 'save',
                payload: {
                    loading: false,
                }
            });
            if (!data || typeof data === 'undefined') return;
            yield put({
                type: 'save',
                payload: {
                    loading: false,
                    searchResult: data.data.map(item => ({...item, icon: item.preview}))
                }
            })
        },
        //从本地存储中恢复用户信息
        * restoreUserFromStorage({payload}, {call, put}) {
            const profile = yield call(AsyncStorage.getItem, STORAGE_KEY);
            yield put({
                type: 'save',
                payload: {... JSON.parse(profile)}
            })
        },
    },

    subscriptions: {

        setup({dispatch}) {
            //启动时从本地存储中恢复用户信息
            // dispatch({
            //     type: 'restoreUserFromStorage',
            // });
            // dispatch({
            //     type: 'fetchBanner',
            // });
            // dispatch({
            //     type: 'fetchHotInformation',
            // });
            // dispatch({
            //     type: 'fetchRecommend',
            // })
        }
    },
};

export default homeModel