/**
 * Created by wtfssd on 2017/9/13.
 */

import {Actions} from 'react-native-router-flux'

import * as request from '../utils/request'
import {AsyncStorage} from 'react-native'
import {Toast} from 'antd-mobile'
import {ERROR_MSG_DURATION} from '../utils/constant'


const STORAGE_KEY = "STORAGE_KEY_order";
const model = {
    namespace: 'order',

    state: {
        loading: false,
        list: [],
        detail: null,
    },

    reducers: {
        save(state, {payload}) {
            return {...state, ...payload}
        }
    },

    effects: {
        //订单列表
        * fetchOrderList({payload}, {call, put, select}) {
            const auth = yield select(state => state.auth);
            yield put({
                type: 'save',
                payload: {loading: true, list: []}
            });
            const data = yield call(request.post, request.API.orderList, {user_id: auth.profile.member_id, ...payload});

            console.log('请求参数', payload);
            console.log('订单列表', data);
            yield put({
                type: 'save',
                payload: {loading: false}
            });
            if (Array.isArray(data.data)) {
                yield put({
                    type: 'save',
                    payload: {list: data.data}
                })
            }

        },


        //订单详情
        * fetchDetail({payload}, {call, put, select}) {
            yield put({
                type: 'save',
                payload: {
                    loading: true,
                    detail: null
                }
            });

            const data = yield call(request.get, request.API.orderDetail, {...payload});
            console.log('订单详情', data);
            yield put({
                type: 'save',
                payload: {
                    loading: false,
                }
            });
            if (data && typeof data === 'object') {
                if (Array.isArray(data.data) && data.data.length > 0) {
                    yield put({
                        type: 'save',
                        payload: {detail: data.data[0]}
                    })

                } else if (typeof data.data === 'object') {
                    let detail = null;
                    let index = 0;
                    Object.keys(data.data).map((k) => {
                        if (index++ === 0) {
                            detail = data.data[k]
                        }
                    });
                    yield put({
                        type: 'save',
                        payload: {detail}
                    })
                }
            }
        },

        //保存用户信息到本地存储
        * saveUserToStorage({payload}, {call}) {
            yield call(AsyncStorage.setItem, STORAGE_KEY, JSON.stringify(payload))
        },
        //从本地存储中恢复用户信息
        * restoreUserFromStorage({payload}, {call, put, select}) {
            const profile = yield call(AsyncStorage.getItem, STORAGE_KEY);

            yield put({
                type: 'save',
                payload: {...JSON.parse(profile)}
            });
            const auth = yield select(state => state.auth);
            if (auth.isSignIn) {
                yield put({
                    type: 'fetchUserInfo',
                    payload: {id: auth.profile.member_id}
                })
            }
        },
        //评价
        * evaluate({payload}, {call, put, select}) {
            const auth = yield select(state => state.auth);
            payload['user_id'] = auth.profile.member_id;
            Toast.loading('加载中...');
            const result = yield call(request.post, request.API.evaluate, payload);
            Toast.hide();
            if (result && result['code'] === 200) {
                Toast.success('评论成功', 0.5);
                yield call(Actions.pop)
            }

        }
    },

    subscriptions: {
        setup({dispatch}) {
            dispatch({
                type: 'save',
                payload: {
                    detail: null,
                    list: []
                }
            })

        }
    },
};

export default model