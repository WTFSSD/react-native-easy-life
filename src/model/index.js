import home from './home'
import chat from './chat'
import auth from './auth'
import product from './product'
import sys from './sys'
import store from './store'
import cart from './cart'
import mark from './mark'
import order from './order'
import coupon from './coupon'


export default [
    home,
    chat,
    auth,
    product,
    sys,
    store,
    cart,
    mark,
    order,
    coupon,
]