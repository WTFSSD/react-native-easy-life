/**
 * Created by wtfssd on 2017/9/13.
 */

import {Actions, ActionConst} from 'react-native-router-flux'

import * as request from '../utils/request'
import {AsyncStorage} from 'react-native'
import {Toast} from 'antd-mobile'
import {ERROR_MSG_DURATION} from '../utils/constant'

import JPush from '../push'

const STORAGE_KEY = "STORAGE_KEY_coupon";
const model = {
    namespace: 'coupon',

    state: {
        loading: false,
        couponList: [
            [],
            [],
            []
        ],
        all: [],
    },

    reducers: {
        save(state, {payload}) {
            return {...state, ...payload}
        }
    },

    effects: {

        //保存用户信息到本地存储
        * saveUserToStorage({payload}, {call}) {
            yield call(AsyncStorage.setItem, STORAGE_KEY, JSON.stringify(payload))
        },
        //从本地存储中恢复用户信息
        * restoreUserFromStorage({payload}, {call, put, select}) {
            const profile = yield call(AsyncStorage.getItem, STORAGE_KEY);
            yield put({type: 'save', payload: {...JSON.parse(profile)}});
        },


        * fetchCouponList({payload}, {call, put, select}) {
            const auth = yield select(state => state.auth);
            let coupon = yield select(state => state.coupon);
            payload['user_id'] = auth.profile.member_id;
            yield put({type: 'save', payload: {loading: true}});
            const result = yield call(request.get, request.API.userCouponList, payload);
            yield put({type: 'save', payload: {loading: false}});
            if (result && typeof result !== 'undefined') {
                if (Array.isArray(result.data) && result.data.length > 0) {
                    coupon.couponList[payload.status - 1] = result.data;
                    yield put({
                        type: 'save',
                        payload: {...coupon}
                    })
                }
            }
        },
        * fetchAllCoupon({payload}, {call, put, select}) {
            const auth = yield select(state => state.auth);
            let coupon = yield select(state => state.coupon);
            payload['user_id'] = auth.profile.member_id;
            yield put({type: 'save', payload: {loading: true}});
            const result = yield call(request.get, request.API.allCoupon, payload);
            yield put({type: 'save', payload: {loading: false}});
            if (result && typeof result !== 'undefined') {
                if (Array.isArray(result.data) && result.data.length > 0) {
                    coupon.all = result.data;
                    yield put({
                        type: 'save',
                        payload: {...coupon}
                    })
                }
            }
        },
        * doExchange({payload}, {call, put, select}) {
            const auth = yield select(state=>state.auth);
            if(!payload)payload = {};
            payload['user_id'] = auth.profile.member_id;
            const result = yield call(request.post,request.API.intergralCoupon,payload);
            if(result&&typeof result!=='undefined'){
                Toast.success('兑换成功!!!',0.6);
            }
        },
    },

    subscriptions: {
        setup({dispatch}) {
            //启动时从本地存储中恢复用户信息
            dispatch({
                type: 'restoreUserFromStorage',
            });
        }
    },
};

export default model