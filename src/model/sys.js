/**
 * Created by wtfssd on 2017/9/13.
 */

import {Actions, ActionConst} from 'react-native-router-flux'

import {AsyncStorage} from 'react-native'
import * as request from '../utils/request'
import {Toast} from 'antd-mobile'
import {ERROR_MSG_DURATION} from '../utils/constant'

const STORAGE_KEY = "STORAGE_KEY_SYS";
const mode = {
    namespace: 'sys',
    state: {
        firstLaunch: true,
        loading: false
    },
    reducers: {
        save(state, {payload}) {
            return {...state, ...payload}
        }
    },

    effects: {

        //保存信息到本地存储
        * saveUserToStorage({payload}, {call}) {
            yield call(AsyncStorage.setItem, STORAGE_KEY, JSON.stringify(payload))
        },
        //从本地存储中恢复信息
        * restoreUserFromStorage({payload}, {call, put}) {
            const data = yield call(AsyncStorage.getItem, STORAGE_KEY);
        },


        * feedback({payload}, {call, put}) {


            yield put({type: 'save', payload: {loading: true}});
            const result = yield call(request.post, request.API.feedBack, payload);
            yield put({type: 'save', payload: {loading: false}});
            if (result && typeof result !== 'undefined') {
                Toast.success('反馈成功!', ERROR_MSG_DURATION);
            }
        },
    },

    subscriptions: {
        setup({dispatch}) {
        }
    },
};

export default mode