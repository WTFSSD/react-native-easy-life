/**
 * Created by wtfssd on 2017/5/17.
 */
import React from 'react'
import {StyleSheet,} from 'react-native'
import {connect,} from 'react-redux'
import {Scene, Router, Modal, Reducer, Stack, Actions} from 'react-native-router-flux'

import MyIcon from './components/common/myIcon'
import {Color} from './assets/styles/index'

import HomePage from './components/home/index'
import SearchPage from './components/home/search'

import CategoryPage from './components/category/index'

import ProductListPage from './components/product/index'
import ProductDetailPage from './components/product/detail'

import ShopPage from './components/shop/index'
import ShopDetailPage from './components/shop/detail'

import ShopCartPage from './components/shopCart/index'
import SubmitOrderPage from './components/shopCart/submitOrder'
import TextInPutModalPage from './components/shopCart/TextInputModal'

import MemberPage from './components/member/index'
import ProfilePage from './components/member/profile'
import ProfileEditPage from './components/member/profileEdit'
import NoticePage from './components/member/notice'
import NoticeDetailPage from './components/member/noticeDetail'
import AddressListPage from './components/member/addressList'
import AddressEditPage from './components/member/addressEdit'
import FavPage from './components/member/fav'
import CouponListPage from './components/member/couponList'
import ContactPage from './components/member/contact'

import LoginPage from './components/auth/index'
import SignUpPage from './components/auth/signUp'
import FindPwdPage from './components/auth/findPwd'


import FinishOrderPage from './components/order/finish/index'
import UnfinishOrderPage from './components/order/unfinish/index'
import AfterSalesPage from './components/order/afterSales/index'
import SendingPage from './components/order/sending/index'
import OrderDetailPage from './components/order/detail/index'
import EvaluatePage from './components/order/evaluate/index'
import RefundPage from './components/order/refund/index'
import DriverTrackPage from './components/order/driverTrack/index'

import CookbookPage from './components/cookbook/index'


import ChatPage from './components/chat/index';

import WebViewPage from './components/web/index'
import MarkListPage from './components/web/marksList'


import RecommendPage from './components/member/recommend'
import FeedbackPage from './components/member/feedback'
import CreditPage from './components/member/credit'


/**
 * 图标
 */
const TabIcon = (props) => {
    return <MyIcon name={props.image} size={25} color={props.focused ? Color.primary : '#848484'}/>
};


/**
 * 样式
 */
const Styles = StyleSheet.create({
    tabBarSelectedItemStyle: {
        backgroundColor: '#fff',
    },
    tabBarStyle: {
        backgroundColor: 'transparent',
    },
    icon: {
        width: 19,
        height: 20
    }
});

const defaultProps = {
    hideNavBar: true,
    hideTabBar: false,
    tabs: true,
    showLabel: true,
    swipeEnabled: false,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    back: true,
    tabBarSelectedItemStyle: Styles.tabBarSelectedItemStyle,
    tabBarStyle: Styles.tabBarStyle,
    navigationBarStyle: {
        backgroundColor: Color.primary,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleStyle: {
        color: 'white'
    }
};
const reducerCreate = params => {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
        return defaultReducer(state, action);
    };
};

class Routers extends React.Component {
    shouldComponentUpdate() {
        return false;
    }


    onEnter = (sceneKey) => {
        if (sceneKey === 'shopCart') {
            if (this.props.auth.isSignIn) {
                this.props.dispatch({
                    type: 'cart/fetchMarketList',
                    payload: {
                        user_id: this.props.auth.profile.member_id,
                    }
                });
                // this.props.dispatch({
                //     type: 'cart/fetchCoupon',
                //     payload: {
                //         user_id: this.props.auth.profile.member_id,
                //     }
                // })
            }
        }
        if (sceneKey === 'shop') {
            this.props.dispatch({
                type: 'store/fetchList'
            })
        }
        if (sceneKey === 'home') {
            this.props.dispatch({
                type: 'home/fetchBanner',
            });
            this.props.dispatch({
                type: 'home/fetchHotInformation',
            });
            this.props.dispatch({
                type: 'home/fetchRecommend',
            })
        }
        if (sceneKey === 'member') {
            if (this.props.auth.isSignIn) {
                this.props.dispatch({type: 'auth/getCouponIntergral', payload: {}})
            }
        }
    };

    render() {
        return (
            <Router getSceneStyle={getSceneStyle} createReducer={reducerCreate}>
                <Scene key="root">
                    <Modal>
                        <Scene key="root" hideNavBar>
                            {/*tab*/}
                            <Scene key="tabView" {...defaultProps} initial>

                                {/*主页*/}
                                <Stack key="home" title="首页" icon={TabIcon} image="shouye"
                                       onEnter={this.onEnter.bind(this, 'home')}>
                                    <Scene key="home" component={HomePage} hideNavBar initial/>
                                    <Scene key="search" title="搜索" component={SearchPage} hideNavBar hideTabBar/>
                                    <Scene key="markList" title="农贸市场列表" component={MarkListPage} hideNavBar
                                           hideTabBar/>
                                </Stack>

                                {/*分类*/}
                                <Stack key="category" title="分类" icon={TabIcon} image="fenlei1-copy">
                                    <Scene key="category" title="分类" component={CategoryPage} hideNavBar initial
                                           onEnter={this.onEnter.bind(this, 'category')}/>
                                </Stack>

                                {/*店铺*/}
                                <Stack key="shop" title="店铺" icon={TabIcon} image="shop"
                                       onEnter={this.onEnter.bind(this, 'shop')}>
                                    <Scene key="shop" component={ShopPage} hideNavBar initial
                                           onEnter={this.onEnter.bind(this, 'shop')}/>
                                </Stack>

                                {/*购物车*/}
                                <Stack key="shopCart" title="购物车" icon={TabIcon} image="gouwuche">
                                    <Scene key="shopCart" component={ShopCartPage} hideNavBar hideTabBar={false} initial
                                           onEnter={this.onEnter.bind(this, 'shopCart')}/>
                                    <Scene key="submitOrder" title="提交订单" component={SubmitOrderPage} hideTabBar
                                           hideNavBar/>
                                </Stack>

                                {/*个人中心*/}
                                <Stack key="member" title="我的" icon={TabIcon} image="gerenzhongxin"
                                       onEnter={this.onEnter.bind(this, 'member')}>
                                    <Scene key="member" component={MemberPage} hideNavBar hideTabBar={false} initial/>
                                    <Scene key="profileEdit" title="修改个人信息" component={ProfileEditPage} hideTabBar
                                           hideNavBar/>
                                    <Scene key="notice" title="通知" component={NoticePage} hideTabBar hideNavBar/>
                                    <Scene key="noticeDetail" title="通知详情" component={NoticeDetailPage} hideTabBar
                                           hideNavBar/>

                                    <Scene key="fav" title="我的收藏" component={FavPage} hideTabBar hideNavBar/>

                                    <Scene key="recommend" title="我的推荐" component={RecommendPage} hideTabBar
                                           hideNavBar/>
                                    <Scene key="contact" title="联系客服" component={ContactPage} hideTabBar hideNavBar/>
                                    <Scene key="credit" title="我的积分" component={CreditPage} hideTabBar hideNavBar/>
                                </Stack>
                            </Scene>

                            {/*公共 Nav 导航*/}
                            {/*个商品*/}
                            <Scene key="productList" title="商品列表" component={ProductListPage} hideNavBar/>
                            <Scene key="productDetail" title="商品详情" component={ProductDetailPage} hideNavBar
                                   hideTabBar/>
                            <Scene key="shopDetail" title="店铺详情" component={ShopDetailPage} hideNavBar hideTabBar/>
                            {/*订单*/}
                            <Scene key="order_finish" title="已完成" component={FinishOrderPage} hideNavBar hideTabBar/>
                            <Scene key="order_unFinish" title="未完成" component={UnfinishOrderPage} hideNavBar
                                   hideTabBar/>
                            <Scene key="order_sending" title="已发货" component={SendingPage} hideNavBar hideTabBar/>
                            <Scene key="order_afterSales" title="退款/售后" component={AfterSalesPage} hideNavBar
                                   hideTabBar/>
                            <Scene key="order_detail" title="订单详情" component={OrderDetailPage} hideNavBar hideTabBar/>
                            <Scene key="order_evaluate" title="订单评价" component={EvaluatePage} hideNavBar hideTabBar/>
                            <Scene key="order_refund" title="申请退款" component={RefundPage} hideNavBar hideTabBar/>
                            <Scene key="order_track" title="骑手跟踪" component={DriverTrackPage} hideNavBar hideTabBar
                            />
                            {/*菜谱推荐*/}
                            <Scene key="cookbook" title="菜谱推荐" component={CookbookPage} hideNavBar hideTabBar/>
                            {/*客服*/}
                            <Scene key="chat" title="客服" component={ChatPage} hideNavBar hideTabBar/>
                            <Scene key="web" title="" component={WebViewPage} hideNavBar hideTabBar/>

                            <Scene key="addressList" title="我的地址" component={AddressListPage} hideTabBar
                                   hideNavBar/>
                            <Scene key="addressEdit" title="编辑地址" component={AddressEditPage} hideTabBar
                                   hideNavBar/>

                            <Scene key="feedback" title="意见反馈" component={FeedbackPage} hideTabBar
                                   hideNavBar/>


                            <Scene key="profile" title="个人信息" component={ProfilePage} hideTabBar hideNavBar/>
                            <Scene key="couponList" title="我的优惠券" component={CouponListPage} hideTabBar
                                   hideNavBar/>
                        </Scene>


                        {/*公共 模态 Modal*/}
                        <Scene key="input" title="买家留言" component={TextInPutModalPage} hideNavBar/>
                        <Scene key="findPwd" title="找回密码" component={FindPwdPage} hideNavBar hideTabBar/>

                        <Scene key="auth" hideTabBar>
                            <Scene key="signIn" title="登录" component={LoginPage} hideNavBar/>
                            <Scene key="signUp" title="注册" component={SignUpPage} hideNavBar hideTabBar/>
                        </Scene>

                    </Modal>
                </Scene>


            </Router>
        )
    }
}


const RouterComponent = () => {


};

export default connect(
    ({auth, cart, mark, store, home}) => ({auth, cart, mark, store, home})
)(Routers);

const getSceneStyle = function (props, computedProps) {
    return {
        flex: 1,
        shadowColor: null,
        shadowOffset: null,
        shadowOpacity: null,
        shadowRadius: null,
        backgroundColor: 'white',
    };
};



