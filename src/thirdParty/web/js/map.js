$(document).ready(function () {
    window.document.addEventListener('message', onMessage);
    let userMarker = null;
    let driverMarker = null;
    const content = "<div class=\"markerContent\"><div class=\"markerContent text\">__TEXT__</div><i class=\"iconfont icon-icon-up\" id=\"arrow\"></i><i class=\"iconfont icon-mg-location\" id=\"\location\"'></i></div>";
    let map = new AMap.Map('container', {
        resizeEnable: true,
    });
    map.plugin(['AMap.Geolocation', 'AMap.ToolBar'], function () {
        const geo = new AMap.Geolocation({
            enableHighAccuracy: true, //是否使用高精度定位，默认:true
            timeout: 10000, //超过10秒后停止定位，默认：无穷大
            maximumAge: 60000, //定位结果缓存0毫秒，默认：0
            convert: false, //自动偏移坐标，偏移后的坐标为高德坐标，默认：true
            showButton: true, //显示定位按钮，默认：true
            buttonPosition: 'LB', //定位按钮停靠位置，默认：'LB'，左下角
            buttonOffset: new AMap.Pixel(10, 20), //定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
            showMarker: true, //定位成功后在定位到的位置显示点标记，默认：true
            showCircle: true, //定位成功后用圆圈表示定位精度范围，默认：true
            panToLocation: true, //定位成功后将定位到的位置作为地图中心点，默认：true
            zoomToAccuracy: true //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
        });
        map.addControl(geo);

        geo.getCurrentPosition();
        AMap.event.addListener(geo, 'complete', onComplete); //返回定位信息
        AMap.event.addListener(geo, 'error', onError); //返回定位出错信息
    });

    function onComplete(data) {

        if (userMarker) {
            userMarker.setPosition(data.position.getLng(), data.position.getLat());
        } else {
            let marker = new AMap.Marker({ //添加自定义点标记
                position: map.getCenter(), //基点位置
                offset:new AMap.Pixel(-59,-74),
                content: content.replace(/__TEXT__/g, '我的位置'),
            });
            userMarker = marker;
            marker.setMap(map);
        }
        console.log(data.position.lng);
        window.postMessage(JSON.stringify({lng: data.position.lng, lat: data.position.lat}), '*');
    }

    function onError(error) {
        window.postMessage(JSON.stringify(error), '*');
    }

    function onMessage(e) {
        document.getElementById('distance').innerHTML = '1234';
    }
});
