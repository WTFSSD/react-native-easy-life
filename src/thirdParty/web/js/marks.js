$(document).ready(function() {
    window.document.addEventListener('message', onMessage);


    let markList = [];
    const infoWindow = new AMap.InfoWindow();
    let currentMarket = 0;
    let map = new AMap.Map('map', {
        resizeEnable: true,
        zoom: 11,
        center: [118.765832, 32.029075]
    });

    AMapUI.loadUI(['control/BasicControl'], function(BasicControl) {

        //缩放控件，显示Zoom值
        map.addControl(new BasicControl.Zoom({
            position: 'rb',
            showZoomNum: true
        }));
    });
    //缩放控件，显示Zoom值
    map.plugin(['AMap.Geolocation', 'AMap.ToolBar'], function() {
        const geo = new AMap.Geolocation({
            enableHighAccuracy: true, //是否使用高精度定位，默认:true
            timeout: 10000, //超过10秒后停止定位，默认：无穷大
            maximumAge: 60000, //定位结果缓存0毫秒，默认：0
            convert: false, //自动偏移坐标，偏移后的坐标为高德坐标，默认：true
            showButton: false, //显示定位按钮，默认：true
            buttonPosition: 'LB', //定位按钮停靠位置，默认：'LB'，左下角
            buttonOffset: new AMap.Pixel(10, 20), //定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
            showMarker: true, //定位成功后在定位到的位置显示点标记，默认：true
            showCircle: true, //定位成功后用圆圈表示定位精度范围，默认：true
            panToLocation: true, //定位成功后将定位到的位置作为地图中心点，默认：true
            zoomToAccuracy: true //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
        });

        AMap.event.addListener(geo, 'complete', onComplete); //返回定位信息
        AMap.event.addListener(geo, 'error', onError); //返回定位出错信息
    });

    function onComplete(data) {


        window.postMessage(JSON.stringify({
            lng: data.position.lng,
            lat: data.position.lat
        }), '*');
    }

    function onError(error) {
        // console.log(error);
        window.postMessage(JSON.stringify(error), '*');
    }


    function onMessage(e) {
        if (e.data) {

            let data = e.data;
            data = JSON.parse(data);
            currentMarket = e.data.id;
            const position = [data.lng, data.lat];

            infoWindow.setContent(data.name);
            infoWindow.open(map, position);
            let markItemEl = $('.markItem');
            markItemEl.removeClass('active');
            $(`.markItem[market_id=${currentMarket}]`).addClass('active');
            map.setCenter(position);
        }

    }

    $('#ok').on('click', function() {

        let data = null;
        $(markList).each(function(index, item) {
            if (currentMarket == item.id) {
                data = item;
            }
        });
        // console.log(data);
        window.postMessage(JSON.stringify(data), '*');
    });

    getList(function(array) {

        markList = array;
        const temp = '<div  class="listItem markItem" market_id="__ID__"><div class="listItem content"><div class="listItem content name">__NAME__</div><div class="listItem content address">__ADDRESS__</div></div></div>';
        var listEl = $('#list');
        $(array).each(function(index, item) {
            if (index == 0) {
                currentMarket = item.id;
            }
            const position = [item.lng, item.lat];
            new AMap.Marker({
                position,
                title: item.name,
                map
            });
            let child = temp
                .replace(/__NAME__/g, item.name)
                .replace(/__ID__/g, item.id)
                .replace(/__ADDRESS__/g, item.adder);
            if (item.id == currentMarket) {
                infoWindow.setContent(item.name);
                infoWindow.open(map, position);
                map.setCenter(position);
            }
            listEl.append(child);
        });
        $('.markItem:first').addClass('active');
        $('.markItem').on('click', clickItem);
    });

    function markerClick(e) {
        infoWindow.setContent(e.target.content);
        infoWindow.open(map, e.target.getPosition());
    }

    function clickItem(e) {
        currentMarket = $(e.currentTarget)[0].getAttribute('market_id');
        let data = null;
        $(markList).each(function(index, item) {
            if (currentMarket == item.id) {
                data = item;
            }
        });
        const position = [data.lng, data.lat];
        infoWindow.setContent(data.name);
        infoWindow.open(map, position);
        let markItemEl = $('.markItem');
        markItemEl.removeClass('active');
        $(`.markItem[market_id=${currentMarket}]`).addClass('active');

        map.setCenter(position);
    }
});


function getList(cb) {
    $.get('http://60.205.231.25:8082/api/shop/markets')
        .fail(function(res) {
            // console.log('fail', res);
            // window.postMessage(JSON.stringify({fail:res}), '*');
        })
        .done(function(res) {
            cb(res.data)
        // window.postMessage(JSON.stringify({done:res}), '*');
        });

}