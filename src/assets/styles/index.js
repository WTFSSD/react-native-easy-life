/**
 * Created by wtfssd on 2017/9/14.
 */
import {
    StyleSheet,
    Dimensions
} from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E9E9E9'
    },
    centerVH: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    flexRow: {
        display: 'flex',
        flexDirection: 'row'
    },
    flex: {
        display: 'flex',
        flexDirection: 'column'
    },

    text1: {
        fontSize: 14,
        color: '#333',
    },
    text2: {
        fontSize: 12,
        color: '#666',
        padding: 10,
    },
    text4: {
        fontSize: 12,
        color: '#999',
    },
    text3: {
        fontSize: 18,
        color: '#fff',
    },
    priceText: {
        color: '#FC4A00',
        fontSize: 18
    },
    pdv10: {
        paddingVertical: 10,
    },
    pd10: {
        padding: 10,
    },
    pdv5: {
        paddingVertical: 5,
    },
    pdb5: {
        paddingBottom: 5,
    },
    pdt5: {
        paddingTop: 5,
    },
    pdt10: {
        paddingTop: 10,
    },

    pdh10: {
        paddingHorizontal: 10,
    },
    pdh5: {
        paddingHorizontal: 5,
    },

});

export const Color = {
    primary: '#0D7EE8'
};

//屏幕信息
export const SCREEN = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    scale: Dimensions.get('window').scale
}

export const Screen_Width = SCREEN.width;
export const Screen_Height = SCREEN.height;
export const Screen_Scale = SCREEN.scale;

export const Constant = {
    screen: SCREEN,
    navBarHeight: 44,
    statusBarHeight: 20,
    tabBarHeight: 50,
};

export const size = n => n / 375.0 * SCREEN.width;




