/**
 * Created by wtfssd on 2017/9/14.
 */

import React from 'react'

import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
} from 'react-native'

import Swiper from 'react-native-swiper'

import {SCREEN} from '../../assets/styles/index'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'

const slider = ['111', '222', '222', '333', '444', '555', '666',];
const cates = [
    {text: '田园实蔬', icon: require('../../assets/image/pic_one.png'), id: 1},
    {text: '食蛋肉类', icon: require('../../assets/image/pic_two.png'), id: 2},
    {text: '水产海鲜', icon: require('../../assets/image/pic_three.png'), id: 3},
    {text: '新鲜水果', icon: require('../../assets/image/pic_for.png'), id: 4},
    {text: '豆制品类', icon: require('../../assets/image/pic_five.png'), id: 5},
    {text: '净菜熟食', icon: require('../../assets/image/pic_six.png'), id: 6},
    {text: '粮油副食', icon: require('../../assets/image/pic_seven.png'), id: 7},
    {text: '调味百货', icon: require('../../assets/image/pic_eight.png'), id: 8},
];

const size = (n) => n * 375 * SCREEN.width;

class Page extends React.Component {
    constructor(props) {
        super(props)
    }
    renderMessage() {
        const {home} = this.props;
        let info = [];
        home.hot_information.map((item, index) => {
             if(item){
                info.push ((<TouchableOpacity key={index} style={Styles.carouselItem}>
                        <Text style={{color: '#333'}} allowFontScaling={false}>{item.mall_hot_information}</Text>
                    </TouchableOpacity>)
                )
            }
        });

        return info;
    }


    renderCategory() {

        return cates.map(({text, icon,id}, index) => {
            return (
                <TouchableOpacity key={index} onPress={Actions.productList.bind(null,{category_id:id})}>
                    <View style={Styles.item}>
                        <Image source={icon} resizeMode={'stretch'}
                               style={{width: 45 / 375 * SCREEN.width, height: 45 / 375.0 * SCREEN.width}}/>
                        <Text style={{color: '#333', fontSize: 14, marginTop: 3}}>{text}</Text>
                    </View>
                </TouchableOpacity>)
        })
    }

    render() {

        return (
            <View style={Styles.container}>
                <View style={Styles.content}>
                    <View style={Styles.category}>
                        {this.renderCategory()}
                    </View>
                    <View style={Styles.line}/>
                    <View style={Styles.message}>
                        <Image source={require('../../assets/image/message.png')}
                               style={{width: 15, height: 15, marginHorizontal: 15}}/>
                        <View style={Styles.carousel}>
                            <Swiper showButtons={false} showsPagination={false} horizontal={false} autoplay={true}>
                                {this.renderMessage()}
                            </Swiper>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        marginTop: -18,
        zIndex: 1,
        height: 208.0 / 375 * SCREEN.width,
        paddingHorizontal: 15,
    },
    content: {
        backgroundColor: 'white',
        borderRadius: 6,
        height: '100%',
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-end'
    },

    message: {
        width: '90%',
        height: 41 * 375.0 / SCREEN.width,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    carousel: {
        flex: 1,
        height: '100%',
        width: '100%',
        display: 'flex',
    },
    carouselItem: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
    },
    line: {
        width: '100%',
        height: 1,
        backgroundColor: "#d0d0d0"
    },
    category: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row'
    },
    item: {
        width: (SCREEN.width - 30) / 4,
        height: 75 / 375 * SCREEN.width,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default connect(
    ({home}) => ({home})
)(Page)