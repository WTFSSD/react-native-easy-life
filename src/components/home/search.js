/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {StyleSheet, View, ScrollView, FlatList,Keyboard,Text} from 'react-native'
import {connect} from 'react-redux'
import { Actions } from 'react-native-router-flux'
import CommonStyle, {Color} from '../../assets/styles/index'
import NavigationBar from 'react-native-navbar'
import {ProductItem, BackButton,LoadingView} from '../common/index'

const TestData = [
    {
        icon: 'http://pic.58pic.com/58pic/15/23/71/63b58PIC9gT_1024.jpg',
        id: 0,
        name: '商品名称',
        spec: '300g',
        price: 0.00,
        sold: 0.00
    },
    {
        icon: 'http://pic.58pic.com/58pic/15/23/71/63b58PIC9gT_1024.jpg',
        id: 1,
        name: '商品名称',
        spec: '300g',
        price: 0.00,
        sold: 0.00
    },
    {
        icon: 'http://pic.58pic.com/58pic/15/23/71/63b58PIC9gT_1024.jpg',
        id: 2,
        name: '商品名称',
        spec: '300g',
        price: 0.00,
        sold: 0.00
    },
    {
        icon: 'http://pic.58pic.com/58pic/15/23/71/63b58PIC9gT_1024.jpg',
        id: 4,
        name: '商品名称',
        spec: '300g',
        price: 0.00,
        sold: 0.00
    },
    {
        icon: 'http://pic.58pic.com/58pic/15/23/71/63b58PIC9gT_1024.jpg',
        id: 5,
        name: '商品名称',
        spec: '300g',
        price: 0.00,
        sold: 0.00
    },
    {
        icon: 'http://pic.58pic.com/58pic/15/23/71/63b58PIC9gT_1024.jpg',
        id: 6,
        name: '商品名称',
        spec: '300g',
        price: 0.00,
        sold: 0.00
    },
];
import TitleView from './titleView'
import Banner from './banner'
import Category from './Categorys'
import Recommand from './Recommand'
import Line from '../common/line'

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {

    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }


    renderItem({item}) {
        return <ProductItem  {...item} onPress={Actions.productDetail.bind(null,{store_id:item.store_id,id:item.id})}/>
    }

    onSearch() {

        const name = this.refs.search.state.value;
        console.log(name);
        Keyboard.dismiss();
        this.props.dispatch({
            type:'home/search',
            payload:{name},
        })

    }

    render() {
        console.log(this.props.home);
        return (
            <View style={CommonStyle.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               leftButton={<BackButton/>}
                               rightButton={{title: '搜索', tintColor: 'white', handler: this.onSearch.bind(this)}}
                               title={<TitleView onSearch={this.onSearch.bind(this)} autoFocus ref='search'/>}
                />


                    {this.props.home.searchResult.length>0? <ScrollView keyboardDismissMode="on-drag">
                        <FlatList data={this.props.home.searchResult}
                                  extraData={this.state}
                                  style={{flex: 1}}
                                  numColumns={2}
                                  ItemSeparatorComponent={Line.bind(null, {height: 1, backgroundColor: '#e9e9e9'})}
                                  renderItem={this.renderItem.bind(this)}
                                  keyExtractor={(item, index) => item.id}
                        />
                    </ScrollView>: <View style={Styles.noData}>
                        <Text>暂无数据</Text>
                    </View>}

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    noData:{
        display:'flex',
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }
});

export default connect(
    ({auth,home}) => ({auth,home})
)(Page)



