/**
 * Created by wtfssd on 2017/9/14.
 */

import React , { PropTypes , } from 'react'
import { View , Text , Image , StyleSheet , TextInput , TouchableOpacity , TouchableHighlight } from 'react-native'
import { Constant } from '../../assets/styles/index';
import SimpleLineIcons , { Button } from 'react-native-vector-icons/SimpleLineIcons'

class Page extends React.Component {
  constructor( props ) {
    super( props );
    this.state = {
      value : '' ,
    }
  }

  componentDidMount() {
    this.setState( {
      value : this.props.value ,
    } )
  }

  componentWillUnmount() {

    this.refs.input.blur();
  }

  clear() {
    this.setState( {
      value : '' ,
    } );
    this.refs.input.focus();
  }

  change( value ) {
    this.setState( {
      value
    } );

    if ( value && value.length > 0 ) {
      this.props.onChange( value );
    }
  }

  render() {
    return (
      <View style={Styles.container}>
        <View style={Styles.searchBar}>
          <SimpleLineIcons name="magnifier" size={20} color="#999"/>
          <TextInput placeholder={this.props.placeholder}
                     ref='input'
                     style={Styles.input}
                     value={this.state.value}
                     underlineColorAndroid="transparent"
                     returnKeyType="done"
                     autoCapitalize="none"
                     autoCorrect={false}
                     clearTextOnFocus={true}
                     autoFocus={this.props.autoFocus}
                     onChangeText={this.change.bind( this )}
                     editable={this.props.editable}
                     onFocus={this.props.onFocus}
                     onSubmitEditing={this.props.onSearch.bind( this , this.state.value )}
          />

          <TouchableOpacity onPress={this.clear.bind( this )}>
            <View>
              <SimpleLineIcons name="close" size={15} color="#999"/>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

Page.propTypes    = {
  onSearch : PropTypes.func ,
  value : PropTypes.string ,
  placeholder : PropTypes.string ,
  editable : PropTypes.bool ,
  onFocus : PropTypes.func ,
  autoFocus : PropTypes.bool ,
  onChange : PropTypes.func ,

};
Page.defaultProps = {
  placeholder : '搜索' ,
  onSearch : () => {} ,
  editable : true ,
  autoFocus : false ,
  onFocus : () => {} ,
  onChange : () => {} ,
};

const Styles = StyleSheet.create( {
  container : {
    width : Constant.screen.width ,
    height : '100%' ,
    paddingHorizontal : 20 ,
    display : 'flex' ,
    flexDirection : 'row' ,
    justifyContent : 'space-around'
  } ,
  contact : {
    width : 25 ,
    height : 27 ,

  } ,
  searchBar : {
    backgroundColor : '#fff' ,
    height : 30 ,
    width : '80%' ,
    display : 'flex' ,
    borderRadius : 5 ,
    justifyContent : 'space-between' ,
    flexDirection : 'row' ,
    alignItems : 'center' ,
    paddingHorizontal : 10 ,
  } ,
  input : {
    flex : 1 ,
    marginLeft : 5 ,
    padding : 0 ,
    fontSize : 14
  }
} );

export default Page;