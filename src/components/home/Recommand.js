/**
 * Created by wtfssd on 2017/9/14.
 */

import React, {
    PropTypes
} from 'react'

import {
    View,
    Text,
    Image,
    StyleSheet,
    ScrollView,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native';

import CommonStyle, {SCREEN} from '../../assets/styles/index'
import {Actions} from 'react-native-router-flux'


class Page extends React.Component {
    constructor(props) {
        super(props)
    }

    renderItem() {
        return this.props.data.map((item, index) => (
            <TouchableOpacity key={index} onPress={this.props.onItemPress.bind(null,item)}>
                <View style={[Styles.item, CommonStyle.centerVH]}>
                    <Image source={{uri: item.icon}}
                           style={{width: (SCREEN.width - 45) / 3, height: (SCREEN.width - 45) / 3}}/>
                    <Text style={{color: '#333', fontSize: 14, marginTop: 5}} allowFontScaling={false}>{item.text}</Text>
                </View>
            </TouchableOpacity>
        ))
    }

    render() {
        return (
            <View style={Styles.container}>
                <View style={Styles.header}>
                    <Image source={require('../../assets/image/hot.png')} style={{width: 18, height: 18}}/>
                    <Text style={{marginLeft: 10, fontSize: 14, color: '#333'}}>{this.props.title}</Text>
                </View>
                {this.props.children ? this.props.children : (
                    <ScrollView style={Styles.content}
                                horizontal
                                scrollEnabled>
                        {this.renderItem()}
                    </ScrollView>
                )}
            </View>
        )
    }
}

Page.propTypes = {
    title: PropTypes.string.isRequired,
    onItemPress: PropTypes.func,
    data:PropTypes.array,
};
Page.defaultProps = {
    onItemPress: (item) => {
        Actions.productDetail({store_id:item.store_id,id:item.id})
    },
    data:[],
};
const Styles = StyleSheet.create({
    container: {
        backgroundColor: 'white'
    },
    header: {
        display: 'flex',
        paddingVertical: 5,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',

    },
    content: {
        width: SCREEN.width,
        height: 139 / 375.0 * SCREEN.width,
        flexWrap: 'nowrap',
        display: 'flex',
    },
    item: {
        width: SCREEN.width / 3.0,
    }
});
export default Page