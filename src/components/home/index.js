/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {StyleSheet, Text, View, AsyncStorage, ScrollView, Image, TouchableOpacity} from 'react-native'
import {connect} from 'react-redux'
import {} from 'antd-mobile'
import CommonStyle, {Color} from '../../assets/styles/index'
import NavigationBar from 'react-native-navbar'
import {Actions, ActionConst} from 'react-native-router-flux'
import TitleView from './titleView'
import Banner from './banner'
import Category from './Categorys'
import Recommand from './Recommand'
import {Line} from '../common/index'

import Jpush from '../../push'

class Page extends Component {
    constructor(props) {
        super(props);
    }

    /*组件将要加载*/
    componentWillMount() {
        if (this.props.auth.isSignIn) {
            Jpush.setAlias(this.props.auth.profile.member_id)
        } else {
            Jpush.setAlias('')
        }
    }


    onSearch() {
    }

    onRefresh = () => {
        this.props.dispatch({
            type: 'home/fetchBanner',
        });
        this.props.dispatch({
            type: 'home/fetchHotInformation',
        });
        this.props.dispatch({
            type: 'home/fetchRecommend',
        })
    };

    renderLetTitle = () => {
        return (
            <TouchableOpacity onPress={Actions.markList.bind(this)}>
                <View style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                    width: 50
                }}>
                    <Text style={{
                        color: '#fff',
                        fontSize: 10
                    }}
                          numberOfLines={2}
                    >{this.props.mark.selectMarket ? this.props.mark.selectMarket.name : '选择市场'}</Text>
                </View>
            </TouchableOpacity>
        )
    };

    render() {
        const RightButton = (
            <TouchableOpacity onPress={Actions.chat}>
                <View style={[CommonStyle.centerVH, {
                    height: 44,
                    paddingHorizontal: 10
                }]}>
                    <Image source={require('../../assets/image/contact.png')} style={{
                        width: 25,
                        height: 25
                    }}/>
                </View>
            </TouchableOpacity>
        );
        return (
            <View style={CommonStyle.container}>
                <NavigationBar tintColor={Color.primary}

                    //TODO:市场列表
                               leftButton={this.renderLetTitle()}
                               statusBar={{style: 'light-content'}}
                               rightButton={RightButton}
                               title={<TitleView onSearch={this.onSearch.bind(this)} onFocus={() => {
                                   this.refs.searchBar.refs.input.blur();
                                   Actions.search();
                               }} autoFocus={false} ref='searchBar'/>}
                />
                <ScrollView keyboardDismissMode='on-drag' onRefresh={this.onRefresh}
                            refreshing={this.props.home.loading}>
                    <Banner slider={this.props.home.banner}/>
                    <Category/>


                    <Line height={15}/>
                    <Recommand title="猜你喜欢" data={this.props.home.recommend}/>


                    {/*<Line height={2}/>*/}
                    {/*<Recommand title="菜谱推荐" onItemPress={Actions.cookbook}/>*/}
                </ScrollView>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default connect(
    ({home, mark, auth}) => ({home, mark, auth})
)(Page)



