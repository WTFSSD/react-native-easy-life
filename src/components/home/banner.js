import React , {
  PropTypes ,
} from 'react'
import { View , Text , Image , TouchableHighlight , Linking } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Carousel } from 'antd-mobile'
import { SCREEN } from '../../assets/styles/index'

class HomePage extends React.Component {
  constructor( props ) {
    super( props )
  }

  handItemClick( platform , uri ) {

  }

  render() {

    return (
      <View>
        <Carousel autoplay infinite dots={true}>
          {this.props.slider.map( ( item , index ) => {
            return (
              <TouchableHighlight key={`${index}`} onPress={this.handItemClick.bind( this , item.platform , item.uri )}>
                <Image source={{
                  uri : item.icon
                }} style={{
                  height : 200.0 / 375 * SCREEN.width ,
                  width : SCREEN.width
                }}
                       resizeMode={'stretch'}
                />
              </TouchableHighlight>
            )
          } )}
        </Carousel>
      </View>
    )
  }
}

HomePage.propTypes    = {
  slider : PropTypes.array
};
HomePage.defaultProps = {
  slider : []
};

export default HomePage


