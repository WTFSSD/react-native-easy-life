/**
 // * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {
    StyleSheet, Text, View, ScrollView,
} from 'react-native'
import {connect} from 'react-redux'
import {List, InputItem, Button, Flex} from 'antd-mobile';
import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color, SCREEN} from '../../assets/styles/index'
import {BackButton} from '../common/index'
import {createForm} from 'rc-form';
import {toastValidateError} from '../../utils/tools'

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {

    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }

    /**
     * 开启定时器
     */
    starTimer() {
        let {auth: {sending, coldDownTime,}, dispatch} = this.props;
        clearInterval(this.timer);
        if (!sending) return;

        this.timer = setInterval(() => {
            if (coldDownTime >= 0) {
                dispatch({
                    type: 'auth/save',
                    payload: {sendButtonText: `${coldDownTime--}s 后重试`, sending: true}
                })
            } else {
                dispatch({
                    type: 'auth/clearTimer',
                })
            }
        }, 1000)

    }

    /**
     * 发送验证码
     */
    sendSms = () => {
        const {form, dispatch} = this.props;
        let {phone} = form.getFieldsValue();
        if (phone) {
            phone = phone.replace(/ /g, '');
            dispatch({
                type: 'auth/sendSms',
                payload: {phone}
            });
            this.starTimer();
        } else {
            toastValidateError(null, '手机号未填写');
        }
    };

    changePwd() {
        const {auth, form, dispatch} = this.props;

        form.validateFields({force: true}, (error) => {
            if (!error) {
                let {phone, verifycode, password, confirm} = form.getFieldsValue();
                phone = phone.replace(/ /g, '');
                dispatch({
                    type: 'auth/changePwd',
                    payload: {phone, verifycode, password, confirm, id: auth.profile.member_id}
                });
            } else {
                toastValidateError(error);
            }
        });
    }

    render() {
        const {getFieldProps} = this.props.form;
        const {auth} = this.props;
        const SenSmsBtn = ({title, loading = false, onPress}) => (
            <Button type="primary"
                    disabled={auth.sending}
                    onClick={onPress}
                    title={title}
                    style={[CommonStyle.centerVH, {height: 26, width: 100}]}>
                <Text style={{fontSize: 12, color: 'white', lineHeight: 20}}>{title}</Text>
            </Button>);
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />
                <ScrollView>
                    <List>
                        <InputItem  {...getFieldProps('phone', {
                            initialValue: (auth.isSignIn && auth.profile) ? auth.profile.phone : null,
                            rules: [
                                {required: true, message: '手机号未填写'},
                            ]
                        })}
                                    placeholder="输入手机号"
                                    labelNumber={2}
                                    type="phone"
                                    clear
                                    extra={<SenSmsBtn title={auth.sendButtonText} onPress={this.sendSms.bind(this)}/>}
                        >

                        </InputItem>
                        <InputItem  {...getFieldProps('verifycode', {
                            rules: [
                                {required: true, message: '验证码未填写'},
                            ]
                        })}
                                    placeholder="输入验证码"
                                    labelNumber={0}
                                    type="number"
                                    clear
                        >

                        </InputItem>

                        <InputItem  {...getFieldProps('password', {
                            rules: [
                                {required: true, message: '密码未填写'},
                            ]
                        })}
                                    placeholder="输入密码"
                                    labelNumber={0}
                                    type="password"
                                    clear
                        >
                        </InputItem>

                        <InputItem  {...getFieldProps('confirm', {
                            rules: [
                                {required: true, message: '请确认新密码'},
                            ]
                        })}
                                    placeholder="确认新密码"
                                    labelNumber={0}
                                    type="password"
                                    clear
                        >
                        </InputItem>
                    </List>
                    <View style={{paddingVertical: 10, paddingHorizontal: 20}}>
                        <Button type="primary"
                                onClick={this.changePwd.bind(this)}
                                title="确定"
                                style={{height: 40}}>确定</Button>
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
    header: {
        width: SCREEN.width,
        height: 139 / 375 * SCREEN.width,
    },
    avatar: {
        width: 139 / 375 * SCREEN.width / 2,
        height: 139 / 375 * SCREEN.width / 2,
        borderRadius: 139 / 375 * SCREEN.width / 4
    },
    headerContent: {
        flex: 1,
        display: 'flex',
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 14
    },
    edit: {
        backgroundColor: 'white',
        borderRadius: 15,
        width: 30,
        height: 30,
        paddingLeft: 1
    },
    userInfo: {
        display: 'flex',
        flex: 1,
        paddingLeft: 10
    },
    icItem: {
        margin: 0,
        display: 'flex',
        flexDirection: 'row',
        height: 85 / 375 * SCREEN.width,
        marginLeft: -15 / 375 * SCREEN.width,
        padding: 0
    },
    orderStatus: {
        display: 'flex',
        flexDirection: 'row',
        marginLeft: -15 / 375 * SCREEN.width,
        height: 71.7 / 357 * SCREEN.width,
        justifyContent: 'space-around'
    }

});
const Wrapper = createForm()(Page);
export default connect(
    ({auth}) => ({auth})
)(Wrapper)
