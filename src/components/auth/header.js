/**
 * Created by wtfssd on 2017/9/17.
 */
import React , { Component } from 'react'
import { StyleSheet , Text , View , ART } from 'react-native'

const { Surface , Group , Shape , Path } = ART;
import CommonStyle , { Color , SCREEN,size } from '../../assets/styles/index'
const headerSize = {
  width : SCREEN.width ,
  height : 227 / 375 * SCREEN.width ,
};
const path       = Path( `M0 0 H${SCREEN.width} V${size(180)} C${size(201.25)} ${size(120)} ${size(93.75)} ${size(220)} 0 ${size(180)}` ).close();
const header     = () => {
  return (
    <View style={[ CommonStyle.centerVH , Styles.header ]}>
      <Surface width={headerSize.width} height={headerSize.height}>
        <Group>
          <Shape d={path} stroke="#fff" strokeWidth={0} fill={Color.primary}/>
        </Group>
      </Surface>
      <Text style={Styles.headerText}>轻生活</Text>
    </View>
  )
};

const Styles = StyleSheet.create( {
  header : {
    width : headerSize.width ,
    height : headerSize.height ,
    backgroundColor : 'white' ,
    position : 'relative'
  } ,
  headerText : {
    position : 'absolute' ,
    color : 'white' ,
    backgroundColor : 'transparent' ,
    fontSize : 26 ,
  }
} );

export default header;