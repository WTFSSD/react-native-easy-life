/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {StyleSheet, Text, View, TouchableOpacity, ScrollView} from 'react-native'
import {connect} from 'react-redux'
import {List, InputItem, Button, Flex} from 'antd-mobile';
import {Actions} from 'react-native-router-flux'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import CommonStyle, {Color, SCREEN} from '../../assets/styles/index'
import Line from '../common/line'
import {createForm} from 'rc-form';
import Header from './header'
import {toastValidateError} from '../../utils/tools'

const headerSize = {
    width: SCREEN.width,
    height: 227 / 375 * SCREEN.width,
};

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {

    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }

    /**
     * 登录
     */
    signIn() {
        const {form, dispatch,from=null} = this.props;
        form.validateFields({force: true}, (error) => {
            if (!error) {
                let {phone, password} = form.getFieldsValue();
                phone = phone.replace(/ /g, '');
                dispatch({
                    type: 'auth/signIn',
                    payload: {phone, password,from}
                });
            } else {
                toastValidateError(error);
            }
        });
    }

    render() {
        const {getFieldProps} = this.props.form;
        const {loading} = this.props.auth;
        return (
            <View style={Styles.container}>
                <ScrollView>
                    <List renderHeader={<Header/>}>
                        <InputItem  {...getFieldProps('phone', {
                            rules: [
                                {required: true, message: '手机号未填写'},
                            ]
                        })}
                                    placeholder="输入手机号"
                                    labelNumber={2}
                                    type="phone"
                                    clear
                        >
                            <FontAwesome name="mobile" size={26} color={Color.primary}/>
                        </InputItem>
                        <InputItem  {...getFieldProps('password', {
                            rules: [
                                {required: true, message: '密码未填写'},
                            ]
                        })}
                                    placeholder="输入密码"
                                    labelNumber={2}
                                    type="password"
                                    clear
                        >
                            <FontAwesome name="lock" size={20} color={Color.primary}/>
                        </InputItem>


                    </List>

                    <View style={{paddingVertical: 10, paddingHorizontal: 20}}>
                        <Button type="primary"
                                onClick={this.signIn.bind(this)}
                                title="登录"
                                loading={loading}
                                disabled={loading}
                                style={{height: 40}}>登录</Button>
                        <Line height={20}/>
                        <Flex justify="between">
                            <TouchableOpacity onPress={Actions.signUp}>
                                <Text style={[CommonStyle.text1, {color: '#666'}]}>注册</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={Actions.findPwd}>
                                <Text style={[CommonStyle.text1, {color: '#666'}]}>忘记密码</Text>
                            </TouchableOpacity>
                        </Flex>
                    </View>


                </ScrollView>

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
});
const Wrapper = createForm()(Page);
export default connect(
    ({auth}) => ({auth})
)(Wrapper)





