/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {StyleSheet, Text, View, TouchableOpacity, ScrollView} from 'react-native'
import {connect} from 'react-redux'
import {List, InputItem, Button, Flex, Toast} from 'antd-mobile';
import {Actions} from 'react-native-router-flux'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MyIcon from '../common/myIcon'
import CommonStyle, {Color, SCREEN} from '../../assets/styles/index'
import Line from '../common/line'
import {createForm} from 'rc-form';
import Header from './header'
import {toastValidateError} from '../../utils/tools'
import {phoneReg} from '../../utils/Reg'

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    /*组件将要加载*/
    componentWillMount() {
        this.props.dispatch({
            type: 'auth/clearTimer',
        })
    }

    /*组件已经加载*/
    componentDidMount() {

    }

    /*组件将要卸载*/
    componentWillUnmount() {
        this.props.dispatch({
            type: 'auth/clearTimer',
        })
    }

    timer = null;

    /**
     * 开启定时器
     */
    starTimer() {
        let {auth: {sending, sendButtonText, coldDownTime,}, dispatch} = this.props;
        clearInterval(this.timer);
        if (!sending) return;

        this.timer = setInterval(() => {
            if (coldDownTime >= 0) {
                dispatch({
                    type: 'auth/save',
                    payload: {sendButtonText: `${coldDownTime--}s 后重试`, sending: true}
                })
            } else {
                dispatch({
                    type: 'auth/clearTimer',
                })
            }
        }, 1000)

    }

    /**
     * 发送验证码
     */
    sendSms = () => {
        const {auth, form, dispatch} = this.props;
        let {phone} = form.getFieldsValue();
        if (phone) {
            phone = phone.replace(/ /g, '');
            dispatch({
                type: 'auth/sendSms',
                payload: {phone}
            });
            this.starTimer();
        } else {
            toastValidateError(null, '手机号未填写');
        }

    };

    /**
     * 注册
     */
    signUp() {
        const {form, dispatch} = this.props;
        form.validateFields({force: true}, (error) => {
            if (!error) {
                let {phone, password, verifycode} = form.getFieldsValue();
                phone = phone.replace(/ /g, '');
                dispatch({
                    type: 'auth/signUp',
                    payload: {phone, password, verifycode}
                });
            } else {
                toastValidateError(error);
            }
        });
    }

    render() {
        const {getFieldProps} = this.props.form;
        const {auth} = this.props;
        const SenSmsBtn = ({title, loading = false, onPress}) => (
            <Button type="primary"
                    disabled={auth.sending}
                    onClick={onPress}
                    title={title}
                    style={[CommonStyle.centerVH, {height: 26, width: 100}]}>
                <Text style={{fontSize: 12, color: 'white', lineHeight: 20}}>{title}</Text>
            </Button>);
        return (
            <View style={Styles.container}>
                <ScrollView>
                    <List renderHeader={<Header/>}>
                        <InputItem  {...getFieldProps('phone', {
                            rules: [
                                {required: true, message: '手机号未填写'},
                            ]
                        })}
                                    placeholder="输入手机号"
                                    labelNumber={2}
                                    type="phone"
                                    clear
                                    extra={<SenSmsBtn title={auth.sendButtonText} onPress={this.sendSms.bind(this)}/>}
                        >
                            <FontAwesome name="mobile" size={26} color={Color.primary}/>
                        </InputItem>
                        <InputItem  {...getFieldProps('password', {
                                rules: [
                                    {required: true, message: '密码未填写'},
                                ]
                            }
                        )}
                                    placeholder="输入密码"
                                    labelNumber={2}
                                    type="password"
                                    clear
                        >
                            <FontAwesome name="lock" size={20} color={Color.primary}/>
                        </InputItem>

                        <InputItem  {...getFieldProps('verifycode', {
                            rules: [
                                {required: true, message: '验证码未填写'},
                            ]
                        })}
                                    placeholder="输入验证码"
                                    labelNumber={2}
                                    type="number"
                                    clear
                        >
                            <MyIcon name="baozhang" size={20} color={Color.primary}/>
                        </InputItem>
                    </List>
                    <View style={{paddingVertical: 10, paddingHorizontal: 20}}>
                        <Button type="primary"
                                onClick={this.signUp.bind(this)}
                                title="注册"
                                disabled={auth.loading}
                                loading={auth.loading}
                                style={{height: 40}}>注册</Button>
                        <Line height={20}/>
                        <Flex justify="between" direction="column">
                            <TouchableOpacity onPress={Actions.signUp}>
                                <Text
                                    style={[CommonStyle.text1, {color: '#666', marginTop: 20}]}>点击注册表示同意《用户服务协议》</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={Actions.pop}>
                                <Text style={[CommonStyle.text1, {color: '#666', marginTop: 20}]}>已有账号立即登陆</Text>
                            </TouchableOpacity>
                        </Flex>
                    </View>

                </ScrollView>

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
});
const Wrapper = createForm()(Page);
export default connect(
    ({auth}) => ({auth})
)(Wrapper)





