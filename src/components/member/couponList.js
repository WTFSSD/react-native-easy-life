/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {
    StyleSheet, Text, View, TouchableOpacity, ImageBackground, ScrollView, FlatList, Image
} from 'react-native'
import {connect} from 'react-redux'
import NavigationBar from 'react-native-navbar'
import {Toast, Icon, Flex, Tabs} from 'antd-mobile'

const TabPane = Tabs.TabPane;
import {Actions,} from 'react-native-router-flux'
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons'

import CommonStyle, {Color, SCREEN} from '../../assets/styles/index'
import {Line, BackButton} from '../common/index'
import moment from 'moment'

const bgs = [
    require('../../assets/image/couponBg.png'),
    require('../../assets/image/couponBg_gray.png'),
    require('../../assets/image/coupon.png'),
];
const tabTable = {
    'tab_0': 1,
    'tab_1': 2,
    'tab_2': 3,
};


const tipTable = [
    '暂无待使用的优惠券',
    '暂无已使用的优惠券',
    '暂无已过期的优惠券',
];

const Coupon = ({name, full, reduce, end_time, bgType = 0, onPress = null}) => {
    return (

        <ImageBackground source={bgs[bgType]} style={Styles.couponBg}>
            <TouchableOpacity style={{flex: 1, flexDirection: 'row'}} onPress={onPress} activeOpacity={1}>
                <View style={{
                    backgroundColor: 'transparent',
                    flex: 0.199,
                    marginLeft: 5,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Text style={{color: 'white', fontSize: 14}}>{name}</Text>
                </View>
                <View style={[CommonStyle.centerVH, {backgroundColor: 'transparent', flex: 1}]}>
                    <Text style={{color: 'white', fontSize: 14}}>满<Text
                        style={{color: 'white', fontSize: 20}}>{full}</Text>减<Text
                        style={{color: 'white', fontSize: 20}}>{reduce}</Text></Text>
                    <Text style={{color: 'white', fontSize: 10}} numberOfLines={2}>{`有效期至${end_time}`}</Text>
                </View>
            </TouchableOpacity>
        </ImageBackground>
    )
};

class Page extends Component {


    constructor() {
        super();
        this.state = {
            currentTab: 'tab_0',
        };
    }


    /*组件已经加载*/
    componentDidMount() {
        this.refresh();
    }

    shouldComponentUpdate(p1, s1) {

        return s1.currentTab === this.state.currentTab;
    }


    refresh = () => {
        this.props.dispatch({
            type: 'coupon/fetchCouponList',
            payload: {
                status: tabTable[this.state.currentTab]
            }
        })
    };

    onTabChange = (currentTab) => {
        this.setState({currentTab});
        this.refresh()

    };

    onCouponPress = (item) => {
        if (this.props.callBack) {
            if (moment(item.start_time).isBefore(moment())) {
                this.props.callBack(item);
                Actions.pop();
            } else {
                Toast.fail('优惠券使用时间未到，无法使用', 0.6)
            }
        }
    };

    onBack = () => {
        this.props.callBack && this.props.callBack(null);
        Actions.pop();
    };
    renderItem = (tab, {item}) => {
        // if (moment(item.start_time).isBefore(moment())) return null;

        return <Coupon {...item}
                       onPress={this.onCouponPress.bind(this, item)}
                       bgType={tab === 0 ? 0 : 1}/>;
        const Heder = () => (
            <TouchableOpacity style={Styles.item}>
                <Flex justify="between">
                    <Text style={CommonStyle.text1}>{name}</Text>
                    <Flex>
                        <Text style={CommonStyle.text2}>进入店铺</Text>
                        <SimpleLineIcon name="arrow-right" size={12} color="#666"/>
                    </Flex>
                </Flex>
            </TouchableOpacity>
        );


        return (
            <View>
                <Heder/>
                <Line height={0.5} backgroundColor="#f2f2f2"/>
                <ScrollView
                    contentContainerStyle={[CommonStyle.pdv10, {flexWrap: 'nowrap', height: 102 / 375 * SCREEN.width}]}
                    horizontal>
                    {coupons.map((item, index) => (<Coupon {...item} key={index}/>))}
                </ScrollView>
            </View>
        );
    };

    renderNoData = (tab) => {
        return (
            <View style={Styles.noData}>
                <Image source={bgs[2]}/>
                <Text style={{color: '#666', fontSize: 16, marginTop: 10}}>{tipTable[tab]}</Text>
            </View>
        )
    };


    renderList = (tab) => {
        return (
            <View style={{width: SCREEN.width, height: '100%', paddingTop: 10}}>
                <FlatList data={this.props.coupon.couponList[tab]}
                          numColumns={2}
                          onRefresh={this.refresh}
                          ListEmptyComponent={this.renderNoData.bind(this, tab)}
                          refreshing={this.props.coupon.loading}
                          ItemSeparatorComponent={() => <Line height={10} backgroundColor={'transparent'}/>}
                          keyExtractor={(i, ii) => ii}
                          renderItem={this.renderItem.bind(this, tab)}/>
            </View>

        )
    };

    render() {

        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: '我的优惠券', tintColor: 'white'}}
                               leftButton={<BackButton onPress={this.onBack}/>}

                />
                <Tabs defaultActiveKey="tab_0"
                      activeKey={this.state.currentTab}
                      onChange={this.onTabChange}>
                    <TabPane key="tab_0" tab='待使用'>
                        {this.renderList(0)}
                    </TabPane>
                    <TabPane key="tab_1" tab='已使用'>
                        {this.renderList(1)}
                    </TabPane>
                    <TabPane key="tab_2" tab='已过期'>
                        {this.renderList(2)}
                    </TabPane>
                </Tabs>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: 'rgb(245,243,244)'
    },

    item: {
        padding: 10,
    },
    btn: {
        display: 'flex',
        flexDirection: 'row',
        marginRight: 5,
        alignItems: 'center'
    },
    //w:h=2.03
    couponBg: {
        width: (SCREEN.width - 30) / 2,
        height: 82 / 375 * SCREEN.width,
        marginLeft: 10,
        // backgroundColor:'white'

    },
    noData: {
        display: 'flex',
        width: SCREEN.width,
        height: SCREEN.height - 44 - 44,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(245,243,244)'
    }

});

export default connect(
    ({auth, coupon}) => ({auth, coupon})
)(Page)





