/**
 * Created by wtfssd on 2017/9/15.
 */

import React from 'react'
import PropTypes from 'prop-types'
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native'
import {SCREEN} from '../../assets/styles/index'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

const Styles = StyleSheet.create({
    content: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row'
    },
    textContent: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    icon: {
        paddingTop: 13 / 375 * SCREEN.width,
        marginLeft: 36 / 375 * SCREEN.width
    }
});
//fa-credit-card
const icIcon = (props) => (
    <TouchableOpacity style={Styles.content} onPress={props.onPress}>
        <View style={Styles.icon}>
            <FontAwesome name={props.icon} size={20} color={props.color}/>
        </View>
        <View style={Styles.textContent}>
            <Text style={{color: props.color, fontSize: 20}}>{props.text}</Text>
            <Text style={{color: props.color, fontSize: 24}}>{props.number}</Text>
        </View>
    </TouchableOpacity>

);

icIcon.propTypes = {
    icon: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    color: PropTypes.string,
    number: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]).isRequired,
    onPress: PropTypes.func,
};
icIcon.defaultProps = {
    color: 'black',
    onPress: null
};
export default icIcon;