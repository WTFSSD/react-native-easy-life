/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {
    StyleSheet, View, FlatList, Text, TouchableOpacity
} from 'react-native'
import {connect} from 'react-redux'
import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color, SCREEN, size} from '../../assets/styles/index'
import {Line, BackButton} from '../common/index'


class Page extends React.Component {
    constructor(props, state) {
        super(props, state);
    }

    componentDidMount() {
        this.props.dispatch({
            type: 'coupon/fetchAllCoupon',
            payload: {}
        })
    }

    doExchange = (item) => {
        this.props.dispatch({
            type: 'coupon/doExchange',
            payload: {
                coupon_id: item.id
            }
        })
    };


    renderListHeaderComponent = () => {
        return (
            <View style={Styles.header}>
                <Text style={{color: 'white', fontSize: 30, fontWeight: 'bold'}}>0</Text>
                <Text style={{color: 'white', fontSize: 20}}>我的积分</Text>
            </View>
        )
    };

    renderItem = ({item}) => {
        return (
            <View style={{paddingHorizontal: 10, backgroundColor: 'rgb(245,243,244)', marginTop: 20}}>
                <View style={Styles.couponContainer}>
                    <View style={Styles.moneyContainer}>
                        <Text style={{color: 'rgb(238,61,88)', fontSize: 12}}>¥
                            <Text style={{fontSize: 16}}>{item.reduce}</Text>
                        </Text>
                    </View>
                    <View style={Styles.infoContainer}>
                        <View>
                            <Text
                                style={{
                                    color: '#333',
                                    fontSize: 14,
                                    fontWeight: '500',
                                    marginBottom: 10
                                }}>{item.name}</Text>
                            <Text style={{color: '#999', fontSize: 12}}>{`·满${item.full}减${item.reduce}`}</Text>
                            <Text style={{color: '#999', fontSize: 12}}>{`·${item.end_time}`}</Text>
                        </View>
                    </View>
                    <View style={Styles.btnContainer}>
                        <TouchableOpacity style={Styles.btn} onPress={this.doExchange.bind(this, item)}>
                            <Text style={{color: 'rgb(238,61,88)', fontSize: 12}}>兑换</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    };

    render() {
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               leftButton={<BackButton/>}
                               title={{title: '我的积分', tintColor: 'white'}}
                />
                <FlatList data={this.props.coupon.all || []}
                          keyExtractor={(item, index) => index}
                          renderItem={this.renderItem}
                          ListHeaderComponent={this.renderListHeaderComponent}
                />
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: 'rgb(245,243,244)'
    },

    header: {
        width: SCREEN.width,
        height: size(100),
        display: 'flex',
        justifyContent: 'flex-end',
        backgroundColor: Color.primary,
        padding: 10
    },
    couponContainer: {
        display: 'flex',
        flexDirection: 'row',
        borderRadius: 4,
        height: size(100),
        backgroundColor: 'white',
        shadowColor: 'black',
        shadowOffset: {width: 5, height: 5},
        shadowOpacity: 0.3,
        shadowRadius: 2
    },

    moneyContainer: {
        display: 'flex',
        flex: 0.2133,
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoContainer: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center'
    },
    btnContainer: {
        display: 'flex',
        flex: 0.26,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btn: {
        width: size(50),
        height: size(20),
        borderWidth: 1,
        borderColor: 'rgb(238,61,88)',
        borderRadius: size(10),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }

});


export default connect(
    ({auth, coupon}) => ({auth, coupon})
)(Page)