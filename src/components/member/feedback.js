/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {
    StyleSheet, Text, View, AsyncStorage, Image, TouchableOpacity, ImageBackground, ScrollView, Linking
} from 'react-native'
import {connect} from 'react-redux'
import {List, TextareaItem, InputItem, Button} from 'antd-mobile'

import {createForm} from 'rc-form';
import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color, SCREEN} from '../../assets/styles/index'
import {BackButton} from '../common/index'

import {toastValidateError} from '../../utils/tools'

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


    submit = () => {
        const {form, dispatch} = this.props;
        form.validateFields({force: true}, (error, payload) => {
            if (!error) {
                this.props.dispatch({
                    type:'sys/feedback',
                    payload
                })
            } else {
                toastValidateError(error);
            }
        });
    };

    render() {
        const {getFieldProps} = this.props.form;
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: '意见反馈', tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />
                <List>
                    <View style={Styles.whiteSpace}/>
                    <TextareaItem placeholder="输入反馈意见"
                                  rows={5}
                                  clear
                                  {...getFieldProps('content', {
                                      rules: [
                                          {required: true, message: '反馈内容未填写'},
                                      ]
                                  })}
                    />
                    <View style={Styles.whiteSpace}/>
                    <InputItem placeholder={'联系方式，便于联系'}
                               clear
                               type={'number'}
                               {...getFieldProps('title', {
                                   initialValue: null
                               })}>
                        联系方式
                    </InputItem>
                </List>
                <View style={Styles.btnContainer}>
                    <Button type={'primary'}
                            onClick={this.submit}
                            loading={this.props.sys.loading}>提交</Button>
                </View>

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#e9e9e9'
    },
    whiteSpace: {
        height: 10,
        backgroundColor: '#e9e9e9'
    },
    btnContainer: {
        backgroundColor: '#e9e9e9',
        display: 'flex',
        padding: 20
    }

});
const Wrapper = createForm()(Page);
export default connect(
    ({auth, sys}) => ({auth, sys})
)(Wrapper)





