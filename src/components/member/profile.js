/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {
    StyleSheet, Text, View, Image, ScrollView, Platform
} from 'react-native'
import {connect} from 'react-redux'
import {List, Button} from 'antd-mobile'
import {Actions} from 'react-native-router-flux'

import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color, SCREEN, size} from '../../assets/styles/index'
import {Line, BackButton} from '../common/index'
import ImagePicker from 'react-native-image-picker'


const ListItem = List.Item;
const options = {
    quality: 1.0,
    maxWidth: 300,
    maxHeight: 300,
    title: '请选择照片',
    cancelButtonTitle: '取消',
    chooseFromLibraryButtonTitle: '从相册中选择',
    takePhotoButtonTitle: '拍照',
    allowsEditing: true,
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar: 'http://pic.58pic.com/58pic/15/23/71/63b58PIC9gT_1024.jpg'
        };
    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {
    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }

    handleImagePicker() {
        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {

            }
            else if (response.error) {

            }
            else if (response.customButton) {

            }
            else {
                let source;  // 保存选中的图片

                if (Platform.OS === 'android') {
                    source = {uri: response.uri};

                } else {
                    source = {uri: response.uri.replace('file://', '')};
                }
                this.setState({
                    avatar: source.uri
                })
            }
        })
    }

    onSubmit() {
        Actions.pop();
    }


    /**
     * 退出登录
     */
    signOut() {

        this.props.dispatch({
            type: 'auth/signOut'
        });
        Actions.pop();

    }

    render() {

        const {auth} = this.props;

        console.log(auth);
        if (!auth.isSignIn) {
            return (
                <View style={Styles.container}>
                    <NavigationBar tintColor={Color.primary}
                                   statusBar={{style: 'light-content'}}
                                   title={{title: this.props.title, tintColor: 'white'}}
                                   leftButton={<BackButton/>}
                    />
                    <View style={Styles.unSignin}>
                        <Text>暂未登录</Text>
                    </View>
                </View>
            )
        }
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />
                <ScrollView>
                    <List>
                        {/*<ListItem arrow="horizontal" onClick={this.handleImagePicker.bind(this)}>*/}
                            {/*<View style={Styles.avatarContent}>*/}
                                {/*<Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>我的头像</Text>*/}
                                {/*<Image style={Styles.avatar} source={{uri: this.state.avatar}}/>*/}
                            {/*</View>*/}
                        {/*</ListItem>*/}
                        {/*<Line height={10} backgroundColor="#F2F2F2"/>*/}
                        <ListItem extra={auth.profile.member_id}>
                            <Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>我的账号</Text>
                        </ListItem>

                        <ListItem arrow="horizontal" extra={auth.profile.user_name}
                                onClick={Actions.profileEdit}>
                                      <Text
                                          style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>我的名称</Text>
                        </ListItem>

                        <ListItem arrow="horizontal" onClick={Actions.findPwd}>
                            <Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>修改密码</Text>
                        </ListItem>


                        <ListItem arrow="horizontal" extra={this.props.auth.profile.alipay?this.props.auth.profile.alipay:'请设置支付宝账号'}
                                  onClick={Actions.profileEdit.bind(this,{title:'修改支付宝账号',label:'支付宝',field:'alipay'})}>
                            <Text
                                style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>支付宝账号</Text>
                        </ListItem>

                        <ListItem arrow="horizontal" extra={this.props.auth.profile.wx_user?this.props.auth.profile.wx_user:'请设置微信账号'}
                                  onClick={Actions.profileEdit.bind(this,{title:'修改微信帐号',label:'微信',field:'wx_user'})}>
                            <Text
                                style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width,}]}>微信账号</Text>
                        </ListItem>


                    </List>
                    <View style={{padding:10,backgroundColor:'transparent'}}>
                        <Text style={{backgroundColor:'transparent',color:'#999999'}}>为方便售后，请填写支付宝或微信账号</Text>
                    </View>

                </ScrollView>

                <View style={{padding: 20}}>
                    <Button style={Styles.quitButton}
                            activeStyle={[Styles.quitButton, {opacity: 0.8}]}
                            onClick={this.signOut.bind(this)}
                    >
                        <Text style={{color: 'white', fontSize: 14}}>退出当前用户</Text>
                    </Button>
                </View>

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#f9f9f9'
    },
    header: {
        width: SCREEN.width,
        height: 139 / 375 * SCREEN.width,
    },
    avatar: {
        width: 60 / 375 * SCREEN.width,
        height: 60 / 375 * SCREEN.width,
        borderRadius: 30 / 375 * SCREEN.width,
    },
    avatarContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 5 / 375 * SCREEN.width
    },
    quitButton: {
        backgroundColor: '#1AD981',
        height: size(41),
        borderColor: '#1AD981',
        borderRadius: size(5)
    },
    unSignin: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    }

});

export default connect(
    ({auth}) => ({auth})
)(Page)





