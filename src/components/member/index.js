/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {
    StyleSheet, Text, View, AsyncStorage, Image, TouchableOpacity, ImageBackground, ScrollView, Linking
} from 'react-native'
import {connect} from 'react-redux'
import {List, WhiteSpace, Icon} from 'antd-mobile'

const ListItem = List.Item;
import {Actions} from 'react-native-router-flux'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons'

import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color, SCREEN} from '../../assets/styles/index'
import {Line, MyIcon} from '../common/index'
import ICItem from './icItem'


class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


    call = () => {
        const url = 'tel:18251811687';
        Linking.canOpenURL(url).then(supported => {
            if (!supported) {
                console.log('Can\'t handle url: ' + url);
            } else {
                return Linking.openURL(url);
            }
        }).catch(err => console.log('An error occurred', err));
    };

    goTo(senceKey, opts = {}) {
        const {auth} = this.props;
        if (!auth.isSignIn) {
            Actions.auth();
            return;
        }
        Actions.push(senceKey, opts);
    }

    /*头部*/
    renderHeader = () => {
        const {auth} = this.props;
        if (auth.isSignIn) {
            return (
                <View style={Styles.header}>
                    <TouchableOpacity style={Styles.headerContent} onPress={Actions.profile} activeOpacity={1}>
                        {/*<View>*/}
                        {/*<Image source={{uri: 'http://img1.3lian.com/2015/a2/193/d/189.jpg'}} style={Styles.avatar}/>*/}
                        {/*</View>*/}

                        <View style={Styles.userInfo}>
                            <Text
                                style={[CommonStyle.text3, {marginBottom: 2}]}>{`${auth.profile.user_name}`}</Text>
                            <Text style={CommonStyle.text3}>{`${auth.profile.phone}`}</Text>
                        </View>
                        <TouchableOpacity onPress={Actions.profile}>
                            <View style={[CommonStyle.centerVH, Styles.edit]}>
                                <FontAwesome name="edit" size={16} color={Color.primary}/>
                            </View>
                        </TouchableOpacity>

                    </TouchableOpacity>
                </View>
            )
        } else {
            return (
                <View style={Styles.header}>
                    <View style={Styles.unAuth}>
                        <TouchableOpacity onPress={Actions.auth}>
                            <View style={Styles.signInBtn}>
                                <Text>现在登录</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
    };


    /*订单状态*/
    renderOrderStatus() {
        const iconProps = {size: 25, color: Color.primary};

        const Item = ({name, text, onPress}) => (
            <TouchableOpacity style={[CommonStyle.centerVH]} onPress={onPress}>
                <MyIcon name={name} {...iconProps}/>
                <Text style={CommonStyle.text1}>{text}</Text>
            </TouchableOpacity>);
        const data = [
            {name: 'tixian1', text: '未完成', onPress: this.goTo.bind(this, 'order_unFinish')},
            {name: 'daifahuo-copy', text: '已发货', onPress: this.goTo.bind(this, 'order_sending')},
            {name: 'hua1', text: '已完成', onPress: this.goTo.bind(this, 'order_finish')},
            // {name: 'tuikuan', text: '退款/售后', onPress: this.goTo.bind(this,'order_afterSales')},
        ];
        return (
            <View style={Styles.orderStatus}>
                {data.map((item, index) => (<Item {...item} key={index}/>))}
            </View>
        )
    }

    renderIntegral_Coupon = () => {


        let coupon = 0;
        let credit = 0;
        if (this.props.auth.isSignIn) {
            coupon = this.props.auth.coupon;
            credit = this.props.auth.integral;
        }
        return (
            <View style={Styles.icItem}>
                <ICItem number={coupon} icon={'credit-card'} onPress={this.goTo.bind(this, 'couponList')} text={'优惠券'}
                        color={'#FF2B00'}/>
                <View style={Styles.separation}/>
                <ICItem number={credit} icon={'database'} onPress={this.goTo.bind(this, 'credit')} text={'积分'}
                        color={'#0D7EE8'}/>
            </View>
        )
    };

    render() {
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                />
                <ScrollView>
                    <List renderHeader={this.renderHeader}>
                        <ListItem style={{padding: 0}}>
                            {this.renderIntegral_Coupon()}
                        </ListItem>
                        <ListItem>
                            <Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>商城订单</Text>
                        </ListItem>

                        <ListItem>
                            {this.renderOrderStatus()}
                        </ListItem>

                        <Line height={10} backgroundColor="#e9e9e9"/>
                        <ListItem arrow="horizontal"
                                  thumb={<SimpleLineIcon name="location-pin" size={20} color={Color.primary}/>}
                                  onClick={this.goTo.bind(this, 'addressList')}>
                            <Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>我的地址</Text>
                        </ListItem>


                        <ListItem arrow="horizontal"
                                  thumb={<SimpleLineIcon name="heart" size={20} color={Color.primary}/>}
                                  onClick={this.goTo.bind(this, 'fav')}>
                            <Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>我的收藏</Text>
                        </ListItem>

                        <ListItem arrow="horizontal"
                                  onClick={Actions.recommend}
                                  thumb={<FontAwesome name="qrcode" size={20} color={Color.primary}/>}>
                            <Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>我的推荐</Text>
                        </ListItem>
                        <ListItem arrow="horizontal" thumb={<MyIcon name="dianhua" size={20} color={Color.primary}/>}
                                  onClick={this.goTo.bind(this, 'contact')}>
                            <Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>联系我们</Text>
                        </ListItem>
                    </List>

                </ScrollView>


            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
    header: {
        width: SCREEN.width,
        height: 139 / 375 * SCREEN.width,
    },
    avatar: {
        width: 139 / 375 * SCREEN.width / 2,
        height: 139 / 375 * SCREEN.width / 2,
        borderRadius: 139 / 375 * SCREEN.width / 4
    },
    headerContent: {
        flex: 1,
        display: 'flex',
        backgroundColor: '#0D7EE8',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 14
    },
    edit: {
        backgroundColor: 'white',
        borderRadius: 15,
        width: 30,
        height: 30,
        paddingLeft: 1
    },
    userInfo: {
        display: 'flex',
        flex: 1,
        paddingLeft: 10
    },
    icItem: {
        margin: 0,
        display: 'flex',
        flexDirection: 'row',
        height: 85 / 375 * SCREEN.width,
        // marginLeft: -15 / 375 * SCREEN.width,
        padding: 0
    },
    orderStatus: {
        display: 'flex',
        flexDirection: 'row',
        marginLeft: -15 / 375 * SCREEN.width,
        height: 71.7 / 357 * SCREEN.width,
        justifyContent: 'space-around'
    },
    unAuth: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.primary
    },
    signInBtn: {
        width: 100,
        height: 40,
        borderRadius: 20,
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    separation: {
        width: 1,
        backgroundColor: '#D8D8D8'
    }
});

export default connect(
    ({auth, coupon}) => ({auth, coupon})
)(Page)





