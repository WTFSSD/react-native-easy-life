/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {StyleSheet, Text, View, Image, FlatList} from 'react-native'

import {connect} from 'react-redux'
import {Tabs, List, Flex} from 'antd-mobile'

import {Actions} from 'react-native-router-flux'

import * as request from '../../utils/request';
import {BackButton} from '../common/index'
import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color, SCREEN} from '../../assets/styles/index'


const TabPane = Tabs.TabPane;
const ListItem = List.Item;

class Page extends Component {


    constructor(props) {
        super(props);
        this.state = {
            currentTab: '0',
        };
    }

    /*组件已经加载*/
    componentDidMount() {
        this.onRefresh();
    }

    shouldComponentUpdate(p1, s1) {
        return s1.currentTab === this.state.currentTab;
    }

    onRefresh = () => {
        const {auth, dispatch} = this.props;
        console.log('2334534', this.state);
        if (Number(this.state.currentTab) === 0) {
            dispatch({type: 'auth/fetchFavList', payload: {id: auth.profile.member_id}});
        }
        if (Number(this.state.currentTab) === 1) {
            dispatch({type: 'auth/fetchStoreFavList', payload: {}})
        }
    };
    onTabChange = currentTab => {

        this.setState({currentTab});
        const {auth, dispatch} = this.props;
        if (Number(currentTab) === 0) {
            dispatch({type: 'auth/fetchFavList', payload: {id: auth.profile.member_id}});
        }
        if (Number(currentTab) === 1) {
            dispatch({type: 'auth/fetchStoreFavList', payload: {}})
        }
    };


    renderItem({item}) {
        const {currentTab} = this.state;


        if (!item) return null;
        if (Number(currentTab) === 0) {
            return (
                <ListItem arrow="horizontal"
                          onClick={Actions.productDetail.bind(null, {store_id: item.store_id, id: item.id})}
                          thumb={<Image source={{uri: item.preview}} style={Styles.icon}/>}>
                    <Flex direction="column" align="start" justify="center" style={Styles.item}>
                        <Text>{item.name}</Text>
                        <Text style={[CommonStyle.priceText, {
                            fontSize: 14,
                            marginTop: 6
                        }]}>¥{Number(item.price).toFixed(2)}</Text>
                    </Flex>
                </ListItem>
            )
        }
        if (!item.get_collects) return null;
        return (
            <ListItem arrow="horizontal"
                      onClick={Actions.shopDetail.bind(null, {store_id: item.get_collects.id})}
                      thumb={<Image source={{uri: request.checkImageUrl(item.get_collects.store_image)}}
                                    style={Styles.icon}/>}>
                <Flex direction="column" align="start" justify="center" style={Styles.item}>
                    <Text>{item.get_collects.store_name}</Text>
                </Flex>
            </ListItem>
        )
    }


    render() {
        const {auth, dispatch, store} = this.props;
        const {currentTab} = this.state;
        const list = (<View style={Styles.tabContent}>
            <FlatList data={Number(currentTab) === 0 ? auth.productFavList : auth.storeFavList}
                      extraData={this.state}
                      style={{flex: 1}}
                      onRefresh={this.onRefresh}
                      refreshing={auth.loading}
                      keyExtractor={(item, index) => item.id}
                      renderItem={this.renderItem.bind(this)}

            />
        </View>);

        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: '我的收藏', tintColor: 'white'}}
                               leftButton={<BackButton/>}

                />
                <Tabs defaultActiveKey="0"
                      activeKey={this.state.currentTab}
                      onChange={this.onTabChange}>
                    <TabPane key="0" tab='商品收藏'>
                        {list}
                    </TabPane>
                    <TabPane key="1" tab='店铺收藏'>
                        {list}
                    </TabPane>
                </Tabs>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
    tabContent: {
        width: SCREEN.width,
        height: '100%',
    },
    icon: {
        width: 50 / 375 * SCREEN.width,
        height: 50 / 375 * SCREEN.width,
        borderRadius: 25 / 375 * SCREEN.width
    },
    item: {
        height: 70 / 375 * SCREEN.width,
        paddingHorizontal: 10,
    },
});

export default connect(
    ({auth, store}) => ({auth, store})
)(Page)





