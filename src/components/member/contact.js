/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {
    StyleSheet, Text, View, AsyncStorage, Image, TouchableOpacity, ImageBackground, ScrollView, Linking
} from 'react-native'
import {connect} from 'react-redux'
import {List, WhiteSpace, Icon} from 'antd-mobile'

const ListItem = List.Item;
const ListItemBrief = List.Item.Brief;
import {Actions, ActionConst} from 'react-native-router-flux'
import PropTypes from 'prop-types'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons'

import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color, SCREEN} from '../../assets/styles/index'
import {BackButton, Line, MyIcon} from '../common/index'
import ICItem from './icItem'

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    /*组件将要加载*/
    componentWillMount() {
    }


    /*组件已经加载*/
    componentDidMount() {
    }

    /*组件将要卸载*/
    componentWillUnmount() {
    }


    call = () => {
        const url = 'tel:18251811687';
        Linking.canOpenURL(url).then(supported => {
            if (!supported) {
                console.log('Can\'t handle url: ' + url);
            } else {
                return Linking.openURL(url);
            }
        }).catch(err => console.log('An error occurred', err));
    };

    goTo(senceKey, opts = {}) {
        Actions.push(senceKey, opts);
    }


    render() {

        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: '联系客服', tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />

                <List>


                    <Line height={10} backgroundColor="#e9e9e9"/>


                    <ListItem arrow="horizontal" onClick={this.call}>
                        <Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>客服电话</Text>
                    </ListItem>

                    <ListItem extra='18251811687'>
                        <Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>客服微信</Text>
                    </ListItem>
                    <ListItem arrow="horizontal" onClick={this.goTo.bind(this, 'feedback')}>
                        <Text style={[CommonStyle.text1, {marginLeft: 5.4 / 375 * SCREEN.width}]}>意见反馈</Text>
                    </ListItem>
                </List>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#e9e9e9'
    },
    header: {
        width: SCREEN.width,
        height: 139 / 375 * SCREEN.width,
    },
    avatar: {
        width: 139 / 375 * SCREEN.width / 2,
        height: 139 / 375 * SCREEN.width / 2,
        borderRadius: 139 / 375 * SCREEN.width / 4
    },
    headerContent: {
        flex: 1,
        display: 'flex',
        backgroundColor: '#0D7EE8',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 14
    },
    edit: {
        backgroundColor: 'white',
        borderRadius: 15,
        width: 30,
        height: 30,
        paddingLeft: 1
    },
    userInfo: {
        display: 'flex',
        flex: 1,
        paddingLeft: 10
    },
    icItem: {
        margin: 0,
        display: 'flex',
        flexDirection: 'row',
        height: 85 / 375 * SCREEN.width,
        marginLeft: -15 / 375 * SCREEN.width,
        padding: 0
    },
    orderStatus: {
        display: 'flex',
        flexDirection: 'row',
        marginLeft: -15 / 375 * SCREEN.width,
        height: 71.7 / 357 * SCREEN.width,
        justifyContent: 'space-around'
    },
    unAuth: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.primary
    },
    signInBtn: {
        width: 100,
        height: 40,
        borderRadius: 20,
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }

});

export default connect(
    ({auth}) => ({auth})
)(Page)





