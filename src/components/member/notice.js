/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {StyleSheet, Text, View, AsyncStorage, Image, TouchableOpacity, FlatList} from 'react-native'
import {connect} from 'react-redux'
import {List, WhiteSpace, Icon, Flex} from 'antd-mobile'
import {Actions, ActionConst} from 'react-native-router-flux'
import PropTypes from 'prop-types'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons'
import MyIcon from '../common/myIcon'
import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color, SCREEN} from '../../assets/styles/index'
import Line from '../common/line'

/*
const data = [
    {
        id: '1',
        title: '轻生活店铺更新啦！',
        content: '店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。',
        time: '2017-09-01 11:41:23 '
    },
    {
        id: '2',
        title: '轻生活店铺更新啦！',
        content: '店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。',
        time: '2017-09-01 11:41:23 '
    },
    {
        id: '3',
        title: '轻生活店铺更新啦！',
        content: '店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。',
        time: '2017-09-01 11:41:23 '
    },
    {
        id: '4',
        title: '轻生活店铺更新啦！',
        content: '店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。',
        time: '2017-09-01 11:41:23 '
    },
    {
        id: '5',
        title: '轻生活店铺更新啦！',
        content: '店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。',
        time: '2017-09-01 11:41:23 '
    },
    {
        id: '6',
        title: '轻生活店铺更新啦！',
        content: '店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。',
        time: '2017-09-01 11:41:23 '
    },
    {
        id: '7',
        title: '轻生活店铺更新啦！',
        content: '店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。',
        time: '2017-09-01 11:41:23 '
    },
    {
        id: '8',
        title: '轻生活店铺更新啦！',
        content: '店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。',
        time: '2017-09-01 11:41:23 '
    },
    {
        id: '9',
        title: '轻生活店铺更新啦！',
        content: '店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。',
        time: '2017-09-01 11:41:23 '
    },

];
*/
const  data = [];

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {

    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }

    renderItem({item: {title, content, id, time}}) {
        return (
            <TouchableOpacity onPress={Actions.noticeDetail.bind(null, {id})}>
                <View style={{padding: 10}}>
                    <Flex justify="between">
                        <Text style={[CommonStyle.text1]}>{title}</Text>
                        <Text style={[CommonStyle.text2]}>{time}</Text>
                    </Flex>
                    <Text style={[CommonStyle.text2, {marginTop: 10}]}>{content}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        const LeftButton = (
            <TouchableOpacity onPress={Actions.pop}>
                <View style={[CommonStyle.centerVH, {height: 44, paddingHorizontal: 10}]}>
                    <SimpleLineIcon name="arrow-left" color="white"/>
                </View>
            </TouchableOpacity>
        );
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                               leftButton={LeftButton}
                />

                {data.length>0? <FlatList data={data}
                                          extraData={this.state}
                                          style={{flex: 1}}
                                          ItemSeparatorComponent={Line.bind(null, {height: 1, backgroundColor: '#e9e9e9'})}
                                          renderItem={this.renderItem.bind(this)}
                                          keyExtractor={(item, index) => item.id}
                />:<View style={Styles.noData}>
                    <Text>暂无数据</Text>
                </View>}


            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#f9f9f9'
    },

    noData: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    }

});

export default connect(
    ({auth}) => ({auth})
)(Page)





