/**
 * Created by wtfssd on 2017/9/15.
 */

import React , { Component } from 'react'
import {
  StyleSheet , Text , View , AsyncStorage , Image , TouchableOpacity , ImageBackground , ScrollView , Platform
} from 'react-native'
import { connect } from 'react-redux'
import { Flex } from 'antd-mobile'
import { Actions , ActionConst } from 'react-native-router-flux'
import PropTypes from 'prop-types'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons'
import MyIcon from '../common/myIcon'
import NavigationBar from 'react-native-navbar'
import CommonStyle , { Color , SCREEN } from '../../assets/styles/index'
import Line from '../common/line'

class Page extends Component {
  constructor( props ) {
    super( props );
    this.state = {};
  }

  /*组件将要加载*/
  componentWillMount() {

  }

  /*组件已经加载*/
  componentDidMount() {

  }

  /*组件将要卸载*/
  componentWillUnmount() {

  }

  render() {

    const LeftButton = (
      <TouchableOpacity onPress={Actions.pop}>
        <View style={[ CommonStyle.centerVH , { height : 44 , paddingHorizontal : 10 } ]}>
          <SimpleLineIcon name="arrow-left" color="white"/>
        </View>
      </TouchableOpacity>
    );
    return (
      <View style={Styles.container}>
        <NavigationBar tintColor={Color.primary}
                       statusBar={{ style : 'light-content' }}
                       title={{ title : this.props.title , tintColor : 'white' }}
                       leftButton={LeftButton}
        />
        <View style={{ padding : 10 , backgroundColor : 'white' }}>
          <Flex justify="between">
            <Text style={[ CommonStyle.text1 ]}>轻生活店铺更新啦！</Text>
            <Text style={[ CommonStyle.text2 ]}>2017-09-01 11:41:23 </Text>
          </Flex>
          <Text style={[ CommonStyle.text2 , { marginTop : 10 } ]}>店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。
            店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。
            店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。
            店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。
            店铺可以自主设置优惠卷，优惠卷只能是现金劵并且必须满足在该店铺消费满一定金额才能使用。

          </Text>
        </View>
      </View>
    )
  }
}

const Styles = StyleSheet.create( {
  container : {
    display : 'flex' ,
    flex : 1 ,
    backgroundColor : '#f9f9f9'
  } ,
} );

export default connect(
  ( { auth } ) => ({ auth })
)( Page )





