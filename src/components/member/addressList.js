/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {
    StyleSheet, Text, View, TouchableOpacity, FlatList
} from 'react-native'
import {connect} from 'react-redux'
import {Flex} from 'antd-mobile'
import {Actions} from 'react-native-router-flux'
import Ionicon from 'react-native-vector-icons/Ionicons'
import {MyIcon, Line, BackButton, CheckBox} from '../common/index'
import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color} from '../../assets/styles/index'

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    /*组件将要加载*/
    componentWillMount() {
        this.fetchData()
    }

    /*组件已经加载*/
    componentDidMount() {
        this.fetchData()
    }

    fetchData() {

        this.props.dispatch({
            type: 'auth/fetchAddressList',
            payload: {user_id: this.props.auth.profile.member_id}
        })
    }

    deleteAddress(item) {
        this.props.dispatch({
            type: 'auth/addressDelete',
            payload: {user_id: this.props.auth.profile.member_id, id: item.id}
        })
    }

    changeDefaultAddress(item, status) {
        console.log(item);
        this.props.dispatch({
            type: 'auth/addAddress',
            payload: {...item, user_id: this.props.auth.profile.member_id, default_address: status ? 1 : 0}
        })
    }

    /*组件将要卸载*/
    componentWillUnmount() {
    }

    onItemPress(item) {
        const {type, onPicked} = this.props.data;
        if (type === 'pick') {
            Actions.pop();
            onPicked && onPicked(item);
        }
    }

    renderItem = ({item}) => {
        return (
            <TouchableOpacity style={Styles.item} onPress={this.onItemPress.bind(this, item)}>
                <Flex justify="between" style={CommonStyle.pdv5}>
                    <Text style={{fontSize: 16, color: '#000'}}>{item.accepter}</Text>
                    <Text style={{fontSize: 16, color: '#000'}}>{item.telphone}</Text>
                </Flex>
                <Text style={[CommonStyle.pdb5, CommonStyle.text2, {fontSize: 14}]}>{item.address}</Text>
                <Text style={[CommonStyle.pdb5, CommonStyle.text2, {
                    fontSize: 14,
                    marginTop: 5
                }]}>{item.address_detail}</Text>
                <Line backgroundColor="#d0d0d0" height={0.5}/>
                <Flex justify="end" align="center" style={[CommonStyle.pdt10]}>
                    <TouchableOpacity onPress={Actions.addressEdit.bind(null, {editType: 'edit', address: {...item}})}>
                        <View style={Styles.btn}>
                            <MyIcon name="bianji" size={20} color="#666"/>
                            <Text style={[CommonStyle.text1]}>编辑</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.deleteAddress.bind(this, item)}>
                        <View style={Styles.btn}>
                            <MyIcon name="shanchu" size={20} color="#666"/>
                            <Text style={[CommonStyle.text1]}>删除</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={Styles.btn}>
                        <CheckBox color='#666' style={{marginRight: 2}} checked={item.default_address === 1}
                                  onChange={this.changeDefaultAddress.bind(this, item)}/>
                        <Text style={[CommonStyle.text1]}>默认地址</Text>
                    </View>
                </Flex>
            </TouchableOpacity>
        );
    };

    render() {
        const RightButton = (
            <TouchableOpacity onPress={Actions.addressEdit.bind(null, {editType: 'add'})}>
                <View style={[CommonStyle.centerVH, {height: 44, paddingHorizontal: 10}]}>
                    <Ionicon name="ios-add" color="white" size={30}/>
                </View>
            </TouchableOpacity>
        );
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}

                               leftButton={<BackButton/>}
                               rightButton={RightButton}
                />
                {(this.props.auth.addressList && this.props.auth.addressList.length > 0) ?
                    <FlatList data={this.props.auth.addressList}
                              extraData={this.state}
                              style={{flex: 1}}
                              numColumns={1}
                              onRefresh={this.fetchData.bind(this)}
                              refreshing={this.props.auth.loading}
                              ItemSeparatorComponent={Line.bind(null, {height: 1, backgroundColor: '#e9e9e9'})}
                              renderItem={this.renderItem.bind(this)}
                              keyExtractor={(item, index) => item.id}
                    /> : <View style={Styles.noData}>
                        <Text>暂无收货地址！</Text>
                    </View>}

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },

    item: {
        padding: 10,
    },
    btn: {
        display: 'flex',
        flexDirection: 'row',
        marginRight: 5,
        alignItems: 'center'
    },

    noData: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});


const dataType = {
    type: PropTypes.oneOf([
        'pick',
        ''
    ]),
    onPicked: PropTypes.func
};
Page.propTypes = {
    data: PropTypes.shape(dataType),
};

Page.defaultProps = {
    data: {
        type: '',
        onPicked: () => {
        }
    }
};
export default connect(
    ({auth}) => ({auth})
)(Page)





