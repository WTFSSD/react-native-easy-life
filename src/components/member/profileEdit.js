/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native'
import {connect} from 'react-redux'
import {InputItem, List} from 'antd-mobile'
import {Actions} from 'react-native-router-flux'
import PropTypes from 'prop-types'
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons'
import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color} from '../../assets/styles/index'


class Page extends Component {

    text=null;
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {

    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }

    onSubmit() {


        let payload = {
            phone: this.props.auth.profile.phone
        };
        if(!this.text){
            return;
        }
        payload[this.props.field] = this.text;
        this.props.dispatch({
            type: 'auth/changeUserName',
            payload
        })
    }


    onChange = (text)=>{
      this.text = text;
    };

    render() {


        const RightButton = (
            <TouchableOpacity onPress={this.onSubmit.bind(this)}>
                <View style={[CommonStyle.centerVH, {height: 44, paddingHorizontal: 10}]}>
                    <Text style={[CommonStyle.text1, {color: 'white'}]}>确定</Text>
                </View>
            </TouchableOpacity>
        );
        const LeftButton = (
            <TouchableOpacity onPress={Actions.pop}>
                <View style={[CommonStyle.centerVH, {height: 44, paddingHorizontal: 10}]}>
                    <SimpleLineIcon name="arrow-left" color="white"/>
                </View>
            </TouchableOpacity>
        );
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                               rightButton={RightButton}
                               leftButton={LeftButton}
                />
                <List style={{flex: 1}}>
                    <InputItem
                        clear
                        placeholder={`输入${this.props.label}`}
                        autoFocus
                        onChange={this.onChange}
                    >{this.props.label}</InputItem>
                </List>
            </View>
        )
    }
}

Page.defaultProps = {
    label: '我的名称',
    field: 'username',
    onSubmit: () => {
    }
};
Page.propTypes = {
    label: PropTypes.string,
    field: PropTypes.string,
    onSubmit: PropTypes.func,
};

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#f9f9f9'
    },

});

export default connect(
    ({auth}) => ({auth})
)(Page)





