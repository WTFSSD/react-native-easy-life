/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {
    StyleSheet, Text, View, Image, ScrollView, Platform
} from 'react-native'
import {connect} from 'react-redux'
import {List, Button} from 'antd-mobile'
import {Actions} from 'react-native-router-flux'

import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color, SCREEN, size} from '../../assets/styles/index'
import {Line, BackButton} from '../common/index'
import ImagePicker from 'react-native-image-picker'


const ListItem = List.Item;
import appInfo from '../../../app.json'

class Page extends Component {
    constructor(props) {
        super(props);

    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {
    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }


    render() {


        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />
                <View style={{display: 'flex', flex: 1, justifyContent: 'center', alignItems: 'center',position:'relative'}}>
                    <Text style={{marginBottom:20,color:'gray'}}>仅限iPhone用户使用</Text>
                    <Image source={require('../../assets/image/1510729381.png')} style={{
                        width: SCREEN.width*0.8,
                        height: SCREEN.width*0.8
                    }}/>

                    <View style={{position:'absolute',bottom:10,right:10}}>
                        <Text style={{color:'gray',fontSize:14}}>{appInfo.version}</Text>
                    </View>
                </View>

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#f9f9f9'
    },
    header: {
        width: SCREEN.width,
        height: 139 / 375 * SCREEN.width,
    },
    avatar: {
        width: 60 / 375 * SCREEN.width,
        height: 60 / 375 * SCREEN.width,
        borderRadius: 30 / 375 * SCREEN.width,
    },
    avatarContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 5 / 375 * SCREEN.width
    },
    quitButton: {
        backgroundColor: '#1AD981',
        height: size(41),
        borderColor: '#1AD981',
        borderRadius: size(5)
    },
    unSignin: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    }

});

export default connect(
    ({auth}) => ({auth})
)(Page)





