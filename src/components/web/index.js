/**
 * Created by wtfssd on 2017/9/15.
 */

import React, { Component} from 'react'
import {StyleSheet, Text, View, WebView} from 'react-native'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'
import {Actions, ActionConst} from 'react-native-router-flux'
import CommonStyle,{Color} from '../../assets/styles/index'
import {BackButton} from '../common/index'

const patchPostMessageFunction = function () {
    const originalPostMessage = window.postMessage;

    const patchedPostMessage = function (message, targetOrigin, transfer) {
        originalPostMessage(message, targetOrigin, transfer);
    };

    patchedPostMessage.toString = function () {
        return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
    };

    window.postMessage = patchedPostMessage;
};

const patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';

class Page extends Component {
    constructor(props) {
        console.log(props);
        super(props);
        this.state = {
            canGoBack:false,
            canGoForward:false,
            loading:false,
            title:props.title?title:''
        };
        this.web = null;
    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {

    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }

    onBack(e){
        // if(this.state.canGoBack){
        //     this.web.goBack()
        // }else {
        //    Actions.pop();
        // }
        console.log(this.web);
    }
    onNavigationStateChange(e){
        console.log('onNavigationStateChange',e);

        this.setState({...e})
    }
    onMessage(e){
        console.log('onMessage',e);
    }
    onLoadEnd(e){
        console.log('onLoadEnd',e.nativeEvent);
        this.setState({...e.nativeEvent})
    }

    renderTitle(){
        return(
            <View style={Styles.titleContainer}>
                <Text style={Styles.title} numberOfLines={1}>
                    {this.state.title}
                </Text>
            </View>

        )
    }
    render() {
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={this.renderTitle()}
                               leftButton={<BackButton onPress={this.onBack.bind(this)}/>}
                />
                <WebView style={{flex: 1}}
                         ref={web => { this.web = web; }}
                         injectedJavaScript={patchPostMessageJsCode}
                         javaScriptEnabled
                         domStorageEnabled
                         source={this.props.source}
                         onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                         onMessage = {this.onMessage.bind(this)}
                         onLoadEnd = {this.onLoadEnd.bind(this)}
                />
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
    title:{
        color:'white',
        fontSize:14,
    },
    titleContainer:{
        width:'80%',
        height:30,
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
    }
});

Page.propTypes = {
    title:PropTypes.string,
    source:PropTypes.object.isRequired
};

export default connect(
    ({home}) => ({home})
)(Page)



