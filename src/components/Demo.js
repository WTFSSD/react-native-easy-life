/**
 * Created by wtfssd on 2017/9/15.
 */

import React , { PropTypes , Component } from 'react'
import { StyleSheet , Text , View , AsyncStorage , TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import {} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'
import { Actions , ActionConst } from 'react-native-router-flux'

class Page extends Component {
  constructor( props ) {
    super( props );
    this.state = {};
  }

  /*组件将要加载*/
  componentWillMount() {

  }

  /*组件已经加载*/
  componentDidMount() {

  }

  /*组件将要卸载*/
  componentWillUnmount() {

  }

  render() {
    return (
      <View style={Styles.container}>
        <Text>Demo</Text>
      </View>
    )
  }
}

const Styles = StyleSheet.create( {
  container : {
    display : 'flex' ,
    flex : 1 ,
    justifyContent : 'center' ,
    alignItems : 'center' ,
  }
} );

export default connect(
  ( { home } ) => ({ home })
)( Page )



