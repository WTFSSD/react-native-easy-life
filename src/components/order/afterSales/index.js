/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {StyleSheet, View, SectionList} from 'react-native'
import {connect} from 'react-redux'
import {Tabs} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'
import {Actions, ActionConst} from 'react-native-router-flux'
import data from '../data'
import CommonStyle, {Color, size} from '../../../assets/styles/index'
import {BackButton, Line} from '../../common/index'
import {TopBar, Product, Header, Footer, Loading} from '../common/index'

const TabPane = Tabs.TabPane;

class Page extends Component {
    isSends = [6, 7];
    currentTab = 0;

    constructor(props) {
        super(props);
        this.state = {
            data,
            refreshing: false,
        };
        this.timer = null
    }


    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {

    }

    /*组件将要卸载*/
    componentWillUnmount() {
        clearTimeout(this.timer);
    }


    onRefresh = (currentIndex) => {
        if (typeof  currentIndex !== 'undefined') {
            this.currentTab = currentIndex;
        }
        this.fetchData();
    };

    fetchData = () => {
        this.props.dispatch({
            type: 'order/fetchOrderList',
            payload: {
                issend: this.isSends[this.currentTab]
            }
        })
    };

    render() {
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: '退款/售后', tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />
                <TopBar titles={['处理中', '退款成功']} onChange={this.onRefresh}/>
                <SectionList sections={this.props.order.list.map((item) => ({...item, data: item.get_order_info}))}
                             renderSectionHeader={Header}
                             renderSectionFooter={(item) => (<Footer {...item} action={{text: '退款', onPress: null}}/>)}
                             keyExtractor={(item, index) => item.id}
                             refreshing={this.props.order.loading}
                             onRefresh={this.onRefresh}
                             style={Styles.list}
                             ItemSeparatorComponent={Line.bind(null, {height: 1})}
                             renderItem={(i) => <Product {...i}/>}
                             keyboardDismissMode="on-drag"
                />
            </View>
        )
    }
}

// renderSectionFooter={Footer}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
    list: {
        flex: 1,
    }
});

export default connect(
    ({auth, order}) => ({auth, order})
)(Page)
