/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {PropTypes, Component} from 'react'
import {StyleSheet, Text, View, TextInput, TouchableOpacity,Image} from 'react-native'
import {connect} from 'react-redux'
import {Button, Flex} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'
import {Actions, ActionConst} from 'react-native-router-flux'
import {BackButton, MyIcon} from '../../common/index'
import CommonStyle, {Color, size} from '../../../assets/styles/index'

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


    render() {
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: '退款售后', tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />

                <View style={Styles.header}>
                    <MyIcon name='tuihuoshenqingdan' color={Color.primary} size={25}/>
                    <Text style={{color: Color.primary, fontSize: 16, flex: 1, marginLeft: 10}}>提交申请后请及时与卖家联系，卖家同意退款后，钱款会在2-3工作日返回您账户。</Text>
                </View>



                <View style={{padding:24}}>
                    <Text style={{color:'red'}}>{'*   '}<Text style={CommonStyle.text1}>申请说明：</Text></Text>
                </View>
                <View style={{paddingHorizontal:35}}>
                    <TextInput placeholder='输入退款原因'
                               multiline
                               style={Styles.input}
                               onChangeText={(value) => {
                                   this.setState({value})
                               }}
                    />
                </View>

                <View style={[CommonStyle.centerVH, {paddingTop: size(42)}]}>
                    <Button style={Styles.btn} activeStyle={[Styles.btn, {opacity: 0.8}]}>
                        <Text style={{color: 'white', fontSize: 16}}>提交</Text>
                    </Button>
                </View>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
    header: {
        backgroundColor: '#F0F2F5',
        width: size(375),
        display: 'flex',
        flexDirection: 'row',
        padding: 22
    },
    input: {
        borderWidth: 1,
        borderColor: '#d0d0d0',
        minHeight: size(137.9),
        fontSize: 14,
        padding: 10,
        borderRadius:5
    },
    btn: {
        width: size(180),
        height: size(33),
        backgroundColor: Color.primary,
        borderColor: Color.primary,
    },
});

export default connect(
    ({auth,order}) => ({auth,home})
)(Page)



