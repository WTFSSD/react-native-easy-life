/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {PropTypes, Component} from 'react'
import {StyleSheet, Text, View, AsyncStorage, TouchableOpacity, SectionList, Image, ScrollView} from 'react-native'
import {connect} from 'react-redux'
import {List, Flex} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'
import {Actions, ActionConst} from 'react-native-router-flux'
import CommonStyle, {Color, size} from '../../../assets/styles/index'
import {BackButton, Line, LoadingView} from '../../common/index'

import {Product, Footer, Header} from '../common/index'
import data from '../data'

const ListItem = List.Item;

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [data[0]],
            refreshing: false,
        };
    }

    /*组件将要加载*/
    componentDidMount() {
        this.props.dispatch({
            type: 'order/fetchDetail',
            payload: {order_id: this.props.order_id}
        })
    }


    onRefresh = () => {

    };


    /**
     * 收货人
     * @return {XML}
     */
    renderListHeader = () => {
        let {get_user, get_adder} = this.props.order.detail;
        if (!get_adder) {
            get_adder = {
                address: '',
                address_detail: ''
            }
        }
        return (
            <View>
                <View style={Styles.listHeader}>
                    <Text style={[CommonStyle.text1, {fontSize: 16}]}>{get_user.username} <Text>{get_user.phone}</Text></Text>
                    <Text style={[CommonStyle.text2, {fontSize: 13, paddingVertical: 4}]}
                          numberOfLines={2}>{`${get_adder.address}${get_adder.address_detail}`}</Text>
                </View>
                <Image source={require('../../../assets/image/color.png')}
                       style={{height: 6.5, backgroundColor: 'white'}}/>
                <Line height={10} backgroundColor="#F2F2F2"/>
            </View>
        )
    };

    renderListFooter = () => {
        const {get_big, order_number, created_at} = this.props.order.detail;
        return (
            <List>
                <ListItem>
                    <Text style={CommonStyle.text2}>{`买家留言:${get_big ? get_big.leaving_message : ''}`}</Text>
                </ListItem>
                <ListItem>
                    <Flex direction='column' align='start'>
                        <Text style={CommonStyle.text2}>订单编号：{order_number}</Text>
                        <Line backgroundColor={'#f4f4f4'}/>
                        <Text style={CommonStyle.text2}>下单时间：{created_at}</Text>
                    </Flex>
                </ListItem>

            </List>
        )
    };


    renderContent = () => {
        if (!this.props.order.detail) return null;
        console.log(this.props.order.detail);
        return <SectionList sections={[{data: this.props.order.detail.get_order_info, ...this.props.order.detail}]}
                            ListHeaderComponent={this.renderListHeader}
                            ListFooterComponent={this.renderListFooter}
                            renderSectionFooter={(item) => (<Footer {...item}/>)}
                            renderSectionHeader={(item) => (<Header {...item}/>)}

                            keyExtractor={(item, index) => item.id}
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh}
                            style={Styles.list}
                            ItemSeparatorComponent={Line.bind(null, {height: 1})}
                            renderItem={(item) => (
                                <Product {...item} evaluate={true} onEvaluate={Actions.order_evaluate}/>)}
                            keyboardDismissMode="on-drag"
        />

    };

    render() {
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />
                <LoadingView loading={this.props.order.loading}>
                    {this.renderContent()}
                </LoadingView>

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
    listHeader: {
        display: 'flex',
        padding: 10,
        backgroundColor: 'white'
    },
    list: {
        flex: 1,
    }
});

export default connect(
    ({home, order}) => ({home, order})
)(Page)



