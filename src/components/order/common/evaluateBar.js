import React from 'react'
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native'
import PropTypes from 'prop-types'
import {Flex} from 'antd-mobile'
import CommonStyle, {size} from '../../../assets/styles/index'
import { Line } from '../../common/index'
const evaluate = props=>(
    <Flex justify='end' style={{padding:10}}>
        <TouchableOpacity style={[CommonStyle.centerVH,Styles.evaluate]} onPress={props.onPress}>
            <Text size={{color:'#999',fontSize:12}}>去评价</Text>
        </TouchableOpacity>
    </Flex>
);
const Styles = StyleSheet.create({
    evaluate:{
        width:size(75.2),
        height:size(27.2),
        borderWidth:1,
        borderColor:'#999',
        borderRadius:size(27.2/2)
    }
});
evaluate.propTypes = {
    onPress:PropTypes.func,
};
evaluate.defaluProps = {
    onPress:()=>{}
};
export default evaluate;