import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity, Linking} from 'react-native'
import PropTypes from 'prop-types'

import CommonStyle, {size, Color} from '../../../assets/styles/index'
import {Toast} from 'antd-mobile'
import {Line} from '../../common/index'

class footer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
        }

    }

    componentWillUnmount() {
        this.onClose();
    }

    onClose = () => {
        this.setState({modal: false})
    };
    showModal = () => {
        this.setState({modal: true})
    };
    call = () => {
        Linking.canOpenURL('tel:12345678901').then(supported => {
            if (!supported) {

                Toast.fail('Can\'t handle url: ' + 'tel:123456', 1);


            } else {

                return Linking.openURL(this.props.url);

            }

        }).catch(err => Toast.fail('出现错误', 1));
        this.onClose();

    };


    renderAction = () => {

        const props = this.props;
        if (!props.action) return null;

        let actions = null;

        if (Array.isArray(props.action)) {
            actions = [];
            props.action.map((action, index) => {
                if (action) {
                    actions.push(
                        <TouchableOpacity style={Styles.btn}
                                          key={index}
                                          onPress={action.onPress}>
                            <Text>{action.text}</Text>
                        </TouchableOpacity>
                    )
                }
            });
        } else {

            actions = (
                <TouchableOpacity style={Styles.btn}
                                  onPress={props.action.onPress}>
                    <Text>{props.action.text}</Text>
                </TouchableOpacity>
            )
        }
        return (
            <View>
                <View style={[Styles.detail, {padding: 20}]}>
                    <View style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'flex-end'
                    }}>
                        {actions}
                    </View>
                </View>
                <Line backgroundColor='#f2f2f2'/>
            </View>

        )
    };


    renderRider = () => {

        let rider = '暂无骑手信息';
        if (!this.props.section) return null;
        if (!this.props.section.get_big) return null;
        if (!this.props.section.get_big.get_rider) return null;
        rider = this.props.section.get_big.get_rider.username + ' ' + this.props.section.get_big.get_rider.phone;
        return (
            <View style={Styles.detail}>
                <Text>骑手信息:</Text>
                <Text>{rider}</Text>
            </View>
        )
    };

    render() {
        const {order_status = 0, count = 0, driver, issend, store_price, totalprice} = this.props.section;
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <View style={Styles.detail}>
                    <Text>{` 合计¥:${Number(totalprice).toFixed(2)}`}</Text>
                </View>
                <Line backgroundColor='#f2f2f2'/>
                {this.renderRider()}
                <Line backgroundColor='#f2f2f2'/>
                {this.renderAction()}
                {/*{(() => {*/}
                {/*if (0 === order_status) {*/}
                {/*return (*/}
                {/*<View style={[Styles.detail, {padding: 20}]}>*/}
                {/*<TouchableOpacity style={Styles.btn} onPress={props.action.onPress}>*/}
                {/*<Text>{props.action.text}</Text>*/}
                {/*</TouchableOpacity>*/}
                {/*</View>*/}
                {/*)*/}
                {/*}*/}
                {/*if (1 === order_status) {*/}
                {/*return (*/}
                {/*<View style={[Styles.driver, {padding: 20}]}>*/}
                {/*<Flex style={[Styles.flex1]} justify='between'>*/}
                {/*<View>*/}
                {/*<Flex>*/}
                {/*<Text>{`骑手信息:${driver.name}\t`}</Text>*/}
                {/*<TouchableOpacity onPress={this.showModal}>*/}
                {/*<Text>{driver.phone}</Text>*/}
                {/*</TouchableOpacity>*/}
                {/*</Flex>*/}
                {/*</View>*/}
                {/*<TouchableOpacity onPress={Actions.order_track}>*/}
                {/*<SimpleLineIcon name="location-pin" size={20} color={Color.primary}/>*/}
                {/*</TouchableOpacity>*/}
                {/*</Flex>*/}
                {/*</View>*/}
                {/*)*/}
                {/*}*/}
                {/*})()}*/}
                {/*<Line backgroundColor='#f2f2f2' height={10}/>*/}
                {/*<Modal*/}
                {/*title="确定拨打电话?"*/}
                {/*transparent*/}
                {/*maskClosable={true}*/}
                {/*visible={this.state.modal}*/}
                {/*onClose={this.onClose}*/}
                {/*footer={[{text: '确定', onPress: this.call}, {text: '取消', onPress: this.onClose}]}*/}
                {/*>*/}
                {/*<View style={[CommonStyle.centerVH, {marginTop: 10}]}>*/}
                {/*<Text>1234567890</Text>*/}
                {/*</View>*/}

                {/*</Modal>*/}
            </TouchableOpacity>
        )
    }
}


const Styles = StyleSheet.create({
    container: {
        display: 'flex'
    },
    detail: {
        display: 'flex',
        height: size(45),
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        paddingRight: 10,
        paddingLeft: 10,
    },
    driver: {
        width: '100%',
        flex: 1,
    },
    btn: {
        borderColor: '#999',
        borderWidth: 1,
        borderRadius: size(35 / 2),
        height: size(35),
        paddingHorizontal: 20,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5
    }
});

const action = {
    text: PropTypes.string,
    onPress: PropTypes.func,
};
footer.propTypes = {
    action: PropTypes.oneOfType([PropTypes.shape(action), PropTypes.array])
};
footer.defaultProps = {
    action: null
};

export default footer;