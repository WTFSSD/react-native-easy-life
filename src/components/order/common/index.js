import React from 'react'
import product from './product'
import header from './sectionHeader'
import footer from './sectionFooter'
import loading from '../../common/loading'
import evaluateBar from './evaluateBar'

import topBar from './topBar'
export const TopBar = topBar;
export const Product = product;
export const Header = header;
export const Footer = footer;
export const Loading = loading;
export const EvaluateBar = evaluateBar;
