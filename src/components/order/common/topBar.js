import React from 'react'
import PropTypes from 'prop-types'
import {View, Text, StyleSheet, TouchableOpacity, Animated} from 'react-native'
import CommonStyle, {size} from '../../../assets/styles/index'
import {Line} from '../../common/index'


const offset = 4;

class topBar extends React.Component {
    constructor() {
        super();
        this.state = {
            lineWidth: 0,
            lineLeft: new Animated.Value(0),

        }
    }

    componentDidMount() {


        const lineWidth = size(375 / this.props.titles.length * 0.5);
        const currentIndex = this.props.currentIndex;
        this.setState({currentIndex, lineWidth});


        Animated.timing(
            this.state.lineLeft,//初始值
            {
                toValue: size(375 / this.props.titles.length * currentIndex + lineWidth / 2),
                duration: 200,
            }
        ).start(() => {
            this.props.onChange(currentIndex)
        });//开始
    }


    selectedItem(currentIndex) {
        const {lineWidth} = this.state;
        if (this.state.currentIndex === currentIndex) return;
        this.setState({currentIndex: currentIndex});
        Animated.timing(
            this.state.lineLeft,//初始值
            {
                toValue: size(375 / this.props.titles.length * currentIndex + lineWidth / 2),
                duration: 200,
            }
        ).start(() => {
            this.props.onChange(currentIndex)
        });//开始

    }

    render() {
        return (
            <View style={Styles.container}>
                <View style={Styles.itemContent}>
                    {this.props.titles.map((text, index) => {
                        if (index !== this.props.titles.length - 1) {
                            return (
                                <TouchableOpacity key={index}
                                                  style={Styles.item}
                                                  onPress={this.selectedItem.bind(this, index)}
                                >
                                    <View style={[CommonStyle.centerVH, {flex: 1}]}>
                                        <Text>{text}</Text>
                                    </View>
                                    <View style={Styles.line}/>
                                </TouchableOpacity>
                            )
                        } else {
                            return (
                                <TouchableOpacity key={index}
                                                  style={Styles.item}
                                                  onPress={this.selectedItem.bind(this, index)}>
                                    <View style={[CommonStyle.centerVH, {flex: 1}]}>
                                        <Text>{text}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        }
                    })}
                </View>
                <Animated.View style={{
                    width: size(this.state.lineWidth),
                    height: 2,
                    backgroundColor: '#0D7EE8',
                    position: 'absolute',
                    bottom: 1,
                    zIndex: 10,
                    transform: [
                        {
                            translateX: this.state.lineLeft
                        }
                    ]
                }}/>
                <Line height={1} backgroundColor="#D8D8D8"/>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        height: 40,
        position: 'relative'
    },
    itemContent: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
    },
    item: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center'
    },
    itemSelected: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center'
    },
    line: {
        width: 1,
        height: '50%',
        backgroundColor: '#D8D8D8',

    },
});

topBar.propTypes = {
    onChange: PropTypes.func.isRequired,
    titles: PropTypes.array,
    currentIndex: PropTypes.number,
};
topBar.defaultProps = {
    currentIndex: 0,
    onChange: () => {
    }
};
//left:size(375/this.props.titles.length*this.props.currentIndex+70/2)

export default topBar;
