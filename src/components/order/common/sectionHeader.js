import React from 'react'
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native'
import CommonStyle, {size,Color} from '../../../assets/styles/index'

import {MyIcon} from '../../common/index'

const product = props => {
    const {order_number,get_store:{store_name}} = props.section;
    return (
       <TouchableOpacity onPress={props.onPress}>
           <View style={Styles.container}>
               <Text style={CommonStyle.text1}>订单号:<Text>{order_number}</Text></Text>
           </View>
           <View style={Styles.store}>
               <MyIcon name='shop' color={Color.primary} size={16}/>
               <Text style={Styles.store_name}>{store_name}</Text>
           </View>
       </TouchableOpacity>

    )
};

const Styles = StyleSheet.create({
    container: {
        height: size(36),
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'center',
        paddingLeft:10,
    },
    store:{
        display:'flex',
        flexDirection:'row',
        padding:5,
        backgroundColor:'#FAFAFA',
        alignItems:'center',
        borderBottomWidth:0.5,
        borderBottomColor:'#D0D0D0'
    },
    store_name:{
        marginLeft:5
    }
});
export default product;