import React from 'react'
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native'
import { Actions } from 'react-native-router-flux'
import PropTypes from 'prop-types'
import {Flex} from 'antd-mobile'
import CommonStyle, {size} from '../../../assets/styles/index'
import { Line } from '../../common/index'
import EvaluateBar from './evaluateBar'
import {checkImageUrl} from '../../../utils/request'
const product = props => {
    const {get_order_product,count} = props.item;
    return (
        <TouchableOpacity onPress={props.onPress}>
            <View style={Styles.container}>
                <Image source={{uri: checkImageUrl(get_order_product.preview)}} style={Styles.icon}/>
                <View  style={[Styles.flex1, {paddingLeft: 10,flexDirection:'column',justifyContent:'center',
                    alignItems:'flex-start'



                }]}>
                    <Text>{get_order_product.name}</Text>
                    <Text>{get_order_product.spec}</Text>
                    <Flex justify='between' style={{width: '100%'}}>
                        <Text style={CommonStyle.priceText}>{Number(get_order_product.price).toFixed(2)}</Text>
                        <Text>x{count}</Text>
                    </Flex>
                </View>
            </View>
            {/*{props.evaluate?<EvaluateBar onPress={props.onEvaluate}/>:null}*/}
            <Line height={1} backgroundColor='#f2f2f2'/>
        </TouchableOpacity>
    )
};

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        backgroundColor: '#fafafa',
        padding: 10,
        flexDirection: 'row',
    },
    flex1: {
        flex: 1,
    },
    icon: {
        width: size(75),
        height: size(75),
        borderRadius: size(75 / 2)
    },
});


const ItemProps = {
    icon:PropTypes.string,
    name:PropTypes.string,
    spec:PropTypes.string,
    price:PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    count:PropTypes.number,
};

product.propTypes = {
    evaluate: PropTypes.bool,
    onEvaluate:PropTypes.func,
    item:PropTypes.shape(ItemProps)
};
product.defaultProps = {
    evaluate: true,
    actions :null,
    onEvaluate:()=>{
        Actions.order_detail()
    }
};
export default product;