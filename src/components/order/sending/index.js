/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {StyleSheet, View, SectionList} from 'react-native'
import {connect} from 'react-redux'
import {Tabs} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'
import {Actions, ActionConst} from 'react-native-router-flux'
import data from '../data'
import CommonStyle, {Color, size} from '../../../assets/styles/index'
import {BackButton, Line} from '../../common/index'
import {TopBar, Product, Header, Footer, Loading} from '../common/index'

const TabPane = Tabs.TabPane;

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data,
            refreshing: false,
        };
    }


    componentDidMount() {
        this.fetchData();
    }

    onRefresh = () => {
        this.fetchData()
    };

    fetchData() {
        this.props.dispatch({
            type: 'order/fetchOrderList',
            payload: {
                issend: 3
            }
        })
    }
    onProductPress = (order_id)=>{
        Actions.order_detail({order_id});
    };
    render() {
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: '已发货', tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />


                <SectionList sections={this.props.order.list.map((item) => ({...item, data: item.get_order_info}))}
                             renderSectionHeader={(item)=>(<Header {...item} onPress={this.onProductPress.bind(this,item.section.id)}/>)}
                             renderSectionFooter={(item) => (<Footer {...item} />)}
                             keyExtractor={(item, index) => item.id}
                             refreshing={this.props.order.loading}
                             onRefresh={this.onRefresh}
                             style={Styles.list}
                             ItemSeparatorComponent={()=>(<Line height={10}/>)}
                             renderItem={(i) => <Product {...i} onPress={this.onProductPress.bind(this,i.item.oid)}/>}
                             keyboardDismissMode="on-drag"
                />
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
    list: {
        flex: 1,
    }
});

export default connect(
    ({auth,order}) => ({auth,order})
)(Page)
