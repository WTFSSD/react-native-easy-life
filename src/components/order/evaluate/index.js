/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {PropTypes, Component} from 'react'
import {StyleSheet, Text, View, TextInput, ScrollView} from 'react-native'
import {connect} from 'react-redux'
import {Button, Toast} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'

import CommonStyle, {Color, size} from '../../../assets/styles/index'
import {BackButton, Line, Star} from '../../common/index'

import {Product, Footer} from '../common/index'


const _StarState = ['非常不满意', '不满意', '一般', '满意', '非常满意'];

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {
            star: 5,
            starState: _StarState[4],
            evaluate: null
        };
    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {

    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }


    onChangeStar(index) {
        this.setState({star: index + 1, starState: _StarState[index]});
    }


    renderProducts = () => {
        let products = [];
        if (!this.props.item) return products;
        if (Array.isArray(this.props.item.section.data)) {
            this.props.item.section.data.map((item, index) => {
                if (item) {
                    products.push(<Product item={item} key={index}/>);
                }
            })
        }
        return products;
    };


    onEvaluate = () => {
        if (!this.state.evaluate) {
            Toast.fail('评论内容不能为空', 0.5);

        } else {
            this.props.dispatch({
                type: 'order/evaluate',
                payload: {...this.state,order_id:this.props.item.section.id}
            })
        }
    };

    render() {
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />

                <View style={{
                    display: 'flex',
                    flex: 1
                }}>
                    <ScrollView>
                        {this.renderProducts()}

                        <TextInput placeholder='说说您的心得，分享给想买的他们吧！'
                                   multiline
                                   style={Styles.input}
                                   onChangeText={(evaluate) => {
                                       this.setState({evaluate})
                                   }}
                        />
                        <View style={Styles.star}>
                            <View style={{width: size(70), marginLeft: 10}}>
                                <Text size={{fontSize: 14, color: '#333'}}>描述相符</Text>
                            </View>
                            <Star stars={this.state.star}
                                  total={5}
                                  editEnable
                                  starStyle={{marginLeft: 5, marginRight: 5}}
                                  onPress={this.onChangeStar.bind(this)}
                            />
                            <View style={{width: size(80)}}>
                                <Text size={{fontSize: 14, color: '#333'}}>{this.state.starState}</Text>
                            </View>
                        </View>
                        <View style={[CommonStyle.centerVH, {paddingTop: size(42)}]}>
                            <Button style={Styles.btn} activeStyle={[Styles.btn, {opacity: 0.8}]} onClick={this.onEvaluate}>
                                <Text style={{color: 'white', fontSize: 16}}>提交</Text>
                            </Button>
                        </View>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1
    },
    input: {
        borderWidth: 0.5,
        borderColor: '#d0d0d0',
        minHeight: 150,
        fontSize: 14,
        padding: 10,
    },
    btn: {
        width: size(180),
        height: size(33),
        backgroundColor: Color.primary,
        borderColor: Color.primary,
    },
    star: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingTop: size(20)
    }
});

export default connect(
    ({home, auth,order}) => ({home, auth,order})
)(Page)



