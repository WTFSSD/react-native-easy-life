/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {Component} from 'react'
import {StyleSheet, View, SectionList} from 'react-native'
import {connect} from 'react-redux'
import {Tabs} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'
import {Actions, ActionConst} from 'react-native-router-flux'
import data from '../data'
import CommonStyle, {Color, size} from '../../../assets/styles/index'
import {BackButton, Line} from '../../common/index'
import {TopBar, Product, Header, Footer, Loading} from '../common/index'

const TabPane = Tabs.TabPane;

class Page extends Component {

    isSends = [4, 5];
    currentTab = 0;

    constructor(props) {
        super(props);
        this.state = {
            data,
            refreshing: false,
        };
        this.timer = null;
    }


    componentDidMount() {
        this.fetchData();
    }


    onRefresh = (currentIndex) => {
        if (typeof  currentIndex !== 'undefined') {
            this.currentTab = currentIndex;
        }
        this.fetchData();
    };

    onPraise = (item) => {
        Actions.order_evaluate({item});
    };
    fetchData = () => {
        this.props.dispatch({
            type: 'order/fetchOrderList',
            payload: {
                issend: this.isSends[this.currentTab]
            }
        })
    };


    onProductPress = (order_id) => {
        Actions.order_detail({order_id});
    };


    render() {
        console.log(this.currentTab);
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: '已完成', tintColor: 'white'}}
                               leftButton={<BackButton/>}
                />
                <TopBar titles={['待评价', '已完成']} onChange={this.onRefresh}/>
                <SectionList sections={this.props.order.list.map((item) => ({...item, data: item.get_order_info}))}
                             renderSectionHeader={(item) => (
                                 <Header {...item} onPress={this.onProductPress.bind(this, item.section.id)}/>)}
                             renderSectionFooter={(item) => (<Footer {...item} action={this.currentTab === 0
                                 ?
                                 {
                                     text: '去评价',
                                     onPress: this.onPraise.bind(this, item)
                                 }

                                 : null}/>)}
                             keyExtractor={(item, index) => item.id}
                             refreshing={this.props.order.loading}
                             onRefresh={this.onRefresh}
                             style={Styles.list}
                             ItemSeparatorComponent={Line.bind(null, {height: 1})}
                             renderItem={(i) => <Product {...i} onPress={this.onProductPress.bind(this, i.item.oid)}/>}
                             keyboardDismissMode="on-drag"
                />
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    },
    list: {
        flex: 1,
    },
});

export default connect(
    ({auth, order}) => ({auth, order})
)(Page)
