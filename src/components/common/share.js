/**
 * Created by wtfssd on 2017/9/19.
 */
import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet , View , TouchableOpacity , Text } from 'react-native'

import CommonStyle , { SCREEN , size } from '../../assets/styles/index'

import MyIcon from './myIcon'

class share extends React.Component {
  constructor() {
    super();
    this.state = {}
  }

  render() {
    return (
      <View style={[ Styles.container , this.props.style ]}>
        <View style={Styles.item}>
          <TouchableOpacity style={[ CommonStyle.centerVH , Styles.icon ]}>
            <MyIcon name="qq" color="rgb(253,81,84)" size={size( 40 )}/>
          </TouchableOpacity>
          <Text style={Styles.text}>QQ</Text>
        </View>

        <View style={Styles.item}>
          <TouchableOpacity style={[ CommonStyle.centerVH , Styles.icon ]}>
            <MyIcon name="wechat-copy" color="rgb(96,213,70)" size={size( 40 )}/>
          </TouchableOpacity>
          <Text style={Styles.text}>微信</Text>
        </View>

        <View style={Styles.item}>
          <TouchableOpacity style={[ CommonStyle.centerVH , Styles.icon ]}>
            <MyIcon name="0054" color="rgb(80,112,189	)" size={size( 40 )}/>
          </TouchableOpacity>
          <Text style={Styles.text}>微信朋友圈</Text>
        </View>

        <View style={Styles.item}>
          <TouchableOpacity style={[ CommonStyle.centerVH , Styles.icon ]}>
            <MyIcon name="qqkongjian" color="rgb(227,197,76)" size={size( 40 )}/>
          </TouchableOpacity>
          <Text style={Styles.text}>QQ空间</Text>
        </View>
      </View>
    )
  }
}

const Styles = StyleSheet.create( {
  container : {
    display : 'flex' ,
    flexDirection : 'row' ,
    height : size( 151 )
  } ,
  item : {
    display : 'flex' ,
    alignItems : 'center' ,
    justifyContent : 'center' ,
    flex : 1 ,

  } ,
  icon : {
    backgroundColor : 'rgb(239,239,239)' ,
    padding : size( 15 ) ,
    borderRadius : 5
  } ,
  text : {
    fontSize : 14 ,
    color : '#333' ,
    marginTop : 4 ,
  }
} );

share.propTypes = {
  shareConfig:PropTypes.object,
};
share.defaultProps = {};

export default share;