import React from 'react'
import PropTypes from 'prop-types'
import {TouchableOpacity, View} from 'react-native'
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons'
import {Popup} from 'antd-mobile'
import {Actions} from 'react-native-router-flux'
import CommonStyle, {} from '../../assets/styles/index'
import checkBox from './checkBox'


import myIcon from './myIcon'
import payMethod from './payMethod'
import counter from './counter'
import productItem from './productItem'
import star from './star'
import line from './line'
import coupon from './coupon'
import share from './share'
import loading from './loading'


export const MyIcon = myIcon;
export const PayMethod = payMethod;
export const Counter = counter;
export const ProductItem = productItem;
export const Star = star;
export const Line = line;
export const Coupon = coupon;
export const Share = share;
export const LoadingView = loading;
export const CheckBox = checkBox;
backButton = props => (
    <TouchableOpacity onPress={props.onPress}>
        <View style={[
            CommonStyle.centerVH, {
                height: 44,
                paddingHorizontal: 10
            }
        ]}>
            <SimpleLineIcon name="arrow-left" color="white"/>
        </View>
    </TouchableOpacity>
);

backButton.propTypes = {
    onPress: PropTypes.func,
};

backButton.defaultProps = {
    onPress: () => {
        Actions.pop()
    }
};
export const BackButton = backButton;


const shareButton = props => (<TouchableOpacity onPress={() => {
    Popup.hide();
    Popup.show(
        <Share shareConfig={props.shareConfig}/>, {
            animationType: 'slide-up', maskClosable: true, onMaskClose: () => {
                Popup.hide();
            }
        }
    )
}}>
    <View style={[CommonStyle.centerVH, {height: 44, paddingHorizontal: 10}]}>
        <MyIcon name="fenxiang1" color="white" size={30}/>
    </View>
</TouchableOpacity> );

shareButton.propTypes = {
    shareConfig: PropTypes.object,
};
export const ShareButton = shareButton;


