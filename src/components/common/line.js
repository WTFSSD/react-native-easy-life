/**
 * Created by wtfssd on 2017/9/14.
 */

import React from 'react'
import PropTypes from 'prop-types';

import {
  View
} from 'react-native'

const Line        = ( props ) => <View
  style={{ height : props.height , width : '100%' , backgroundColor : props.backgroundColor }}/>;
Line.propTypes    = {
  height : PropTypes.number ,
  backgroundColor : PropTypes.string
};
Line.defaultProps = {
  height : 1 ,
  backgroundColor : 'transparent'
};
export default Line;