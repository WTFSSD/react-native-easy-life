/**
 * Created by wtfssd on 2017/9/16.
 */

import createIconSet from 'react-native-vector-icons/lib/create-icon-set';
import glyphMap from './iconfont.json';

const iconSet = createIconSet( glyphMap , 'iconfont' , 'iconfont.ttf' );

export default iconSet;