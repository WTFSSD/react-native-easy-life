
import React from 'react'
import PropTypes from 'prop-types'
import {ImageBackground ,StyleSheet,View,Text} from 'react-native'
import {Flex} from 'antd-mobile'
import CommonStyle , { size } from '../../assets/styles/index'

 coupon =  props  => (
  <ImageBackground source={require( '../../assets/image/couponBg.png' )} style={Styles.couponBg}>
    <Flex style={{ flex : 1 }}>
      <View style={{ backgroundColor : 'transparent' , flex : 0.199 }}>
        <Text style={{ color : 'white' , fontSize : 14 }}>{props.name}</Text>
      </View>
      <View style={[ CommonStyle.centerVH , { backgroundColor : 'transparent' , flex : 1 } ]}>
        <Text style={{ color : 'white' , fontSize : 14 }}>满<Text
          style={{ color : 'white' , fontSize : 20 }}>{props.threshold}</Text>减<Text
          style={{ color : 'white' , fontSize : 20 }}>{props.discount}</Text></Text>
      </View>
    </Flex>
  </ImageBackground>
);

const Styles = StyleSheet.create( {
  container : {
    display : 'flex' ,
    flex : 1 ,
  } ,

  item : {
    padding : 10 ,
  } ,
  btn : {
    display : 'flex' ,
    flexDirection : 'row' ,
    marginRight : 5 ,
    alignItems : 'center'
  } ,
  //w:h=2.03
  couponBg : {
    width : size(82) * 2.03 ,
    height : size(82),
    marginLeft : 10 ,
    paddingHorizontal : 10 ,
  } ,

} );

coupon.propTypes = {
  name :PropTypes.string,
  threshold :PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  discount:PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
};
export default coupon
