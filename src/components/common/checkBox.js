/**
 *  复选框
 */
import React, {Component} from 'react';

import {
    StyleSheet,
    View,
    ViewPropTypes,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';

import Icon from './myIcon'

export default class CheckBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: props.checked,
        }
    }

    componentWillReceiveProps({checked}) {
       this.setState({checked});
    }
    static propTypes = {
        ...(ViewPropTypes || View.PropTypes),
        checked: React.PropTypes.bool,
        onChange: React.PropTypes.func,
        color: React.PropTypes.string,
        checkedColor: React.PropTypes.string,
        size: React.PropTypes.number,
    };
    static defaultProps = {
        checked: false,
        onChange: null,
        size: 20,
        color: 'gray',
        checkedColor: 'red',
    };
    onChange(){
        const checked = !this.state.checked;
        this.setState({
            checked
        });
        if (this.props.onChange&&typeof this.props.onChange === 'function'){
            this.props.onChange(checked);
        }

    }
    render() {
        const {size, color, checkedColor} = this.props;
        const {checked} = this.state;
        return (
            <TouchableOpacity {...this.props} onPress={this.onChange.bind(this)}>
                <Icon size={size} color={checked ? checkedColor : color} name={!checked?'fuxuankuang':'checkbox-selected-copy'}/>
            </TouchableOpacity>
        );
    }
}

