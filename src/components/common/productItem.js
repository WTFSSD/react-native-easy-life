/**
 * Created by wtfssd on 2017/9/19.
 */

import React from 'react'
import PropTypes from 'prop-types'
import {
    StyleSheet, Text, View, AsyncStorage, ScrollView, Image, TouchableOpacity, FlatList
} from 'react-native'
import {Flex} from 'antd-mobile'

import CommonStyle, {SCREEN, size} from '../../assets/styles/index'

const product = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress} activeOpacity={0.9}>
            <View style={Styles.container}>
                <Image source={{uri: props.icon}} style={Styles.icon}/>
                <Text style={[CommonStyle.text1, CommonStyle.pdv5]}>{props.name}</Text>
                <Text style={[CommonStyle.text1, CommonStyle.pdv5]} numberOfLines={1}>{props.spec}</Text>
                <Flex justify="between">
                    <Text style={CommonStyle.priceText}>¥{Number(props.price).toFixed(2)}</Text>
                    <Text style={CommonStyle.text2}>已售 {props.sold}</Text>
                </Flex>
            </View>
        </TouchableOpacity>
    )
};

product.propTypes = {
    onPress: PropTypes.func,
    icon: PropTypes.string,
    spec: PropTypes.string,
    price: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    name: PropTypes.string,
    sold: PropTypes.number,
};
product.defaultProps = {
    onPress: () => {
    },
    icon: '',
    spec: '0g',
    price: 0.00,
    sold: 0,
};

const Styles = StyleSheet.create({
    container: {
        width: (SCREEN.width - 2) / 2,
        marginRight: 1,
        backgroundColor: 'white',
        padding: 10
    },
    icon: {
        width: size(130),
        height: size(130),
        borderRadius: size(65),
        marginLeft: size(18.25)
    }
});

export default product;