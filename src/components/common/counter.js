/**
 * Created by wtfssd on 2017/9/16.
 */
import React from 'react'
import {StyleSheet, Text, View, AsyncStorage, SectionList, Image, TouchableOpacity} from 'react-native'
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons'
import PropTypes from 'prop-types'
import CommonStyle, {SCREEN} from '../../assets/styles/index'

const counter = (props) => {
    return (
        <TouchableOpacity activeOpacity={1} {...props}>
            <View style={[Styles.container, {width: props.width}, props.style]}>
                <TouchableOpacity onPress={props.onMinus.bind(null, props.count)} disabled={props.disabled}>
                    <SimpleLineIcon name="minus" size={20} color="#9a9a9a"/>
                </TouchableOpacity>
                <Text style={CommonStyle.text1}>x{props.count}</Text>
                <TouchableOpacity onPress={props.onPlus.bind(null, props.count)} disabled={props.disabled}>
                    <SimpleLineIcon name="plus" size={20} color="#F2302F"/>
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
    )
};

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
});
counter.propTypes = {
    onPlus: PropTypes.func,
    onMinus: PropTypes.func,
    width: PropTypes.number,
    disabled: PropTypes.bool,
    count: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ])
};

counter.defaultProps = {
    onPlus: () => {
    },
    onMinus: () => {
    },
    count: 1,
    width: 97 / 375 * SCREEN.width,
    disabled: false,
};

export default counter;