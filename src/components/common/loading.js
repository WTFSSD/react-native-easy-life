import React from 'react'
import { View,ActivityIndicator,Text } from 'react-native'
import PropTypes from 'prop-types'
import CommonStyle from '../../assets/styles/index'

const loadingview = props=>{
    if (props.loading){
        return (
            <View style={[CommonStyle.centerVH,{flex:1,backgroundColor:'#fff'}]}>
                <ActivityIndicator
                    size='large'
                    color="black"
                />
                <Text style={{marginTop:20}}>加载中...</Text>
            </View>
        )
    }else {
        return (
            <View style={[{flex:1},props.style]}>
                {props.children}
            </View>
        )
    }

};

loadingview.propTypes = {
    loading:PropTypes.bool,
};
loadingview.defaultProps = {
    loading:false
};

export default loadingview;