/**
 * Created by wtfssd on 2017/9/17.
 */
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native'
import {Popup, List} from 'antd-mobile'

const ListItem = List.Item;
import CommonStyle, {SCREEN, Color} from '../../assets/styles/index'
import MyIcon from './myIcon'
import Line from './line'

const TopBar = (props) => {
    return (
        <View style={Styles.topBar}>
            <TouchableOpacity onPress={() => {
                Popup.hide();
            }}>
                <MyIcon name="cuowu" color="#666" size={20}/>
            </TouchableOpacity>
            <View style={[CommonStyle.centerVH, {flex: 1}]}>
                <Text style={[CommonStyle.text1, {fontSize: 16}]}>{props.title}</Text>
            </View>
            {props.extra &&
            <TouchableOpacity onPress={props.onExtraPress}>
                {props.extra}
            </TouchableOpacity>}
        </View>
    )
};
TopBar.propTypes = {
    title: PropTypes.string.isRequired,
    extra: PropTypes.element,
    onExtraPress: PropTypes.func,
};
TopBar.defaultProps = {
    title: '',
    extra: null,
    onExtraPress: null,
};

const payMethod = (props) => {
    return (
        <View style={Styles.container}>
            <TopBar title='选择支付方式'/>
            <Line backgroundColor="#d0d0d0" height={0.5}/>
            <List>
                <ListItem thumb={<MyIcon name="zhifubao" color="#1296db" size={23}/>}
                          onClick={()=>{
                              Popup.hide();
                              props.onPay('alipay');
                          }}
                          arrow="horizontal">
                    <Text style={[CommonStyle.text1, {marginLeft: 5}]}>支付宝</Text>
                </ListItem>
                <ListItem thumb={<MyIcon name="weixin" color="#1afa29" size={23}/>}
                          onClick={()=>{
                              Popup.hide();
                              props.onPay('wx');
                          }}
                          arrow="horizontal">
                    <Text style={[CommonStyle.text1, {marginLeft: 5}]}>微信</Text>
                </ListItem>
            </List>
        </View>
    )
};

payMethod.propTypes = {
    price: PropTypes.number.isRequired,
    onPay: PropTypes.func,
};
payMethod.defaultProps = {
    price: 0.00,
    onPay: () => { },
};
const Styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        minHeight: 150 / 375 * SCREEN.height
    },
    topBar: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        padding: 10,
    }
});

export default payMethod;
