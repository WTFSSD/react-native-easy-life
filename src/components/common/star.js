/**
 * Created by wtfssd on 2017/9/19.
 */
import React from 'react'
import PropTypes from 'prop-types'
import {StyleSheet, View, TouchableOpacity} from 'react-native'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import CommonStyle, {SCREEN, size} from '../../assets/styles/index'

class star extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    //heart-0
    //heart

    renderHeart() {
        let hears = [];
        const props = this.props;
        for (let i = 0; i < this.props.total; i++) {
            let startName = 'star-o';
            let starColor = '#999';
            if (i < this.props.stars) {
                startName = 'star';
                starColor = 'orange';
            }
            hears.push(
                <TouchableOpacity key={i}
                                  disabled={!this.props.editEnable}
                                  style={props.starStyle}
                                  onPress={props.onPress.bind(null,i)}
                >
                    <FontAwesomeIcon name={startName} size={props.starSize} color={starColor}/>
                </TouchableOpacity>)
        }

        return hears;
    }

    render() {
        return (
            <View style={[Styles.container, this.props.style]}>
                {this.renderHeart()}
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row'
    }
});

star.propTypes = {
    stars: PropTypes.number.isRequired,
    total: PropTypes.number,
    editEnable: PropTypes.bool,
    starStyle: PropTypes.object,
    starSize: PropTypes.number,
    onPress:PropTypes.func,
};
star.defaultProps = {
    stars: 0,
    total: 5,
    editEnable: false,
    starStyle:null,
    starSize:20,
    onPress:()=>{}
};

export default star;