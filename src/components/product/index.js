/**
 * Created by wtfssd on 2017/9/15.
 */

import React , { PropTypes , Component } from 'react'
import {
  StyleSheet , Text , View , AsyncStorage , ScrollView , Image , TouchableOpacity , FlatList
} from 'react-native'
import { connect } from 'react-redux'
import { Flex } from 'antd-mobile'
import CommonStyle , { Color , size } from '../../assets/styles/index'
import NavigationBar from 'react-native-navbar'
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import { Actions , ActionConst } from 'react-native-router-flux'
import { LoadingView,ProductItem } from '../common/index'
const SortItem = ( { text = '' , selected = 0,onPress=()=>{} } ) => (
  <View style={Styles.sort}>
    <TouchableOpacity style={CommonStyle.flexRow} onPress={onPress}>
      <Text style={{ marginRight : 10 , color : selected !== 0 ? '#FC4A00' : '#666666' }}>{text}</Text>
      <FontAwesomeIcon name={selected === 1 ? "caret-down" : "caret-up"} color={selected !== 0 ? '#FC4A00' : '#666666'}
                       size={10}/>
    </TouchableOpacity>
  </View>
);
import Line from '../common/line'

class Page extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      refresh : false
    };
  }

  /*组件将要加载*/
  componentWillMount() {

  }

  /*组件已经加载*/
  componentDidMount() {
      // console.log(this.props.category_id);
    this.props.dispatch({
        type:'product/fetchList',
        payload:{category_id:this.props.category_id}
    })
  }

  /*组件将要卸载*/
  componentWillUnmount() {

  }

  onSearch() {

  }

  renderItem( { item } ) {
    return <ProductItem  {...item} onPress={Actions.productDetail.bind(null,{store_id:item.store_id,id:item.id})}/>
  }

  renderSortView = () => {
    return (
      <Flex align="center" style={{ backgroundColor : 'white' }}>
        <SortItem text="价格" selected={0}/>
        <View>
          <View style={Styles.line}/>
        </View>
        <SortItem text="销量" selected={0}/>
      </Flex>
    )
  };

  onRefresh = () => {
      this.props.dispatch({
          type:'product/fetchList',
          payload:{category_id:this.props.category_id}
      })
  };

  render() {
    const LeftButton = (
      <TouchableOpacity onPress={Actions.pop}>
        <View style={[ CommonStyle.centerVH , { height : 44 , paddingHorizontal : 10 } ]}>
          <SimpleLineIcon name="arrow-left" color="white"/>
        </View>
      </TouchableOpacity>
    );

    return (
      <View style={CommonStyle.container}>
        <NavigationBar tintColor={Color.primary}
                       statusBar={{ style : 'light-content' }}
                       leftButton={LeftButton}
                       title={{ title : this.props.title , tintColor : 'white' }}
        />
        {this.renderSortView()}
        <Line/>

        <LoadingView loading={this.props.product.loading}>
          <FlatList data={this.props.product.list}
                    extraData={this.state}
                    style={{ flex : 1 }}
                    numColumns={2}
                    onRefresh={this.onRefresh}
                    refreshing={this.state.refresh}
                    ItemSeparatorComponent={Line.bind( null , { height : 1 , backgroundColor : '#e9e9e9' } )}
                    renderItem={this.renderItem.bind( this )}
                    keyExtractor={( item , index ) => item.id}
          />
        </LoadingView>

      </View>
    )
  }
}

const Styles = StyleSheet.create( {
  container : {
    display : 'flex' ,
    flex : 1 ,
    justifyContent : 'center' ,
    alignItems : 'center' ,
  } ,
  sort : {
    flex : 1 ,
    backgroundColor : 'white' ,
    height : size( 40 ) ,
    flexDirection : 'row' ,
    justifyContent : 'center' ,
    alignItems : 'center'
  } ,
  line : {
    width : 1 ,
    height : size( 20 ) ,
    backgroundColor : '#D8D8D8'
  }

} );

export default connect(
  ( { auth,product } ) => ({ auth ,product})
)( Page )



