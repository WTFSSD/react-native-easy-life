/**
 * Created by wtfssd on 2017/9/15.
 */

import React from 'react'
import {StyleSheet, Image, Text, View, FlatList, TouchableOpacity} from 'react-native'
import {connect} from 'react-redux'
import {} from 'antd-mobile'
import {Actions} from 'react-native-router-flux'
import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color, SCREEN} from '../../assets/styles/index'
import {Line,LoadingView} from '../common/index'

const data1 = [
    {id: '1', text: '田园蔬菜'},
    {id: '2', text: '禽蛋肉类'},
    {id: '3', text: '水产海鲜'},
    {id: '4', text: '新鲜蔬果'},
    {id: '5', text: '豆制品类'},
    {id: '6', text: '净菜熟食'},
    {id: '7', text: '粮油副食'},
    {id: '8', text: '调味百货'},
];

class Page extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            leftSelectedId: null,
        };
    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {
        this.setState({leftSelectedId: '' + data1[0].id});
        this.fetchData(data1[0].id)
    }

    fetchData(category_id) {
        this.props.dispatch({
            type: 'product/fetchList',
            payload: {category_id}
        })
    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }

    _onPressItem = ({id}) => {
        this.setState({leftSelectedId: '' + id});
        this.fetchData('' + id);
    };

    renderItem1({item, index}) {

        const MText = props => {
            return <Text {...props}
                         style={{color: props.selected ? Color.primary : '#333', fontSize: 12}}>{props.children}</Text>;
        };

        return (
            <TouchableOpacity onPress={this._onPressItem.bind(this, item, index)}>
                <View key={index}
                      style={[
                          CommonStyle.centerVH,
                          Styles.leftItem,
                          (this.state.leftSelectedId === item.id) ? Styles.leftItemSelected : {}
                      ]}
                >
                    <MText selected={(this.state.leftSelectedId === item.id)}>{item.text}</MText>
                </View>
            </TouchableOpacity>

        )
    }

    renderItem2 = ({item}) => (
        <TouchableOpacity onPress={Actions.productDetail.bind(null,{store_id:item.store_id,id:item.id})}>
            <View style={[CommonStyle.centerVH, Styles.rightItem]}>
                <Image source={{uri: item.icon}}
                       style={{width: (SCREEN.width * 0.8 - 5) / 3 - 20, height: (SCREEN.width * 0.8 - 5) / 3 - 20}}/>
                <Text style={{color: '#333', fontSize: 12, marginTop: 2}}>{item.name}</Text>
            </View>
        </TouchableOpacity>

    );

    keyExtractor = (item, index) => item.id;

    render() {
        const {product} = this.props;
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                />
                <View style={Styles.listContainer}>
                    <View style={Styles.list1}>
                        <FlatList data={data1}
                                  extraData={this.state}
                                  style={{flex: 1}}
                                  showsHorizontalScrollIndicator={false}
                                  showsVerticalScrollIndicator={false}
                                  ItemSeparatorComponent={Line.bind(null, {height: 1, backgroundColor: '#e9e9e9'})}
                                  renderItem={this.renderItem1.bind(this)}
                                  keyExtractor={this.keyExtractor}

                        />
                    </View>
                    <LoadingView loading={product.loading}>
                        <View style={Styles.list2}>
                            <FlatList data={product.list}
                                      extraData={this.state}
                                      numColumns={3}
                                      renderItem={this.renderItem2.bind(this)}
                                      keyExtractor={this.keyExtractor}
                                      getItemLayout={(data, index) => ( {length: 100, offset: 100 * index, index} )}
                            />
                        </View>
                    </LoadingView>

                </View>

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#e9e9e9'
    },
    listContainer: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
    },
    list1: {
        flex: 0.28,
        backgroundColor: 'white'
    },
    list2: {
        flex: 1,
        marginLeft: 5,
        backgroundColor: 'white'
    },
    leftItem: {
        height: 68 / 375.0 * SCREEN.width,
    },
    rightItem: {
        height: 100 / 375.0 * SCREEN.width,
        width: (SCREEN.width * 0.8 - 5) / 3,
    },
    leftItemSelected: {
        backgroundColor: '#F3F4F5'
    }
});

export default connect(
    ({product}) => ({product})
)(Page)




