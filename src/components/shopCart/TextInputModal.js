/**
 * Created by wtfssd on 2017/9/15.
 */

import React , { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet , Text , View , AsyncStorage , TouchableOpacity , TextInput } from 'react-native'
import { connect } from 'react-redux'
import {} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import MyIcon from '../common/myIcon'
import { Actions , ActionConst } from 'react-native-router-flux'
import CommonStyle , { Color , SCREEN } from '../../assets/styles/index'

const PagePropTypes = {
  onSubmit : PropTypes.func.isRequired
};

class Page extends Component<PagePropTypes , any> {
  constructor( props ) {
    super( props );
    this.state = {
      value : ''
    };
  }

  /*组件将要加载*/
  componentWillMount() {

  }

  /*组件已经加载*/
  componentDidMount() {

  }

  /*组件将要卸载*/
  componentWillUnmount() {

  }

  render() {
    const LeftButton = (
      <TouchableOpacity onPress={Actions.pop}>
        <View style={[ CommonStyle.centerVH , { height : 44 , paddingHorizontal : 10 } ]}>
          <MyIcon name="cuowu" color="white"/>
        </View>
      </TouchableOpacity>
    );
    return (
      <View style={Styles.container}>
        <NavigationBar tintColor={Color.primary}
                       statusBar={{ style : 'light-content' }}
                       title={{ title : this.props.title , tintColor : 'white' }}
                       leftButton={LeftButton}
        />

        <View style={Styles.content}>
          <TextInput placeholder='选填：对本次交易的说明（建议填写已和卖…'
                     multiline
                     style={Styles.input}
                     onChangeText={( value ) => {this.setState( { value } )}}
          />

          <View style={Styles.btnCentent}>
            <TouchableOpacity onPress={
              () => {
                Actions.pop();
                this.props.onSubmit( this.state.value )
              }
            }>
              <View style={Styles.btn}>
                <Text>确定</Text>
              </View>
            </TouchableOpacity>
          </View>

        </View>


      </View>
    )
  }
}

const Styles = StyleSheet.create( {
  container : {
    display : 'flex' ,
    flex : 1 ,
  } ,
  content : {
    display : 'flex' ,
    flex : 1 ,
    width : SCREEN.width ,
    padding : 10 ,
  } ,
  input : {
    borderWidth : 0.5 ,
    borderColor : '#d0d0d0' ,
    minHeight : 150 ,
    fontSize : 14 ,
  } ,
  btn : {
    backgroundColor : Color.primary ,
    paddingVertical : 10 ,
    paddingHorizontal : 50 ,
    borderRadius : 5 ,
  } ,
  btnCentent : {
    display : 'flex' ,
    justifyContent : 'center' ,
    alignItems : 'center' ,
    marginTop : 10
  }
} );

export default connect(
  ( { home } ) => ({ home })
)( Page )



