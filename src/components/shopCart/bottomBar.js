/**
 * Created by wtfssd on 2017/9/17.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {StyleSheet, Text, View, AsyncStorage, SectionList, Image, TouchableOpacity} from 'react-native'
import CommonStyle, {SCREEN, Color} from '../../assets/styles/index'

const canSubmit = (props) => {

if(props.count<=0){
    return true;
}
return false;
    if (props.type === 'shopCart') {
        return Number(props.total) < 20
    }
    return false;
};


const getStyle = (props) => {
    if (props.type === 'shopCart') {
        if (Number(props.total) < 20) {
            return Styles.disable;
        }
    }
    return Styles.barSummit;
};


const getTips = (props)=>{
    if (props.type === 'shopCart') {
        if (Number(props.total) < 20) {
           return <Text style={[CommonStyle.text1, {color: 'white'}]}>{`差￥${Number(20-Number(props.total)).toFixed(2)}起送`}</Text>
        }
    }
    return null;
};



const bottomBar = (props) => {
    const v1 = () => (
        <View style={[CommonStyle.centerVH, {flex: 1, flexDirection: 'row',}]}>
            <Text style={[CommonStyle.text1, {fontSize: 16}]}>合计:</Text>
            <Text style={[CommonStyle.priceText, {fontSize: 15}]}>¥{props.total.toFixed(2)}</Text>
        </View>
    );
    const v2 = () => (
        <View style={[CommonStyle.centerVH, {flex: 1, flexDirection: 'row',}]}>
            <Text style={[CommonStyle.text1]}>共:{props.count}件商品</Text>
            <Text style={[CommonStyle.text1, {fontSize: 16, marginLeft: 2}]}>合计:</Text>
            <Text style={[CommonStyle.priceText, {fontSize: 15}]}>¥{props.total.toFixed(2)}</Text>
        </View>
    );
    return (
        <View style={Styles.bar}>
            {
                props.type === 'shopCart' ? v1() : v2()
            }
            <TouchableOpacity activeOpacity={1} onPress={props.onSubmit} disabled={canSubmit(props)}>
                <View style={[CommonStyle.centerVH, getStyle(props)]}>
                    <Text style={[CommonStyle.text1, {color: 'white'}]}>{
                        (props.type === 'shopCart' ? ('结算(' + props.count + ')') : '提交订单')
                    }</Text>
                    {getTips(props)}
                </View>
            </TouchableOpacity>
        </View>
    )
};
bottomBar.propTypes = {
    count: PropTypes.number,
    total: PropTypes.number,
    onSubmit: PropTypes.func,
    type: PropTypes.oneOf([
        'shopCart',
        'submit'
    ]),
};
bottomBar.defaultProps = {
    count: 0,
    type: 'shopCart',
    onSubmit: () => {
    },
};

const Styles = StyleSheet.create({
    bar: {
        height: 44,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        // borderTopWidth: 0.5,
        borderTopColor: '#d0d0d0',
        backgroundColor:'#fff'
    },
    barSummit: {
        backgroundColor: Color.primary,
        height: '100%',
        width: 104 / 375 * SCREEN.width,
        flexDirection: 'column'
    },
    disable: {
        backgroundColor: '#ededed',
        height: '100%',
        width: 104 / 375 * SCREEN.width,
        flexDirection: 'column'
    }
});
export default bottomBar