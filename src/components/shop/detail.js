/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {PropTypes, Component} from 'react'
import {StyleSheet, Text, View, AsyncStorage, FlatList, TouchableOpacity, ScrollView} from 'react-native'
import {connect} from 'react-redux'
import {Tabs} from 'antd-mobile'
import {Actions, ActionConst} from 'react-native-router-flux'
import NavigationBar from 'react-native-navbar'
import CommonStyle, {Color} from '../../assets/styles/index'
import ShopItem from './shopItem'
import {ShareButton, BackButton, Line} from '../common/index'
import Menu from './pageMenus'
import Detail from './pageDetail'

const TabPane = Tabs.TabPane;

class Page extends Component {


    constructor(props) {
        super(props);
        this.state = {
            currentTab: '0'
        }
    }

    /*组件将要加载*/
    componentWillMount() {

    }


    shouldComponentUpdate(p1, s1) {
        return s1.currentTab === this.state.currentTab;
    }

    onTabChange = currentTab => {
        this.setState({currentTab})
    };


    renderItem = ({item}) => (
        <ShopItem item={item}/>
    );

    keyExtractor = (item, index) => item.id;

    render() {
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                               leftButton={<BackButton/>}
                               rightButton={<ShareButton/>}
                />
                <ScrollView style={Styles.container} overScrollMode='never' scrollEnabled={false}>
                    <Tabs onChange={this.onTabChange}
                          defaultActiveKey="0"
                          activeKey={this.state.currentTab}
                          style={Styles.container}>
                        <TabPane key={'0'} tab='店铺菜单' style={Styles.container}>
                            <Menu store_id={this.props.store_id}/>
                        </TabPane>
                        <TabPane key={'1'} tab='店铺详情' style={Styles.container}>
                            <Detail store_id={this.props.store_id}/>
                        </TabPane>
                    </Tabs>
                </ScrollView>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#fff',
    },

});

export default connect(
    ({store}) => ({store})
)(Page)



