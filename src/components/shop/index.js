/**
 * Created by wtfssd on 2017/9/15.
 */

import React , { PropTypes , Component } from 'react'
import { StyleSheet , Text , View , AsyncStorage , FlatList , TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import {} from 'antd-mobile'
import { Actions , ActionConst } from 'react-native-router-flux'
import NavigationBar from 'react-native-navbar'
import CommonStyle , { Color } from '../../assets/styles/index'
import {Line,LoadingView} from '../common/index'
import ShopItem from './shopItem'

class Page extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      refreshing : false
    };
  }

  /*组件将要加载*/
  componentWillMount() {

  }

  /*组件已经加载*/
  componentDidMount() {
      this.props.dispatch({
          type:'store/fetchList'
      })
  }

  /*组件将要卸载*/
  componentWillUnmount() {

  }

  renderItem = ( { item } ) => (
    <ShopItem item={item} onPress={Actions.shopDetail.bind(null,{store_id:item.id})}/>
  );

  keyExtractor = ( item , index ) => item.id;

  render() {

    return (
      <View style={Styles.container}>
        <NavigationBar tintColor={Color.primary}
                       statusBar={{ style : 'light-content' }}
                       title={{ title : this.props.title , tintColor : 'white' }}
        />

        <LoadingView loading={this.props.store.loading}>
          <FlatList data={this.props.store.list}
                    style={{ flex : 1 , display : 'flex' }}
                    extraData={this.state}
                    numColumns={2}
                    onRefresh={() => {
                        this.setState( { refreshing : true } );
                        setTimeout( () => {this.setState( { refreshing : false } );} , 2000 )
                    }}
                    refreshTitle="dfd"
                    refreshing={this.state.refreshing}
                    ItemSeparatorComponent={Line.bind( null , { height : 1 , backgroundColor : 'transparent' } )}
                    renderItem={this.renderItem.bind( this )}
                    keyExtractor={this.keyExtractor}

          />
        </LoadingView>

      </View>
    )
  }
}

const Styles = StyleSheet.create( {
  container : {
    display : 'flex' ,
    flex : 1 ,
    backgroundColor : '#e9e9e9'
  }
} );

export default connect(
  ( { store } ) => ({ store })
)( Page )



