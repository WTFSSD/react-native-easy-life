import React from 'react'
import {StyleSheet, Image, Text, View, AsyncStorage, FlatList, TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {Flex} from 'antd-mobile'
import CommonStyle, {Color, SCREEN, size} from '../../assets/styles/index'
import {Line,LoadingView} from '../common/index'

import {connect} from 'react-redux'

import {checkImageUrl} from '../../utils/request'

const data1 = [
    {
        id: '1',
        text: '田园蔬菜'
    },
    {
        id: '2',
        text: '食蛋肉类'
    },
    {
        id: '3',
        text: '水产海鲜'
    },
    {
        id: '4',
        text: '新鲜蔬果'
    },
    {
        id: '5',
        text: '豆制品类'
    },
    {
        id: '6',
        text: '净菜熟食'
    },
    {
        id: '7',
        text: '粮油副食'
    },
    {
        id: '8',
        text: '调味百货'
    },
];

class Page extends React.Component {
    constructor() {
        super();
        this.state = {
            leftSelectedId: null,
        };
    }

    componentWillMount(){

        this.props.dispatch({
            type:'store/fetchCategories',
            payload:{store_id:this.props.store_id}
        })
        // this.fetchData(data1[0].id);
    }



    _onPressItem = (selectedCategory,index) => {
        this.props.dispatch({
            type:'store/selectCategory',
            payload:{selectedCategory,store_id:this.props.store_id}
        })
    };

    renderItem1({item, index}) {

        const MText = props => {
            return <Text {...props} style={{
                color: props.selected ? Color.primary : '#333',
                fontSize: 12
            }}>{props.children}</Text>;
        };




        return (
            <TouchableOpacity onPress={this._onPressItem.bind(this, item, index)}>
                <View key={index}
                      style={[
                          CommonStyle.centerVH,
                          Styles.leftItem,
                          (this.props.store.selectedCategory.id === item.id) ? Styles.leftItemSelected : {}
                      ]}
                >
                    <MText selected={(this.state.leftSelectedId === item.id)}>{item.store_menu}</MText>
                </View>
            </TouchableOpacity>

        )
    }

    renderItem2 = ({item:{get_douct_info,store_id}}) => {
        if(!get_douct_info) return null;
        const {preview,name,spec,price,id} = get_douct_info;
        return (
            <TouchableOpacity onPress={Actions.productDetail.bind(null,{store_id,id})}>
                <View style={Styles.rightItem}>
                    <Image source={{uri: checkImageUrl(preview)}} style={Styles.rightItemImage}/>
                    <View style={Styles.rightItemContent}>
                        <Text>{name}</Text>
                        <Text>{spec}</Text>
                        <Flex justify='between'>
                            <Text style={CommonStyle.priceText}>¥{Number(price).toFixed(2)}</Text>
                            <Text style={CommonStyle.text4}>已售 {0}</Text>
                        </Flex>
                    </View>
                </View>
            </TouchableOpacity>
        )
    };

    renderNoData = ()=>{
      return (
          <View style={{display:'flex',justifyContent:'center',alignItems:'center',padding:10}}>
              <Text>暂无数据</Text>
          </View>
      )
    };

    render() {
        return (
            <View style={Styles.listContainer}>
                <View style={Styles.list1}>
                    <LoadingView loading={this.props.store.categoryLoading}>
                    <FlatList data={this.props.store.categoryList}
                              extraData={this.state}
                              style={{flex: 1}}
                              ListEmptyComponent={this.renderNoData}
                              ItemSeparatorComponent={Line.bind(null, {height: 1, backgroundColor: '#e9e9e9'})}
                              renderItem={this.renderItem1.bind(this)}
                              keyExtractor={(item, index) => item.id}

                    />
                    </LoadingView>
                </View>

                <LoadingView loading={this.props.store.productLoading}>
                    <View style={Styles.list2} >
                        <FlatList data={this.props.store.storeProductList}
                                  extraData={this.state}
                                  ListEmptyComponent={this.renderNoData}
                                  renderItem={this.renderItem2.bind(this)}
                                  ItemSeparatorComponent={Line.bind(null, {height: 1, backgroundColor: '#e9e9e9'})}
                                  keyExtractor={(item, index) => item.id}
                                  getItemLayout={(data, index) => ( {length: 100, offset: 100 * index, index})}
                        />
                    </View>
                </LoadingView>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    listContainer: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#F3F4F5'
    },
    list1: {
        flex: 0.28,
        backgroundColor: 'white'
    },
    list2: {
        display:'flex',
        flex: 1,
        marginLeft: 5,
        height:SCREEN.height,
        backgroundColor: 'white'
    },
    leftItem: {
        height: 68 / 375.0 * SCREEN.width,
    },

    leftItemSelected: {
        backgroundColor: '#F3F4F5'
    },

    rightItem: {
        height: size(128),
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
    },

    rightItemImage: {
        width: size(100),
        height: size(106)
    },
    rightItemContent: {
        paddingHorizontal: 10,
        display: 'flex',
        width: size(163),
        height: size(106),
        justifyContent: 'space-around',
    }

});

export default connect(
    ({product,store}) => ({product,store})
)(Page);
