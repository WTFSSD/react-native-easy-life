import React from 'react'
import propTypes from 'prop-types'
import {StyleSheet, Text, View, ImageBackground, TouchableOpacity, ScrollView} from 'react-native'
import {Actions, ActionConst} from 'react-native-router-flux'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import {Flex, List} from 'antd-mobile'
import CommonStyle, {size, SCREEN} from '../../assets/styles/index'
import {Line, LoadingView} from '../common/index'
import {connect} from 'react-redux'
import * as request from '../../utils/request';
import Recommand from '../home/Recommand'

const ListItem = List.Item;


class Page extends React.Component {
    constructor() {
        super();
    }


    componentWillMount() {
        this.props.dispatch({
            type: 'store/fetchDetail',
            payload: {store_id: this.props.store_id}
        })
    }

    //添加收藏
    addFav = () => {
        const {store: {detail}, auth} = this.props;
        if (!auth.isSignIn) {
            Actions.auth();
            return;
        }


        console.log('adshfgiuasdyfgvads', detail);
        this.props.dispatch({
            type: 'store/addFav',
            payload: {
                user_id: auth.profile.member_id,
                store_id: detail.id
            }
        })
    };

    /*显示商品头*/
    renderHeader = () => {
        const {store} = this.props;
        const detail = store.detail;
        const recommendProduct = store.recommendProduct;
        const is_collect = detail.store_collect;
        let views = [];
        views.push(
            <ImageBackground source={{uri: detail.store_image}}
                             key={0}
                             style={Styles.header}>
                <TouchableOpacity
                    style={{backgroundColor: 'white', borderRadius: size(15)}}
                    onPress={this.addFav}
                >
                    <View style={is_collect === 1 ? Styles.btn1Active : Styles.btn1}>
                        <FontAwesomeIcon name="heart-o" size={size(15)} color={is_collect === 1 ? 'red' : '#ccc'}/>
                        <Text style={{
                            color: is_collect === 1 ? 'red' : '#ccc',
                            fontSize: 16
                        }}>{is_collect === 1 ? '已收藏' : '收藏店铺'}</Text>
                    </View>
                </TouchableOpacity>
            </ImageBackground>
        );
        // if (recommendProduct && recommendProduct.length > 0) {
        //     let data = recommendProduct.map((i, index) => (
        //         {
        //             ...i,
        //             icon: request.checkImageUrl(i.preview),
        //             text: i.name
        //         }
        //     ));
        //     views.push(<Recommand title={'热销单品'}
        //                           data={data}
        //                           onItemPress={(item) => {
        //                               Actions.productDetail({store_id: item.store_id, id: item.id})
        //                           }}/>)
        // }
        return views;
    };

    renderRecommendProduct = () => {
        const {store} = this.props;
        const recommendProduct = store.recommendProduct;
        let views = [];
        console.log(recommendProduct);
        if (recommendProduct && recommendProduct.length > 0) {
            let data = recommendProduct.map((i, index) => (
                {
                    ...i,
                    icon: request.checkImageUrl(i.preview),
                    text: i.name
                }
            ));
            return <Recommand title={'热销单品'}
                              data={data}
                              onItemPress={(item) => {
                                  Actions.productDetail({store_id: item.store_id, id: item.id})
                              }}/>;
        }
        return null;

    };

    render() {

        if (!this.props.store) {
            return (<View/>)
        }
        const {store: {detail}} = this.props;
        return (
            <View style={Styles.container}>
                <LoadingView loading={this.props.store.detailLoading}>
                    <ScrollView>
                        <View style={{width: SCREEN.width, height: SCREEN.height+100}}>
                            <List renderHeader={this.renderHeader}>
                                <ListItem>{`店铺名称:${detail.store_name}`}</ListItem>

                                <View style={{display: 'flex', flexDirection: 'row', padding: 12}}>
                                    <View style={{display: 'flex'}}>
                                        <Text style={{fontSize: 18, color: '#000'}}>店铺介绍:</Text>
                                    </View>
                                    <View style={{display: 'flex', flex: 1, paddingLeft: 5, justifyContent: 'center'}}>
                                        <Text numberOfLines={0}
                                              style={{fontSize: 14, color: '#333'}}>{detail.store_introduced}</Text>
                                    </View>
                                </View>
                                <Line height={size(10)} backgroundColor="#f2f2f2"/>
                                {this.renderRecommendProduct()}
                                {/*
                         <Recommand title="优惠券">
                            <ScrollView
                                contentContainerStyle={[CommonStyle.pdv10, {flexWrap: 'nowrap', height: size(102)}]}
                                horizontal>
                                {coupons.map((item, index) => (<Coupon {...item} key={index}/>))}
                            </ScrollView>
                        </Recommand>
                        <Line height={size(10)} backgroundColor="#f2f2f2"/>
                        <Recommand title="猜你喜欢"/>
                        <Line height={size(10)} backgroundColor="transparent"/>*/}

                            </List>
                        </View>

                    </ScrollView>
                </LoadingView>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        height: size(563),
    },

    header: {
        height: size(375),
        width: size(375),
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        padding: 10,
    },
    btn1: {
        backgroundColor: 'transparent',
        display: 'flex',
        flexDirection: 'row',
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: size(15),
        paddingHorizontal: 10,
        alignItems: 'center',
        paddingVertical: 2
    },

    btn1Active: {
        backgroundColor: 'transparent',
        display: 'flex',
        flexDirection: 'row',
        borderColor: 'red',
        borderWidth: 1,
        borderRadius: size(15),
        paddingHorizontal: 10,
        alignItems: 'center',
        paddingVertical: 2
    },
    btn2: {
        display: 'flex',
        flexDirection: 'row',
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: size(15),
        paddingHorizontal: 10,
        alignItems: 'center',
        paddingVertical: 2
    },

});

Page.propTypes = {
    store_id: propTypes.any,
};
export default connect(
    ({store, auth}) => ({store, auth})
)(Page);
