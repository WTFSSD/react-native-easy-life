/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {PropTypes, Component} from 'react'
import {StyleSheet, Text, View, AsyncStorage, Image, TouchableOpacity} from 'react-native'
import {connect} from 'react-redux'
import ProsTypes from 'prop-types'
import {} from 'antd-mobile'
import CommonStyle, {SCREEN} from '../../assets/styles/index'
import {Actions, ActionConst} from 'react-native-router-flux'
import Swiper from 'react-native-swiper'

const page = (props) => (
    <TouchableOpacity style={Styles.container} onPress={props.onPress} activeOpacity={0.9}>
        <Image source={{uri: props.item.icon}} style={Styles.icon}/>
        <View style={Styles.bottom}>
            <Text style={CommonStyle.text1}>{props.item.title}</Text>
            <View style={{flex: 1, marginTop: 6}}>
                <Text style={CommonStyle.text2} numberOfLines={2}>{props.item.content}</Text>
            </View>
            {props.item.coupons.length > 0 ? <View style={Styles.coupon}>
                <Image source={require('../../assets/image/coupon.png')} style={{width: 20, height: 13}}/>
                <Swiper showButtons={false} showsPagination={false} horizontal={true} autoplay={false}
                        style={{marginLeft: 3}}>
                    {props.item.coupons.map(({text}, index) => (
                        <View key={index}>
                            <Text style={CommonStyle.text2}>{text}</Text>
                        </View>
                    ))}
                </Swiper>
            </View> : null}
        </View>
    </TouchableOpacity>
);

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        width: (SCREEN.width - 2) / 2,
        height: (SCREEN.width - 2) / 2 * 1.25,
        backgroundColor: 'white',
        marginRight: 1,
    },
    icon: {
        width: (SCREEN.width - 1) / 2,
        height: 158 / 375 * SCREEN.width,
    },
    bottom: {
        padding: 5,
        display: 'flex',
        flex: 1
    },
    coupon: {
        display: 'flex',
        flexDirection: 'row',
        height: 17 / 375 * SCREEN.width,
        alignItems: 'center',
    }

});

page.propTypes = {
    onPress: PropTypes.func,
};
page.defaultProps = {
    onPress: () => {
        Actions.shopDetail()
    },
};
export default page



