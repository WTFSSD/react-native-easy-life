/**
 * Created by wtfssd on 2017/9/15.
 */

import React, {PropTypes, Component} from 'react'
import {StyleSheet, Text, View, WebView,Image,TouchableOpacity} from 'react-native'
import {connect} from 'react-redux'
import {Modal} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'
import {Actions, ActionConst} from 'react-native-router-flux'
import {BackButton} from '../common/index'
import { Loading }  from '../order/common/index'
import CommonStyle, {Color,size,SCREEN} from '../../assets/styles/index'
const html ='<!DOCTYPE html><html><head><meta charset="UTF-8"><meta name="viewport"content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/><title>菜谱推荐</title><style type="text/css">*{margin:0}img{width:100%}p{padding:10px}.content{position:relative}.ifram{position:absolute;left:0;top:0;z-index:100;background:white}</style></head><body><div class="content"><img src="http://a3.topitme.com/0/1c/12/1128107705fd5121c0l.jpg"/><p>海派鱼香肉丝</p><p>特制本帮鱼香汁，爱本帮风味的一定别错过！</p><p>1.干木耳提前1分钟用90度的热水浸泡后捞出备用，蒜肉切成蒜片，姜切成姜丝备用。</p><img src="http://a3.topitme.com/0/1c/12/1128107705fd5121c0l.jpg"/><p>海派鱼香肉丝</p><p>特制本帮鱼香汁，爱本帮风味的一定别错过！</p><p>1.干木耳提前1分钟用90度的热水浸泡后捞出备用，蒜肉切成蒜片，姜切成姜丝备用。</p><img src="http://a3.topitme.com/0/1c/12/1128107705fd5121c0l.jpg"/><p>海派鱼香肉丝</p><p>特制本帮鱼香汁，爱本帮风味的一定别错过！</p><p>1.干木耳提前1分钟用90度的热水浸泡后捞出备用，蒜肉切成蒜片，姜切成姜丝备用。</p><img src="http://a3.topitme.com/0/1c/12/1128107705fd5121c0l.jpg"/><p>海派鱼香肉丝</p><p>特制本帮鱼香汁，爱本帮风味的一定别错过！</p><p>1.干木耳提前1分钟用90度的热水浸泡后捞出备用，蒜肉切成蒜片，姜切成姜丝备用。</p><img src="http://a3.topitme.com/0/1c/12/1128107705fd5121c0l.jpg"/><p>海派鱼香肉丝</p><p>特制本帮鱼香汁，爱本帮风味的一定别错过！</p><p>1.干木耳提前1分钟用90度的热水浸泡后捞出备用，蒜肉切成蒜片，姜切成姜丝备用。</p><img src="http://a3.topitme.com/0/1c/12/1128107705fd5121c0l.jpg"/><p>海派鱼香肉丝</p><p>特制本帮鱼香汁，爱本帮风味的一定别错过！</p><p>1.干木耳提前1分钟用90度的热水浸泡后捞出备用，蒜肉切成蒜片，姜切成姜丝备用。</p></div></body><script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script><script type="text/javascript">$(document).ready(function(){$(\'img\').on(\'click\',function(){window.postMessage($(this).attr(\'src\'),"")})})</script></html>'
class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {
            html:'<div >加载中</div>',
            modal:false,
            loading:true,
            imgUrl:'',
            imageSize:{width:0, height:0},
        };
    }

    /*组件将要加载*/
    componentWillMount() {

    }

    /*组件已经加载*/
    componentDidMount() {
        this.setState({html})
    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }


    closeModal(){
        this.setState({modal:false})
    }

    openModal(imgUrl){
        this.setState({modal:true,loading:true});
        Image.getSize(
            imgUrl,
            (width, height)=>{

                let imageSize ={width:size(375),height:size(375)/(width/height)};
                console.log('getSize',width,height,imageSize);
                this.setState({imgUrl,imageSize,loading:false})
            },
            ()=>{}
            )
    }
    render() {
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                               leftButton={<BackButton/>}

                />
                <WebView style={{flex: 1}}
                         source={{html:this.state.html}}
                         onNavigationStateChange={(e)=>{
                             console.log('onNavigationStateChange',e);
                         }}
                         onMessage = {(e)=>{
                            console.log('onMessage',e);

                             if (typeof e === 'object'){
                                 console.log('onMessage11',e.nativeEvent);
                                 this.openModal(e.nativeEvent.data);
                             }
                         }}
                />

                <Modal
                    transparent={false}
                    maskClosable={true}
                    visible={this.state.modal}
                    onClose={this.closeModal.bind(this)}
                >
                    <View style={[{width:SCREEN.width,height:SCREEN.height,backgroundColor:'transparent'},CommonStyle.centerVH]}>
                        <Loading loading={this.state.loading} style={CommonStyle.centerVH}>
                            <TouchableOpacity onPress={this.closeModal.bind(this)} activeOpacity={0.9}>
                                <Image source={{uri:this.state.imgUrl}}
                                       resizeMode='cover'
                                       style={{...this.state.imageSize}}
                                />
                            </TouchableOpacity>
                        </Loading>


                    </View>

                </Modal>

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
    }
});

export default connect(
    ({home}) => ({home})
)(Page)



