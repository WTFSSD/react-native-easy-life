import PropTypes from 'prop-types';
import React from 'react';
import {
    Linking,
    Platform,
    StyleSheet,
    TouchableOpacity,
    ViewPropTypes,
    View,
    Text
} from 'react-native';
import Sound from 'react-native-sound'
import  { MyIcon } from '../common/index'
import { AudioUtils} from 'react-native-audio';
export default class CustomView extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            duration:0,
        };

    }

    componentDidMount() {
        if(this.props.currentMessage.audio){
            console.log(this.props.currentMessage.audio);
            setTimeout(() => {
                const sound = new Sound('test.acc', AudioUtils.DocumentDirectoryPath, (error) => {
                    if (error) {
                        console.log('failed to load the sound', error);
                    }
                });

                setTimeout(() => {
                    sound.play((success) => {
                        if (success) {
                            console.log('successfully finished playing');
                        } else {
                            console.log('playback failed due to audio decoding errors');
                        }
                    });
                }, 100);
            }, 1000);
        }
    }
    render() {
        if (this.props.currentMessage.audio) {

            return (
                <TouchableOpacity style={[styles.container, this.props.containerStyle]} onPress={() => {

                }}>
                    <View style={styles.audio}>
                       <MyIcon name='yuyin-copy' size={25} color='#ccc'/>
                        <Text>{this.state.duration}s</Text>
                    </View>
                </TouchableOpacity>
            );
        }
        return null;
    }
}

const styles = StyleSheet.create({
    container: {},
    audio: {
        display:'flex',
        flexDirection:'row',
        alignItems:'center'
    },
});

CustomView.defaultProps = {
    currentMessage: {},
    containerStyle: {},
    mapViewStyle: {},
};

CustomView.propTypes = {
    currentMessage: PropTypes.object,
    containerStyle: ViewPropTypes.style,
    mapViewStyle: ViewPropTypes.style,
};
