/**
 * Created by wtfssd on 2017/9/15.
 */
import PropTypes from 'prop-types'
import React, {Component} from 'react'
import {StyleSheet, Text, View, KeyboardAvoidingView, ScrollView, Platform} from 'react-native'
import {connect} from 'react-redux'
import {Modal, InputItem, List, Toast} from 'antd-mobile'
import NavigationBar from 'react-native-navbar'
import * as RouterFlux from 'react-native-router-flux'
import CommonStyle, {Color, size, SCREEN} from '../../assets/styles/index'
import {BackButton, Line, LoadingView} from '../common/index'
import BotttomTool from './bottomTool'

import {GiftedChat, Actions, Bubble} from 'react-native-gifted-chat';
import CustomActions from './CustomActions';
import CustomView from './CustomView';

const prompt = Modal.prompt;

class Page extends Component {


    constructor() {
        super();
        this.state = {
            typingText: null,
            isLoadingEarlier: false,
        };
        this.webview = null;
        this.onSend = this.onSend.bind(this);
        this.renderCustomActions = this.renderCustomActions.bind(this);
        this.renderBubble = this.renderBubble.bind(this);
        this.renderFooter = this.renderFooter.bind(this);
        this.onLoadEarlier = this.onLoadEarlier.bind(this);
    }

    /*组件将要加载*/
    componentWillMount() {
        console.log(this.props.chat);
        const callBacks = {
            onTextMessage: this.onTextMessage.bind(this),
            onPictureMessage: this.onPictureMessage.bind(this),
            onAudioMessage: this.onAudioMessage.bind(this)
        };
        const {dispatch, chat: {isSignIn, accessToken, user, pwd}} = this.props;
        if (isSignIn && accessToken) {
            dispatch({
                type: 'chat/signIn',
                payload: {
                    userInfo: {accessToken},
                    callBacks
                }
            })
        } else {
            dispatch({
                type: 'chat/signIn',
                payload: {
                    userInfo: {user, pwd},
                    callBacks
                }
            })
        }
    }

    /*组件将要卸载*/
    componentWillUnmount() {

    }

    /***********************自定义事件***************************************/
    cleanHistory() {
        this.props.dispatch({
            type: 'chat/clean',
            payload: {}
        })
    }

    /***********************消息UI********************************************/
    onLoadEarlier() {
        console.log('消息UI 加载更早消息');
    }

    onSend(messages = []) {

        console.log('消息UI发送消息', messages);
        this.props.dispatch({
            type: 'chat/sendMessage',
            payload: {message: messages[0]}
        })
    }


    /*************************************环信SDK回调****************************/

    onTextMessage(e) {
        Toast.info("收到文字消息");
        this.props.dispatch({
            type: 'chat/onReceivedMessage',
            payload: {
                message: {type: 'txt', msg: {...e}},
                callBacks: {}
            }
        })
    }

    onPictureMessage(e) {
        Toast.info("收到图片信息");
        this.props.dispatch({
            type: 'chat/onReceivedMessage',
            payload: {
                message: {type: 'img', msg: {...e}},
                callBacks: {}
            }
        })
    }

    onAudioMessage(e) {
        console.log('收到音频文件', e);
        Toast.info("收到音频文件");
    }


    renderCustomActions(props) {

        return (
            <CustomActions
                {...props}
            />
        );
    }

    renderBubble(props) {
        return (
            <Bubble
                {...props}
                wrapperStyle={{
                    left: {
                        backgroundColor: '#f0f0f0',
                        padding: 10
                    },
                    right: {
                        padding: 10
                    }
                }}
            />
        );
    }

    renderCustomView(props) {
        return (
            <CustomView
                {...props}
            />
        );
    }

    renderFooter(props) {
        if (this.state.typingText) {
            return (
                <View style={Styles.footerContainer}>
                    <Text style={Styles.footerText}>
                        {this.state.typingText}
                    </Text>
                </View>
            );
        }
        return null;
    }

    renderComposer(props) {
        return (<BotttomTool {...props}/>)
    }

    /*********************************************************************************************************************/


    render() {
        console.log(this.props.chat);
        return (
            <View style={Styles.container}>
                <NavigationBar tintColor={Color.primary}
                               statusBar={{style: 'light-content'}}
                               title={{title: this.props.title, tintColor: 'white'}}
                               leftButton={<BackButton/>}
                               rightButton={{title: "清除记录", tintColor: 'white', handler: this.cleanHistory.bind(this)}}
                />
                <LoadingView loading={this.props.chat.loading}>
                    <GiftedChat
                        messages={this.props.chat.messages}
                        loadEarlier={this.state.loadEarlier}
                        onLoadEarlier={this.onLoadEarlier}
                        isLoadingEarlier={this.state.isLoadingEarlier}
                        onSend={this.onSend}
                        renderBubble={this.renderBubble}
                        renderCustomView={this.renderCustomView}
                        renderFooter={this.renderFooter}
                        renderComposer={this.renderComposer}
                    />
                </LoadingView>

            </View>

        );
    }
}

const Styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#fff'
    },
    content: {
        height: SCREEN.height - 113,
    },
    footerContainer: {
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
    },
    footerText: {
        fontSize: 14,
        color: '#aaa',
    },
});

export default connect(
    ({chat}) => ({chat})
)(Page)
